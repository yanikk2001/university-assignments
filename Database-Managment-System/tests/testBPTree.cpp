#define CATCH_CONFIG_MAIN
#include "../../catch.h"
#include <string>
#include <sstream>

#include "../DataStructures/BPTree.h"

TEST_CASE("Insertion")
{
    BPTree<int, char> tr(2);

    SECTION("Display empty")
    {
        std::stringstream sstr;
        tr.content(sstr);
        REQUIRE(sstr.str().size() == 0);
    }

    SECTION("Insert-only one node")
    {
        std::stringstream sstr;
        tr.insert(1, 'a');
        tr.insert(2, 'b');

        tr.content(sstr);

        std::string str = "1a2b";
        REQUIRE(sstr.str() == str);
    }

    SECTION("Insert-creating parent root node")
    {
        tr.insert(1, 'a');
        tr.insert(2, 'b');
        tr.insert(3, 'c');

        std::stringstream sstr;
        std::string str = "21a2b3c";
        tr.content(sstr);
        REQUIRE(sstr.str() == str);
    }

    SECTION("Insert-adding to parent root node")
    {
        std::stringstream sstr;
        tr.insert(1, 'a');
        tr.insert(2, 'b');
        tr.insert(3, 'c');
        tr.insert(4, 'd');

        tr.content(sstr);

        std::string str = "231a2b3c4d";
        REQUIRE(sstr.str() == str);
    }

    SECTION("Insert-spliting parent root node")
    {
        std::stringstream sstr;
        tr.insert(1, 'a');
        tr.insert(2, 'b');
        tr.insert(3, 'c');
        tr.insert(4, 'd');
        tr.insert(5, 'e');

        tr.content(sstr);

        std::string str = "321a2b43c4d5e";
        REQUIRE(sstr.str() == str);
    }
}

TEST_CASE("Other data types")
{
    SECTION("String as value")
    {
        BPTree<int, std::string> tr(2);

        tr.insert(12, "Hi");

        tr.insert(1, "le&1");

        tr.insert(6, "h03");

        std::stringstream sstr;
        tr.content(sstr);
        REQUIRE(sstr.str() == "61le&16h0312Hi");
    }
}

TEST_CASE("Searching")
{
    BPTree<std::string, size_t> tr(4);
    tr.insert("Agent Smith", 1);
    tr.insert("Neo", 2);
    tr.insert("Trinity", 3);
    tr.insert("Marcus", 4);
    tr.insert("XAE||12", 5);
    tr.insert("MUCK", 6);
    tr.insert("Bob", 7);
    tr.insert("Christopher Robin", 8);
    tr.insert("Winnie The Pooh", 9);

    std::stringstream sstr;
    tr.content(sstr);

    REQUIRE(sstr.str() == "Christopher RobinNeoAgent Smith1Bob7Christopher Robin8MUCK6Marcus4Neo2Trinity3Winnie The Pooh9XAE||125");

    SECTION("Searching for exsisting individual key")
    {
        REQUIRE(tr.find("Agent Smith") == 1);
        REQUIRE(tr.find("Neo") == 2);
        REQUIRE(tr.find("Trinity") == 3);
        REQUIRE(tr.find("Marcus") == 4);
        REQUIRE(tr.find("XAE||12") == 5);
        REQUIRE(tr.find("MUCK") == 6);
        REQUIRE(tr.find("Bob") == 7);
        REQUIRE(tr.find("Christopher Robin") == 8);
        REQUIRE(tr.find("Winnie The Pooh") == 9);
        REQUIRE(tr.find("Neo") == 2);
    }

    SECTION("Searching for non-existing key")
    {
        REQUIRE_THROWS(tr.find("Kekw"));
        REQUIRE_THROWS(tr.find("Hello"));
    }
}

TEST_CASE("Removing elements")
{
    BPTree<int, char> tr(2);

    SECTION("Remove empty")
    {
        tr.remove(1);
        std::stringstream sstr;
        tr.content(sstr);
        REQUIRE(sstr.str().size() == 0);
    }

    SECTION("Removing at root")
    {
        std::stringstream sstr;
        tr.insert(1, 'a');
        tr.insert(2, 'b');

        tr.content(sstr);

        std::string str = "1a2b";
        REQUIRE(sstr.str() == str);

        tr.remove(1);
        REQUIRE_THROWS(tr.find(1));
        REQUIRE(tr.find(2) == 'b');

        std::stringstream sstr1;
        str = "2b";
        tr.content(sstr1);
        REQUIRE(sstr1.str() == str);

        std::stringstream sstr2;
        tr.remove(2);
        REQUIRE_THROWS(tr.find(2));
        tr.content(sstr2);
        REQUIRE(sstr2.str().size() == 0);
    }

    SECTION("Remove key from node")
    {
        tr.insert(1, 'a');
        tr.insert(2, 'b');
        tr.insert(3, 'c');

        std::stringstream sstr;
        std::string str = "21a2b3c";
        tr.content(sstr);
        REQUIRE(sstr.str() == str);

        tr.remove(2);
        REQUIRE_THROWS(tr.find(2));
        REQUIRE(tr.find(1) == 'a');
        REQUIRE(tr.find(3) == 'c');

        std::stringstream sstr1;
        str = "21a3c";
        tr.content(sstr1);
        REQUIRE(sstr1.str() == str);
    }

    SECTION("Remove only from leaf merging nodes")
    {
        std::stringstream sstr;
        tr.insert(1, 'a');
        tr.insert(2, 'b');
        tr.insert(3, 'c');
        tr.insert(4, 'd');

        tr.content(sstr);

        std::string str = "231a2b3c4d";
        REQUIRE(sstr.str() == str);

        tr.remove(2);
        REQUIRE_THROWS(tr.find(2));
        REQUIRE(tr.find(1) == 'a');
        REQUIRE(tr.find(4) == 'd');
        REQUIRE(tr.find(3) == 'c');

        str = "241a3c4d";
        std::stringstream sstr1;
        tr.content(sstr1);
        REQUIRE(sstr1.str() == str);
    }

    SECTION("Removing- if right child has key to share")
    {
        std::stringstream sstr;
        tr.insert(1, 'a');
        tr.insert(2, 'b');
        tr.insert(3, 'c');
        tr.insert(4, 'd');
        tr.insert(5, 'e');

        tr.content(sstr);

        std::string str = "321a2b43c4d5e";
        REQUIRE(sstr.str() == str);

        tr.remove(3);
        REQUIRE_THROWS(tr.find(3));
        REQUIRE(tr.find(1) == 'a');
        REQUIRE(tr.find(4) == 'd');
        REQUIRE(tr.find(2) == 'b');
        REQUIRE(tr.find(5) == 'e');

        str = "321a2b54d5e";

        std::stringstream sstr1;
        tr.content(sstr1);
        REQUIRE(sstr1.str() == str);
    }

    SECTION("Removing if left child has keys to share")
    {
        BPTree<int, char> tr(3);
        tr.insert(1, 'a');
        tr.insert(2, 'a');
        tr.insert(3, 'a');
        tr.insert(4, 'a');
        tr.insert(5, 'a');
        tr.insert(6, 'a');
        tr.insert(7, 'a');
        tr.insert(8, 'a');
        tr.insert(9, 'a');
        tr.insert(10, 'a');

        tr.remove(7);
        REQUIRE_THROWS(tr.find(7));

        std::stringstream sstr;
        tr.content(sstr);

        std::string str = "531a2a3a4a75a6a8a9a10a";
        REQUIRE(sstr.str() == str);
    }
}