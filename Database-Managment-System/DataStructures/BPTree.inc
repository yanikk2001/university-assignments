// Constructor of Node
template <typename KeyType, typename ValueType>
inline BPTree<KeyType, ValueType>::Node::Node(const size_t &degree)
{
	try
	{
		key = new KeyType[degree];
		value = new ValueType[degree];
		ptr = new Node *[degree + 1](); // filled with nullptrs
	}
	catch (const std::bad_alloc &e)
	{
		delete[] key;
		delete[] value;
		delete[] ptr;
		throw e;
	}
}

template<typename KeyType, typename ValueType>
void inline BPTree<KeyType, ValueType>::Node::insert(KeyType& _key, ValueType& val, const size_t& pos, bool rearange) 
{
	if(rearange)
	{
		for (size_t i = this->size; i > pos; i--)
		{
			key[i] = key[i - 1];
			value[i] = value[i - 1];
		}
	}

	key[pos] = _key;
	value[pos] = val;
	size++;

	ptr[size] = ptr[size - 1]; // Move link to next leaf node to the last ptr
	ptr[size - 1] = nullptr;
}

template<typename KeyType, typename ValueType>
void inline BPTree<KeyType, ValueType>::Node::remove(const size_t& pos) 
{
	//Remove key and rearange array
	for (size_t i = pos; i < size; i++)
	{
		key[i] = key[i + 1];
		value[i] = value[i + 1];
	}
	size--;
	ptr[size] = ptr[size + 1];
	ptr[size + 1] = nullptr;
}

template <typename KeyType, typename ValueType>
void BPTree<KeyType, ValueType>::clean(Node *_root)
{
	if (!_root)
		return;

	for (size_t i = 0; i < _root->size; i++)
	{
		clean(_root->ptr[i]);
	}
	delete[] _root->key;
	delete[] _root->value;
	delete[] _root->ptr;
	_root->key = nullptr;
	_root->value = nullptr;
	_root->ptr = nullptr;
}

template <typename KeyType, typename ValueType>
inline typename BPTree<KeyType, ValueType>::siblings BPTree<KeyType, ValueType>::getToLeafLevel(Node **cursor, Node **parent, const KeyType &x)
{
	// Till cursor reaches the
	// leaf node
	siblings s;
	while ((*cursor)->IS_LEAF == false)
	{
		*parent = *cursor;

		for (size_t i = 0; i < (*cursor)->size; i++)
		{
			// If found the position
			// where we have to insert
			// node
			s.indexLeft = i - 1;
			s.indexRight = i + 1;
			if (x < (*cursor)->key[i])
			{
				*cursor = (*cursor)->ptr[i];
				break;
			}

			// If reaches the end
			if (i == (*cursor)->size - 1)
			{
				s.indexLeft = i;
				s.indexRight = i + 2;
				*cursor = (*cursor)->ptr[i + 1];
				break;
			}
		}
	}
	return s;
}

template <typename KeyType, typename ValueType>
inline size_t BPTree<KeyType, ValueType>::findPos(const KeyType arr[], const size_t &size, const KeyType &key) const
{
	size_t i = 0;
	while (key > arr[i] && i < size)
	{
		i++;
	}
	return i;
}

template <typename KeyType, typename ValueType>
void BPTree<KeyType, ValueType>::insertAt(KeyType arr[], const size_t &size, const size_t &pos, KeyType &value) const
{
	for (size_t j = size; j > pos; j--)
	{
		arr[j] = arr[j - 1];
	}

	arr[pos] = value;
}

template <typename KeyType, typename ValueType>
void BPTree<KeyType, ValueType>::insertAt(ValueType arr[], const size_t &size, const size_t &pos, ValueType &value) const
{
	for (size_t j = size; j > pos; j--)
	{
		arr[j] = arr[j - 1];
	}

	arr[pos] = value;
}
// Function to implement the Insert
// Operation in B+ Tree
template <typename KeyType, typename ValueType>
inline void BPTree<KeyType, ValueType>::insert(KeyType x, ValueType val)
{
	// If root is null then return
	// newly created node
	if (root == nullptr)
	{
		try
		{
			root = new Node(MAX);
		}
		catch (const std::bad_alloc &e)
		{
			return;
		}

		root->key[0] = x;
		root->value[0] = val;
		root->IS_LEAF = true;
		root->size = 1;
	}

	// Traverse the B+ Tree
	else
	{
		Node *cursor = root;
		Node *parent;

		getToLeafLevel(&cursor, &parent, x);

		// We are on a leaf level now
		if (cursor->size < MAX) // Insert into node, no overflow
		{
			// Find right pos in keys arr
			size_t i = findPos(cursor->key, cursor->size, x);

			// Move all values after ith pos one cell to the right
			cursor->insert(x, val, i);
		}

		else // Can`t insert into node, Overflow
		{
			// Create a newLeaf node
			Node *newLeaf{};
			KeyType *virtualNode{};
			ValueType *virtualNodeVal{};
			try
			{
				newLeaf = new Node(MAX);

				virtualNode = new KeyType[MAX + 1];
				virtualNodeVal = new ValueType[MAX + 1];
			}
			catch (const std::bad_alloc &e)
			{
				delete newLeaf;
				delete[] virtualNode;
				delete[] virtualNodeVal;
				return;
			}

			// Update cursor to virtual
			// node created
			for (size_t i = 0; i < MAX; i++)
			{
				virtualNode[i] = cursor->key[i];
				virtualNodeVal[i] = cursor->value[i];
			}

			size_t i = 0, j;
			// Traverse to find where the new
			// node is to be inserted
			i = findPos(virtualNode, MAX, x);

			// Update the current virtual
			// Node to its previous
			insertAt(virtualNode, MAX, i, x);
			insertAt(virtualNodeVal, MAX, i, val);

			newLeaf->IS_LEAF = true;

			cursor->size = (MAX + 1) / 2;
			newLeaf->size = MAX + 1 - (MAX + 1) / 2;

			cursor->ptr[cursor->size] = newLeaf;

			newLeaf->ptr[newLeaf->size] = cursor->ptr[MAX];

			cursor->ptr[MAX] = nullptr;

			// Update the current virtual
			// Node's key to its previous
			for (i = 0; i < cursor->size; i++)
			{
				cursor->key[i] = virtualNode[i];
				cursor->value[i] = virtualNodeVal[i];
			}

			// Update the newLeaf key to
			// virtual Node
			for (i = 0, j = cursor->size; i < newLeaf->size; i++, j++)
			{
				newLeaf->key[i] = virtualNode[j];
				newLeaf->value[i] = virtualNodeVal[j];
			}

			// If cursor is the root node
			if (cursor == root)
			{
				// Create a new Node
				Node *newRoot{};
				try
				{
					newRoot = new Node(MAX);
				}
				catch (const std::bad_alloc &e)
				{
					delete newLeaf;
					delete[] virtualNode;
					delete[] virtualNodeVal;
					return;
				}

				// Update rest field of
				// B+ Tree Node
				newRoot->key[0] = newLeaf->key[0];
				newRoot->ptr[0] = cursor;
				newRoot->ptr[1] = newLeaf;
				newRoot->IS_LEAF = false;
				newRoot->size = 1;
				root = newRoot;
			}
			else
			{
				// Recursive Call for
				// insert in internal
				try
				{
					insertInternal(newLeaf->key[0], parent, newLeaf);
				}
				catch (const std::bad_alloc &e)
				{
					return;
				}
			}
			delete[] virtualNode;
			delete[] virtualNodeVal;
		}
	}
}

template <typename KeyType, typename ValueType>
inline void BPTree<KeyType, ValueType>::display(const Node *_root, std::ostream &out) const
{
	if (_root == nullptr)
		return;

	for (size_t i = 0; i < _root->size; i++)
	{
		out << _root->key[i] << ' ';
	}
	out << '\n';

	if (_root->IS_LEAF)
	{
		for (size_t i = 0; i < _root->size; i++)
		{
			out << _root->value[i] << ' ';
		}
		out << '\n';
		return;
	}

	if (_root->ptr[0]->IS_LEAF)
	{
		for (size_t j = 0; j < _root->size + 1; j++)
		{
			auto childLeaf = _root->ptr[j];
			for (size_t i = 0; i < childLeaf->size; i++)
			{
				out << childLeaf->key[i] << ' ';
			}
			out << "  ";
		}
		out << '\n';

		for (size_t j = 0; j < _root->size + 1; j++)
		{
			auto childLeaf = _root->ptr[j];
			for (size_t i = 0; i < childLeaf->size; i++)
			{
				out << childLeaf->value[i] << ' ';
			}
			out << "  ";
		}
		out << '\n';
		return;
	}

	for (size_t i = 0; i < _root->size + 1; i++)
	{
		display(_root->ptr[i], out);
	}
}

template <typename KeyType, typename ValueType>
inline void BPTree<KeyType, ValueType>::content(std::ostream &out) const
{
	content(root, out);
}

template <typename KeyType, typename ValueType>
inline void BPTree<KeyType, ValueType>::content(const Node *_root, std::ostream &out) const
{
	if (!_root)
		return;

	for (size_t i = 0; i < _root->size; i++)
	{
		out << _root->key[i];
		if (_root->IS_LEAF)
			out << _root->value[i];
	}
	if (_root->IS_LEAF)
		return;

	for (size_t i = 0; i < _root->size + 1; i++)
	{
		content(_root->ptr[i], out);
	}
}

// Function to implement the Insert
// Internal Operation in B+ Tree
template <typename KeyType, typename ValueType>
inline void BPTree<KeyType, ValueType>::insertInternal(KeyType x, Node *cursor, Node *child)
{
	// If we dont have overflow
	if (cursor->size < MAX)
	{
		// Traverse the child node
		// for current cursor node
		size_t i = findPos(cursor->key, cursor->size, x);

		// Traverse the cursor node
		// and update the current key
		// to its previous node key
		insertAt(cursor->key, cursor->size, i, x);

		// Traverse the cursor node
		// and update the current ptr
		// to its previous node ptr
		for (size_t j = cursor->size + 1; j > i + 1; j--)
		{
			cursor->ptr[j] = cursor->ptr[j - 1];
		}

		cursor->ptr[i + 1] = child;
		cursor->size++;
	}

	// For overflow, break the node
	else
	{
		// For new Interval
		Node *newInternal{};
		KeyType *virtualKey{};
		Node **virtualPtr{};
		try
		{
			newInternal = new Node(MAX);
			virtualKey = new KeyType[MAX + 1];
			virtualPtr = new Node *[MAX + 2];
		}
		catch (const std::bad_alloc &e)
		{
			delete newInternal;
			delete[] virtualKey;
			delete[] virtualPtr;
			throw e;
		}

		// Insert the current list key
		// of cursor node to virtualKey
		for (size_t i = 0; i < MAX; i++)
		{
			virtualKey[i] = cursor->key[i];
		}

		// Insert the current list ptr
		// of cursor node to virtualPtr
		for (size_t i = 0; i < MAX + 1; i++)
		{
			virtualPtr[i] = cursor->ptr[i];
		}

		size_t i = 0, j;

		// Traverse to find where the new
		// node is to be inserted
		i = findPos(virtualKey, MAX, x);

		// Traverse the virtualKey node
		// and update the current key
		// to its previous node key
		insertAt(virtualKey, MAX + 1, i, x);

		// Traverse the virtualKey node
		// and update the current ptr
		// to its previous node ptr
		for (size_t j = MAX + 2; j > i + 1; j--)
		{
			virtualPtr[j] = virtualPtr[j - 1];
		}

		virtualPtr[i + 1] = child;
		newInternal->IS_LEAF = false;

		cursor->size = (MAX + 1) / 2;

		newInternal->size = MAX - (MAX + 1) / 2;

		// Insert new node as an
		// internal node
		for (i = 0, j = cursor->size + 1; i < newInternal->size; i++, j++)
		{
			newInternal->key[i] = virtualKey[j];
		}

		for (i = 0, j = cursor->size + 1; i < newInternal->size + 1; i++, j++)
		{
			newInternal->ptr[i] = virtualPtr[j];
		}

		// If cursor is the root node
		if (cursor == root)
		{
			// Create a new root node
			Node *newRoot{};
			try
			{
				newRoot = new Node(MAX);
			}
			catch (const std::bad_alloc &e)
			{
				delete newInternal;
				delete[] virtualKey;
				delete[] virtualPtr;
				throw e;
			}

			// Update key value
			newRoot->key[0] = cursor->key[cursor->size];

			// Update rest field of
			// B+ Tree Node
			newRoot->ptr[0] = cursor;
			newRoot->ptr[1] = newInternal;
			newRoot->IS_LEAF = false;
			newRoot->size = 1;
			root = newRoot;
		}
		else
		{
			// Recursive Call to insert
			// the data
			try
			{
				insertInternal(cursor->key[cursor->size], findParent(root, cursor), newInternal);
			}
			catch (const std::bad_alloc &e)
			{
				throw e;
			}
		}
		delete[] virtualKey;
		delete[] virtualPtr;
	}
}

// Function to find the parent node
template <typename KeyType, typename ValueType>
inline typename BPTree<KeyType, ValueType>::Node *BPTree<KeyType, ValueType>::findParent(Node *cursor, Node *child)
{
	Node *parent;

	// If cursor reaches the end of Tree
	if (cursor->IS_LEAF)
	{
		return nullptr;
	}

	// Traverse the current node with
	// all its child
	for (size_t i = 0; i < cursor->size + 1; i++)
	{
		// Update the parent for the
		// child Node
		if (cursor->ptr[i] == child)
		{
			parent = cursor;
			return parent;
		}

		// Else recursively traverse to
		// find child node
		else
		{
			parent = findParent(cursor->ptr[i], child);

			// If parent is found, then
			// return that parent node
			if (parent != nullptr)
				return parent;
		}
	}

	// Return parent node
	return parent;
}

template <typename KeyType, typename ValueType>
size_t BPTree<KeyType, ValueType>::GetValuePos(const KeyType &key, const KeyType keys[], const size_t &size) const
{
	for (size_t i = 0; i < size; i++)
	{
		if (keys[i] == key)
			return i;
	}
	throw std::invalid_argument("No such key exists");
}

template <typename KeyType, typename ValueType>
typename BPTree<KeyType, ValueType>::Node *BPTree<KeyType, ValueType>::findNode(const KeyType &key, Node *_root) const
{
	if (!_root)
		return nullptr;

	if (_root->IS_LEAF)
		return _root;

	size_t i = 0;
	for (i = 0; i < _root->size; i++)
	{
		if (key < _root->key[i])
			return findNode(key, _root->ptr[i]);
	}
	return findNode(key, _root->ptr[_root->size]);
}

template <typename KeyType, typename ValueType>
ValueType BPTree<KeyType, ValueType>::find(const KeyType &key) const
{
	Node *n = findNode(key, root);
	if (!n)
		throw std::invalid_argument("There is no such key");
	size_t valPos;
	try
	{
		valPos = GetValuePos(key, n->key, n->size);
	}
	catch (const std::invalid_argument &e)
	{
		throw e;
	}

	return n->value[valPos];
}

template <typename KeyType, typename ValueType>
void BPTree<KeyType, ValueType>::remove(const KeyType& x)
{
	if (root == nullptr)
		return;

	Node *cursor = root;
	Node *parent;
	int leftSibling, rightSibling;
	//Find node, return it`s siblings
	siblings Siblings = getToLeafLevel(&cursor, &parent, x);
	leftSibling = Siblings.indexLeft;
	rightSibling = Siblings.indexRight;

	//Search keys
	size_t pos;
	try
	{
		pos = GetValuePos(x, cursor->key, cursor->size);
	}
	catch(const std::invalid_argument& e) //If it throws it didnt found the key
	{
		return;
	}
	
	//Remove key and rearange array
	cursor->remove(pos);

	if (cursor == root && cursor->size <= 0) //-------------------------------------
	{
		clean(root);
		return;
	}

	//Minimum children number requirement is met so no rebalancing is needed
	if (cursor->size >= (MAX + 1) / 2)
	{
		return;
	}

	//If there is left sibling
	if (leftSibling >= 0)
	{
		Node* leftNode = parent->ptr[leftSibling];
		if (leftNode->size >= (MAX + 1) / 2 + 1) //If left sibling has a key to share 
		{
			KeyType lastKeyFromLeftSibling = leftNode->key[leftNode->size - 1];
			ValueType lastValueFromSiblng = leftNode->value[leftNode->size - 1];
			//Move all keys to the left
			cursor->insert(lastKeyFromLeftSibling, lastValueFromSiblng, 0);
			leftNode->size--;
			leftNode->ptr[leftNode->size] = cursor;
			leftNode->ptr[leftNode->size + 1] = nullptr;
			parent->key[leftSibling] = cursor->key[0]; //Change parent search key
			return;
		}
	}

	//If there is right sibling
	if (rightSibling <= int(parent->size))
	{
		Node* rightNode = parent->ptr[rightSibling];
		if (rightNode->size >= (MAX + 1) / 2 + 1) //If right sibling can give one key
		{
			//Get first key of right sibling and insert at the end
			cursor->insert(rightNode->key[0], rightNode->value[0], cursor->size);

			rightNode->size--;
			rightNode->ptr[rightNode->size] = rightNode->ptr[rightNode->size + 1];
			rightNode->ptr[rightNode->size + 1] = nullptr;
			for (size_t i = 0; i < rightNode->size; i++) //Rearange right node
			{
				rightNode->key[i] = rightNode->key[i + 1];
				rightNode->value[i] = rightNode->value[i + 1];
			}
			parent->key[rightSibling - 1] = rightNode->key[0]; //Change parent search key
			return;
		}
	}

	if (leftSibling >= 0) //There is left node but doesn`t have have a key to share
	{
		Node *leftNode = parent->ptr[leftSibling];
		for (size_t i = leftNode->size, j = 0; j < cursor->size; i++, j++) //Fill left node with cursor`s content
		{
			leftNode->key[i] = cursor->key[j];
			leftNode->value[i] = cursor->value[j];
		}
		// leftNode->ptr[leftNode->size] = nullptr;
		leftNode->size += cursor->size;
		leftNode->ptr[leftNode->size] = cursor->ptr[cursor->size];
		removeInternal(parent->key[leftSibling], parent, cursor); //Search inner nodes to remove instances of the key
		//Remove cursor node
		delete[] cursor->key;
		delete[] cursor->value;
		delete[] cursor->ptr;
		delete cursor;
	}
	else if (rightSibling <= int(parent->size)) //There is right node but has to keys to share
	{
		Node *rightNode = parent->ptr[rightSibling];
		for (size_t i = cursor->size, j = 0; j < rightNode->size; i++, j++) //Merge the two nodes into cursor
		{
			cursor->key[i] = rightNode->key[j];
			cursor->value[i] = rightNode->value[j];
		}
		// cursor->ptr[cursor->size] = nullptr;
		cursor->ptr[cursor->size] = rightNode->ptr[rightNode->size];
		cursor->size += rightNode->size;
		removeInternal(parent->key[rightSibling - 1], parent, rightNode); //Search inner nodes to remove instances of the key
		delete[] rightNode->key;
		delete[] rightNode->value;
		delete[] rightNode->ptr;
		delete rightNode;
	}
}

template <typename KeyType, typename ValueType>
void BPTree<KeyType, ValueType>::removeInternal(const KeyType& x, Node *cursor, Node *child)
{
	if (cursor == root && cursor->size == 1)
	{
		if (cursor->ptr[1] == child) //Remove old root containing old key
		{
			delete[] child->key;
			delete[] child->value;
			delete[] child->ptr;
			delete child;
			root = cursor->ptr[0]; //Make left child new root
			delete[] cursor->key;
			delete[] cursor->value;
			delete[] cursor->ptr;
			delete cursor;
			return;
		}
		else if (cursor->ptr[0] == child)
		{
			delete[] child->key;
			delete[] child->value;
			delete[] child->ptr;
			delete child;
			root = cursor->ptr[1];
			delete[] cursor->key;
			delete[] cursor->value;
			delete[] cursor->ptr;
			delete cursor;
			return;
		}
	}

	//Find key pos in parent
	size_t pos;
	for (pos = 0; pos < cursor->size; pos++)
	{
		if (cursor->key[pos] == x)
		{
			break;
		}
	}

	//Move arr 1 position to the left from pos of old key
	for (size_t i = pos; i < cursor->size; i++)
	{
		cursor->key[i] = cursor->key[i + 1];
	}

	//Find pos of child in parent ptr arr
	for (pos = 0; pos < cursor->size + 1; pos++)
	{
		if (cursor->ptr[pos] == child)
		{
			break;
		}
	}

	//Move arr 1 position to the left from pos of old key ptr
	for (size_t i = pos; i < cursor->size + 1; i++)
	{
		cursor->ptr[i] = cursor->ptr[i + 1];
	}
	cursor->size--;

	//If internal node meets mimimal num of children requirements return
	if (cursor->size >= (MAX + 1) / 2 - 1)
	{
		return;
	}

	//Root has no minimal requirements
	if (cursor == root)
		return;

	Node *parent = findParent(root, cursor);
	size_t leftSibling, rightSibling;
	for (pos = 0; pos < parent->size + 1; pos++)
	{
		if (parent->ptr[pos] == cursor)
		{
			leftSibling = pos - 1;
			rightSibling = pos + 1;
			break;
		}
	}
	if (leftSibling >= 0)
	{
		Node *leftNode = parent->ptr[leftSibling];
		if (leftNode->size >= (MAX + 1) / 2)
		{
			for (size_t i = cursor->size; i > 0; i--)
			{
				cursor->key[i] = cursor->key[i - 1];
			}
			cursor->key[0] = parent->key[leftSibling];
			parent->key[leftSibling] = leftNode->key[leftNode->size - 1];
			for (size_t i = cursor->size + 1; i > 0; i--)
			{
				cursor->ptr[i] = cursor->ptr[i - 1];
			}
			cursor->ptr[0] = leftNode->ptr[leftNode->size];
			cursor->size++;
			leftNode->size--;
			return;
		}
	}
	if (rightSibling <= parent->size)
	{
		Node *rightNode = parent->ptr[rightSibling];
		if (rightNode->size >= (MAX + 1) / 2)
		{
			cursor->key[cursor->size] = parent->key[pos];
			parent->key[pos] = rightNode->key[0];
			for (size_t i = 0; i < rightNode->size - 1; i++)
			{
				rightNode->key[i] = rightNode->key[i + 1];
			}
			cursor->ptr[cursor->size + 1] = rightNode->ptr[0];
			for (size_t i = 0; i < rightNode->size; ++i)
			{
				rightNode->ptr[i] = rightNode->ptr[i + 1];
			}
			cursor->size++;
			rightNode->size--;
			return;
		}
	}
	if (leftSibling >= 0)
	{
		Node *leftNode = parent->ptr[leftSibling];
		leftNode->key[leftNode->size] = parent->key[leftSibling];
		for (size_t i = leftNode->size + 1, j = 0; j < cursor->size; j++)
		{
			leftNode->key[i] = cursor->key[j];
		}
		for (size_t i = leftNode->size + 1, j = 0; j < cursor->size + 1; j++)
		{
			leftNode->ptr[i] = cursor->ptr[j];
			cursor->ptr[j] = nullptr;
		}
		leftNode->size += cursor->size + 1;
		cursor->size = 0;
		removeInternal(parent->key[leftSibling], parent, cursor);
	}
	else if (rightSibling <= parent->size)
	{
		Node *rightNode = parent->ptr[rightSibling];
		cursor->key[cursor->size] = parent->key[rightSibling - 1];
		for (size_t i = cursor->size + 1, j = 0; j < rightNode->size; j++)
		{
			cursor->key[i] = rightNode->key[j];
		}
		for (size_t i = cursor->size + 1, j = 0; j < rightNode->size + 1; j++)
		{
			cursor->ptr[i] = rightNode->ptr[j];
			rightNode->ptr[j] = nullptr;
		}
		cursor->size += rightNode->size + 1;
		rightNode->size = 0;
		removeInternal(parent->key[rightSibling - 1], parent, rightNode);
	}
}