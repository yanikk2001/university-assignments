#pragma once

#include <vector>
#include <iostream>
#include <utility>

using namespace std;

// BP tree
template <typename KeyType, typename ValueType>
class BPTree
{
	// BP node
	// template <typename KeyType, typename ValueType>
	struct Node
	{
		bool IS_LEAF;
		KeyType *key{};
		ValueType *value{};
		size_t size;
		Node** ptr;
		friend class BPTree<KeyType, ValueType>;

		Node(const size_t& degree);

		void insert(KeyType& _key, ValueType& val, const size_t& pos, bool rearange = true);
		void remove(const size_t& pos);
	};

	Node *root{};
	const size_t MAX;

private:
	//Helper structs
	struct siblings
	{
		int indexRight;
		int indexLeft;
	};

public:
	BPTree(const size_t& degree): MAX(degree) {}
	~BPTree() {clean(root);}
	BPTree(const BPTree& other) = delete;
	BPTree& operator=(const BPTree& other) = delete;
	BPTree(const BPTree&& other) = delete;
	// void search(int);
	void display(std::ostream& out = std::cout) const {display(root, out);}
	void content(std::ostream& out)const;
	void insert(const KeyType x, const ValueType val);
	ValueType find(const KeyType& key) const; 
	void remove(const KeyType& key);

private:
	void insertInternal(KeyType x, Node* cursor, Node* child);
	void removeInternal(const KeyType& x, Node *cursor, Node *child);
	Node* findParent(Node* cursor, Node* child);
	void display(const Node* _root, std::ostream& out) const;
	void content(const Node* _root, std::ostream& out) const;
	void clean(Node* _root);
	Node* findNode(const KeyType& key, Node* _root) const;
	size_t GetValuePos(const KeyType& key, const KeyType keys[], const size_t& size) const;

	//Insert helper methods
	siblings getToLeafLevel(Node** cursor, Node** parent, const KeyType& x);
	size_t findPos(const KeyType arr[], const size_t& size, const KeyType& key) const;
	void insertAt(KeyType arr[], const size_t& size, const size_t& pos, KeyType& value) const;
	void insertAt(ValueType arr[], const size_t& size, const size_t& pos, ValueType& value) const;
};

#include "BPTree.inc"