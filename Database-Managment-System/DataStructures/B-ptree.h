#pragma once

#include <vector>
#include <iostream>

using namespace std;
size_t MAX = 3;



// BP tree
template <typename KeyType, typename ValueType>
class BPTree
{
	// BP node
	// template <typename KeyType, typename ValueType>
	struct Node
	{
		bool IS_LEAF;
		KeyType *key{};
		ValueType *value{};
		size_t size;
		Node** ptr;
		friend class BPTree<KeyType, ValueType>;

		Node();
	};

	Node<KeyType, ValueType> *root{};
	void insertInternal(int x, Node<KeyType, ValueType>* cursor, Node<KeyType, ValueType>* child);
	Node<KeyType, ValueType> *findParent(Node<KeyType, ValueType>* cursor, Node<KeyType, ValueType>* child);

	void display(Node<KeyType, ValueType>* _root);
public:
	// BPTree();
	// ~BPTree();
	// void search(int);
	void display() {display(root);}
	void insert(const KeyType x, const ValueType val);
	// Node *getRoot();
};

// Constructor of Node
template <typename KeyType, typename ValueType>
Node<KeyType, ValueType>::Node()
{
	key = new KeyType[MAX];
	value = new ValueType[MAX];
	ptr = new Node<KeyType, ValueType> *[MAX + 1];
}

// Function to implement the Insert
// Operation in B+ Tree
template <typename KeyType, typename ValueType>
void BPTree<KeyType, ValueType>::insert(KeyType x, ValueType val)
{
	// If root is null then return
	// newly created node
	if (root == nullptr)
	{
		root = new Node<KeyType, ValueType>;
		root->key[0] = x;
		root->value[0] = val;
		root->IS_LEAF = true;
		root->size = 1;
	}

	// Traverse the B+ Tree
	else
	{
		Node<KeyType, ValueType> *cursor = root;
		Node<KeyType, ValueType> *parent;

		// Till cursor reaches the
		// leaf node
		while (cursor->IS_LEAF == false)
		{
			parent = cursor;

			for (size_t i = 0; i < cursor->size; i++)
			{
				// If found the position
				// where we have to insert
				// node
				if (x < cursor->key[i])
				{
					cursor = cursor->ptr[i];
					break;
				}

				// If reaches the end
				if (i == cursor->size - 1)
				{
					cursor = cursor->ptr[i + 1];
					break;
				}
			}
		}

		//We are on a leaf level now
		if (cursor->size < MAX)//Insert into node, no overflow
		{
			//Find right pos in keys arr
			size_t i = 0;
			while (x > cursor->key[i] && i < cursor->size)
			{
				i++;
			}

			//Move all values after ith pos one cell to the right
			for (size_t j = cursor->size; j > i; j--)
			{
				cursor->key[j] = cursor->key[j - 1];
				cursor->value[j] = cursor->value[j - 1];
			}

			cursor->key[i] = x;
			cursor->value[i] = val;
			cursor->size++;

			cursor->ptr[cursor->size] = cursor->ptr[cursor->size - 1]; //Move link to next leaf node to the last ptr
			cursor->ptr[cursor->size - 1] = nullptr;
		}

		else //Can`t insert into node, Overflow
		{
			// Create a newLeaf node
			Node<KeyType, ValueType> *newLeaf = new Node<KeyType, ValueType>;

			KeyType* virtualNode = new KeyType[MAX + 1];
			ValueType* virtualNodeVal = new ValueType[MAX + 1];

			// Update cursor to virtual
			// node created
			for (size_t i = 0; i < MAX; i++)
			{
				virtualNode[i] = cursor->key[i];
				virtualNodeVal[i] = cursor->value[i];
			}
			size_t i = 0, j;

			// Traverse to find where the new
			// node is to be inserted
			while (x > virtualNode[i] && i < MAX)
			{
				i++;
			}

			// Update the current virtual
			// Node to its previous
			for (size_t j = MAX + 1; j > i; j--)
			{
				virtualNode[j] = virtualNode[j - 1];
				virtualNodeVal[j] = virtualNodeVal[j - 1];
			}

			virtualNode[i] = x;
			virtualNodeVal[i] = val;
			newLeaf->IS_LEAF = true;

			cursor->size = (MAX + 1) / 2;
			newLeaf->size = MAX + 1 - (MAX + 1) / 2;

			cursor->ptr[cursor->size] = newLeaf;

			newLeaf->ptr[newLeaf->size] = cursor->ptr[MAX];

			cursor->ptr[MAX] = NULL;

			// Update the current virtual
			// Node's key to its previous
			for (i = 0; i < cursor->size; i++)
			{
				cursor->key[i] = virtualNode[i];
				cursor->value[i] = virtualNodeVal[i];
			}

			// Update the newLeaf key to
			// virtual Node
			for (i = 0, j = cursor->size; i < newLeaf->size; i++, j++)
			{
				newLeaf->key[i] = virtualNode[j];
				newLeaf->value[i] = virtualNodeVal[j];
			}

			// If cursor is the root node
			if (cursor == root)
			{

				// Create a new Node
				Node<KeyType, ValueType> *newRoot = new Node<KeyType, ValueType>;

				// Update rest field of
				// B+ Tree Node
				newRoot->key[0] = newLeaf->key[0];
				newRoot->ptr[0] = cursor;
				newRoot->ptr[1] = newLeaf;
				newRoot->IS_LEAF = false;
				newRoot->size = 1;
				root = newRoot;
			}
			else
			{
				// Recursive Call for
				// insert in internal
				insertInternal(newLeaf->key[0], parent, newLeaf);
			}
		}
	}
}

template<typename KeyType, typename ValueType>
void BPTree<KeyType, ValueType>::display(Node<KeyType, ValueType>* _root) 
{
	if(_root == nullptr) return;

	for(size_t i = 0; i < _root->size; i++)
	{
		std::cout << _root->key << ' ';
	}
	std::cout << '\n';

	if(_root->IS_LEAF)
	{
		for(size_t i = 0; i < _root->size; i++)
		{
			std::cout << _root->value << ' ';
		}
		std::cout << '\n';
		return;
	}
	
	for(size_t i = 0; i < _root->size + 1; i++)
	{
		display(root->ptr[i]);
	}
}

// Function to implement the Insert
// Internal Operation in B+ Tree
template <typename KeyType, typename ValueType>
void BPTree<KeyType, ValueType>::insertInternal(int x, Node<KeyType, ValueType>* cursor, Node<KeyType, ValueType>* child)
{
    // If we dont have overflow
    if (cursor->size < MAX) 
	{
        size_t i = 0;
 
        // Traverse the child node
        // for current cursor node
        while (x > cursor->key[i] && i < cursor->size) 
		{
            i++;
        }
 
        // Traverse the cursor node
        // and update the current key
        // to its previous node key
        for (size_t j = cursor->size; j > i; j--)
		{
            cursor->key[j] = cursor->key[j - 1];
        }
 
        // Traverse the cursor node
        // and update the current ptr
        // to its previous node ptr
        for (size_t j = cursor->size + 1; j > i + 1; j--) 
		{
            cursor->ptr[j] = cursor->ptr[j - 1];
        }
 
        cursor->key[i] = x;
        cursor->size++;
        cursor->ptr[i + 1] = child;
    }
 
    // For overflow, break the node
    else {
        // For new Interval
        Node<KeyType, ValueType>* newInternal = new Node<KeyType, ValueType>;
        KeyType* virtualKey = new KeyType[MAX + 1];
        Node<KeyType, ValueType>* virtualPtr = new Node<KeyType, ValueType>[MAX + 2];
 
        // Insert the current list key
        // of cursor node to virtualKey
		size_t i = 0;
        for (i; i < MAX; i++) 
		{
            virtualKey[i] = cursor->key[i];
            virtualPtr[i] = cursor->ptr[i];
        }
        virtualPtr[i+1] = cursor->ptr[i+1];

        // // Insert the current list ptr
        // // of cursor node to virtualPtr
        // for (int i = 0; i < MAX + 1; i++) {
        // }
 
        i = 0;
 
        // Traverse to find where the new
        // node is to be inserted
        while (x > virtualKey[i] && i < MAX) 
		{
            i++;
        }
 
        // Traverse the virtualKey node
        // and update the current key
        // to its previous node key
        for (size_t j = MAX + 1; j > i; j--) 
		{
            virtualKey[j] = virtualKey[j - 1];
        }
 
        virtualKey[i] = x;
 
        // Traverse the virtualKey node
        // and update the current ptr
        // to its previous node ptr
        for (size_t j = MAX + 2; j > i + 1; j--) 
		{
            virtualPtr[j] = virtualPtr[j - 1];
        }
 
        virtualPtr[i + 1] = child;
        newInternal->IS_LEAF = false;
 
        cursor->size = (MAX + 1) / 2;
 
        newInternal->size = MAX - (MAX + 1) / 2;
 
        // Insert new node as an
        // internal node
		size_t j;
        for (i = 0, j = cursor->size + 1; i < newInternal->size; i++, j++) 
		{
            newInternal->key[i] = virtualKey[j];
        }
 
        for (i = 0, j = cursor->size + 1; i < newInternal->size + 1; i++, j++) //-------!!!!!!!!!!!!!!
		{
            newInternal->ptr[i] = virtualPtr[j];
        }
 
        // If cursor is the root node
        if (cursor == root) 
		{
            // Create a new root node
            Node<KeyType, ValueType>* newRoot = new Node<KeyType, ValueType>;
 
            // Update key value
            newRoot->key[0] = cursor->key[cursor->size];
 
            // Update rest field of
            // B+ Tree Node
            newRoot->ptr[0] = cursor;
            newRoot->ptr[1] = newInternal;
            newRoot->IS_LEAF = false;
            newRoot->size = 1;
            root = newRoot;
        }
 
        else {
            // Recursive Call to insert
            // the data
            insertInternal(cursor->key[cursor->size], findParent(root,cursor), newInternal);
        }
    }
}

// Function to find the parent node
template <typename KeyType, typename ValueType>
Node<KeyType, ValueType>* BPTree<KeyType, ValueType>::findParent(Node<KeyType, ValueType>* cursor, Node<KeyType, ValueType>* child)
{
    Node<KeyType, ValueType>* parent;
 
    // If cursor reaches the end of Tree
    if (cursor->IS_LEAF || (cursor->ptr[0])->IS_LEAF)
	{
        return nullptr;
    }
 
    // Traverse the current node with
    // all its child
    for (size_t i = 0; i < cursor->size + 1; i++) 
	{
        // Update the parent for the
        // child Node
        if (cursor->ptr[i] == child) {
            parent = cursor;
            return parent;
        }
 
        // Else recursively traverse to
        // find child node
        else 
		{
            parent = findParent(cursor->ptr[i], child);
 
            // If parent is found, then
            // return that parent node
            if (parent != NULL)
                return parent;
        }
    }
 
    // Return parent node
    return parent;
}


---------------------------------------------------------------------------------------


template <typename KeyType, typename ValueType>
void BPTree<KeyType, ValueType>::remove(const KeyType& x)
{
	if (root == nullptr)
		return;

	Node *cursor = root;
	Node *parent;
	size_t leftSibling, rightSibling;
	//Find node
	while (cursor->IS_LEAF == false)
	{
		for (size_t i = 0; i < cursor->size; i++)
		{
			parent = cursor;
			leftSibling = i - 1;
			rightSibling = i + 1;
			if (x < cursor->key[i])
			{
				cursor = cursor->ptr[i];
				break;
			}
			if (i == cursor->size - 1)
			{
				leftSibling = i;
				rightSibling = i + 2;
				cursor = cursor->ptr[i + 1];
				break;
			}
		}
	}
	bool found = false;
	size_t pos;
	//Search keys
	for (pos = 0; pos < cursor->size; pos++)
	{
		if (cursor->key[pos] == x)
		{
			found = true;
			break;
		}
	}
	if (!found)
	{
		cout << "Not found\n";
		return;
	}

	//Remove key and rearange array
	for (size_t i = pos; i < cursor->size; i++)
	{
		cursor->key[i] = cursor->key[i + 1];
		cursor->value[i] = cursor->value[i + 1];
	}
	cursor->size--;
	cursor->ptr[cursor->size] = cursor->ptr[cursor->size + 1];
	cursor->ptr[cursor->size + 1] = nullptr;

	if (cursor == root) //-------------------------------------
	{
		for (size_t i = 0; i < MAX + 1; i++)
		{
			cursor->ptr[i] = nullptr;
		}
		if (cursor->size == 0)
		{
			cout << "Tree died\n";
			delete[] cursor->key;
			delete[] cursor->value;
			delete[] cursor->ptr;
			delete cursor;
			root = nullptr;
		}
		return;
	}

	//Minimum children number requirement is met so no rebalancing is needed
	if (cursor->size >= (MAX + 1) / 2)
	{
		return;
	}

	//If there is left sibling
	if (leftSibling >= 0)
	{
		Node* leftNode = parent->ptr[leftSibling];
		if (leftNode->size >= (MAX + 1) / 2 + 1) //If left sibling has a key to share 
		{
			//Move all keys to the left
			for (size_t i = cursor->size; i > 0; i--)
			{
				cursor->key[i] = cursor->key[i - 1];
				cursor->value[i] = cursor->value[i - 1];
			}
			cursor->size++;
			cursor->ptr[cursor->size] = cursor->ptr[cursor->size - 1];
			cursor->ptr[cursor->size - 1] = nullptr;
			cursor->key[0] = leftNode->key[leftNode->size - 1]; //get last key from left sibling
			cursor->value[0] = leftNode->value[leftNode->size - 1];
			leftNode->size--;
			leftNode->ptr[leftNode->size] = cursor;
			leftNode->ptr[leftNode->size + 1] = nullptr;
			parent->key[leftSibling] = cursor->key[0]; //Change parent search key
			return;
		}
	}

	//If there is right sibling
	if (rightSibling <= parent->size)
	{
		Node* rightNode = parent->ptr[rightSibling];
		if (rightNode->size >= (MAX + 1) / 2 + 1) //If right sibling can give one key
		{
			cursor->size++;
			cursor->ptr[cursor->size] = cursor->ptr[cursor->size - 1];
			cursor->ptr[cursor->size - 1] = nullptr;
			cursor->key[cursor->size - 1] = rightNode->key[0]; //Get first key of right sibling
			cursor->value[cursor->size - 1] = rightNode->value[0];
			rightNode->size--;
			rightNode->ptr[rightNode->size] = rightNode->ptr[rightNode->size + 1];
			rightNode->ptr[rightNode->size + 1] = nullptr;
			for (size_t i = 0; i < rightNode->size; i++) //Rearange right node
			{
				rightNode->key[i] = rightNode->key[i + 1];
				rightNode->value[i] = rightNode->value[i + 1];
			}
			parent->key[rightSibling - 1] = rightNode->key[0]; //Change parent search key
			return;
		}
	}

	if (leftSibling >= 0) //There is left node but doesn`t have have a key to share
	{
		Node *leftNode = parent->ptr[leftSibling];
		for (size_t i = leftNode->size, j = 0; j < cursor->size; i++, j++) //Fill left node with cursor`s content
		{
			leftNode->key[i] = cursor->key[j];
			leftNode->value[i] = cursor->value[j];
		}
		// leftNode->ptr[leftNode->size] = nullptr;
		leftNode->size += cursor->size;
		leftNode->ptr[leftNode->size] = cursor->ptr[cursor->size];
		removeInternal(parent->key[leftSibling], parent, cursor); //Search inner nodes to remove instances of the key
		//Remove cursor node
		delete[] cursor->key;
		delete[] cursor->value;
		delete[] cursor->ptr;
		delete cursor;
	}
	else if (rightSibling <= parent->size) //There is right node but has to keys to share
	{
		Node *rightNode = parent->ptr[rightSibling];
		for (size_t i = cursor->size, j = 0; j < rightNode->size; i++, j++) //Merge the two nodes into cursor
		{
			cursor->key[i] = rightNode->key[j];
			cursor->value[i] = rightNode->value[j];
		}
		// cursor->ptr[cursor->size] = nullptr;
		cursor->ptr[cursor->size] = rightNode->ptr[rightNode->size];
		cursor->size += rightNode->size;
		removeInternal(parent->key[rightSibling - 1], parent, rightNode); //Search inner nodes to remove instances of the key
		delete[] rightNode->key;
		delete[] rightNode->value;
		delete[] rightNode->ptr;
		delete rightNode;
	}
}

template <typename KeyType, typename ValueType>
void BPTree<KeyType, ValueType>::removeInternal(const KeyType& x, Node *cursor, Node *child)
{
	if (cursor == root && cursor->size == 1)
	{
		if (cursor->ptr[1] == child) //Remove old root containing old key
		{
			delete[] child->key;
			delete[] child->value;
			delete[] child->ptr;
			delete child;
			root = cursor->ptr[0]; //Make left child new root
			delete[] cursor->key;
			delete[] cursor->value;
			delete[] cursor->ptr;
			delete cursor;
			return;
		}
		else if (cursor->ptr[0] == child)
		{
			delete[] child->key;
			delete[] child->value;
			delete[] child->ptr;
			delete child;
			root = cursor->ptr[1];
			delete[] cursor->key;
			delete[] cursor->value;
			delete[] cursor->ptr;
			delete cursor;
			return;
		}
	}

	//Find key pos in parent
	size_t pos;
	for (pos = 0; pos < cursor->size; pos++)
	{
		if (cursor->key[pos] == x)
		{
			break;
		}
	}

	//Move arr 1 position to the left from pos of old key
	for (size_t i = pos; i < cursor->size; i++)
	{
		cursor->key[i] = cursor->key[i + 1];
	}

	//Find pos of child in parent ptr arr
	for (pos = 0; pos < cursor->size + 1; pos++)
	{
		if (cursor->ptr[pos] == child)
		{
			break;
		}
	}

	//Move arr 1 position to the left from pos of old key ptr
	for (size_t i = pos; i < cursor->size + 1; i++)
	{
		cursor->ptr[i] = cursor->ptr[i + 1];
	}
	cursor->size--;

	//If internal node meets mimimal num of children requirements return
	if (cursor->size >= (MAX + 1) / 2 - 1)
	{
		return;
	}

	//Root has no minimal requirements
	if (cursor == root)
		return;

	Node *parent = findParent(root, cursor);
	size_t leftSibling, rightSibling;
	for (pos = 0; pos < parent->size + 1; pos++)
	{
		if (parent->ptr[pos] == cursor)
		{
			leftSibling = pos - 1;
			rightSibling = pos + 1;
			break;
		}
	}
	if (leftSibling >= 0)
	{
		Node *leftNode = parent->ptr[leftSibling];
		if (leftNode->size >= (MAX + 1) / 2)
		{
			for (size_t i = cursor->size; i > 0; i--)
			{
				cursor->key[i] = cursor->key[i - 1];
			}
			cursor->key[0] = parent->key[leftSibling];
			parent->key[leftSibling] = leftNode->key[leftNode->size - 1];
			for (size_t i = cursor->size + 1; i > 0; i--)
			{
				cursor->ptr[i] = cursor->ptr[i - 1];
			}
			cursor->ptr[0] = leftNode->ptr[leftNode->size];
			cursor->size++;
			leftNode->size--;
			return;
		}
	}
	if (rightSibling <= parent->size)
	{
		Node *rightNode = parent->ptr[rightSibling];
		if (rightNode->size >= (MAX + 1) / 2)
		{
			cursor->key[cursor->size] = parent->key[pos];
			parent->key[pos] = rightNode->key[0];
			for (size_t i = 0; i < rightNode->size - 1; i++)
			{
				rightNode->key[i] = rightNode->key[i + 1];
			}
			cursor->ptr[cursor->size + 1] = rightNode->ptr[0];
			for (size_t i = 0; i < rightNode->size; ++i)
			{
				rightNode->ptr[i] = rightNode->ptr[i + 1];
			}
			cursor->size++;
			rightNode->size--;
			return;
		}
	}
	if (leftSibling >= 0)
	{
		Node *leftNode = parent->ptr[leftSibling];
		leftNode->key[leftNode->size] = parent->key[leftSibling];
		for (size_t i = leftNode->size + 1, j = 0; j < cursor->size; j++)
		{
			leftNode->key[i] = cursor->key[j];
		}
		for (size_t i = leftNode->size + 1, j = 0; j < cursor->size + 1; j++)
		{
			leftNode->ptr[i] = cursor->ptr[j];
			cursor->ptr[j] = nullptr;
		}
		leftNode->size += cursor->size + 1;
		cursor->size = 0;
		removeInternal(parent->key[leftSibling], parent, cursor);
	}
	else if (rightSibling <= parent->size)
	{
		Node *rightNode = parent->ptr[rightSibling];
		cursor->key[cursor->size] = parent->key[rightSibling - 1];
		for (size_t i = cursor->size + 1, j = 0; j < rightNode->size; j++)
		{
			cursor->key[i] = rightNode->key[j];
		}
		for (size_t i = cursor->size + 1, j = 0; j < rightNode->size + 1; j++)
		{
			cursor->ptr[i] = rightNode->ptr[j];
			rightNode->ptr[j] = nullptr;
		}
		cursor->size += rightNode->size + 1;
		rightNode->size = 0;
		removeInternal(parent->key[rightSibling - 1], parent, rightNode);
	}
}