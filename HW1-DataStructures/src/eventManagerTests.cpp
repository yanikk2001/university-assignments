#define CATCH_CONFIG_MAIN

#include "catch.h"
#include "eventManager.h"

TEST_CASE("Adding new data")
{
    EventManager eventManager;

    SECTION("Add a client")
    {
        eventManager.add(Client{0, 10, 20 , 10});
        auto event = eventManager.getEvents()[0];
        REQUIRE(event.bananas == 10);
        REQUIRE(event.schweppes == 20);
    }

    SECTION("Add a Worker Event")
    {
        eventManager.add(WorkerEvent(EventType::WorkerSend, 10, ResourceType::banana));
        auto event = eventManager.getEvents()[0];
        REQUIRE(event.type == EventType::WorkerSend);
        REQUIRE(event.minute == 10);
        REQUIRE(event.resource == ResourceType::banana);
    }

    SECTION("Add a Worker depart event using delivery order")
    {
        eventManager.add(deliveryOrder{34, ResourceType::schweppes});
        auto event = eventManager.getEvents()[0];
        REQUIRE(event.type == EventType::WorkerBack);
        REQUIRE(event.minute == 34);
        REQUIRE(event.resource == ResourceType::schweppes);
    }

    SECTION("Add vector of clients")
    {
        std::vector<Client> clients = {
            Client{0, 0, 0, 10},
            Client{1, 1, 1, 10},
            Client{2, 2, 2, 10},
            };
        eventManager.add(clients);
        auto vec = eventManager.getEvents();
        for(int i = 0; i < vec.size(); i++)
        {
            REQUIRE(vec[i].bananas == i);
            REQUIRE(vec[i].schweppes == i);
        }
    }
}

TEST_CASE("Valid order")
{
    EventManager eventManager;

    SECTION("When multiple events at one minute")
    {
        std::vector<Client> clients = {
            Client{0, 0, 0, 10},
            Client{1, 1, 1, 10},
            Client{2, 2, 2, 10},
            };
        
        eventManager.add(clients);

        eventManager.add(WorkerEvent(EventType::WorkerSend, 10, ResourceType::banana));
        eventManager.add(deliveryOrder{10, ResourceType::banana});

        eventManager.add(WorkerEvent(EventType::WorkerSend, 11, ResourceType::schweppes));

        auto events = eventManager.getEvents();

        REQUIRE(events[0].type == EventType::WorkerSend);
        REQUIRE(events[1].type == EventType::WorkerBack);
        REQUIRE(events[2].type == EventType::ClientDepart);
        REQUIRE(events[3].type == EventType::WorkerSend);
        REQUIRE(events[4].type == EventType::ClientDepart);
        REQUIRE(events[5].type == EventType::ClientDepart);
    }
}