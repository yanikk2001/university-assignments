#pragma once

const int C_RESTOCK_TIME = 60;
const int C_RESTOCK_AMOUNT = 100;

enum class ResourceType {
	banana,
	schweppes,
};

struct deliveryOrder 
{
    int arrivalMinute;
    ResourceType resourse;
};

enum EventType {
	WorkerSend, WorkerBack, ClientDepart
};

struct WorkerEvent 
{
    EventType type;
    int minute;
    ResourceType resourse;

    WorkerEvent(const deliveryOrder& order)
    {
        type = EventType::WorkerBack;
        minute = order.arrivalMinute;
        resourse = order.resourse;
    }

    WorkerEvent(const EventType& type, const int& minute, const ResourceType& resource) : 
                type(type), minute(minute), resourse(resourse){}
};