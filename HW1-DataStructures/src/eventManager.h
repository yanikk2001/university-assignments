#pragma once

#include "interface.h"
#include "./Data_Structures/heap.h"
#include "ResourceTypes.h"
#include <iostream>

struct Event
{
    EventType type;
    int minute;
    ResourceType resource;
    std::size_t index;
    int bananas;
    int schweppes;

    bool operator>(const Event &other)
    {
        if (this->minute == other.minute)
        {
            if(type == EventType::ClientDepart)
                return (this->index > other.index);
            return (this->type > other.type);
        }
        return (this->minute > other.minute);
    }

    bool operator<(const Event &other)
    {
        if (this->minute == other.minute)
        {
            if(type == EventType::ClientDepart)
                return (this->index < other.index);
            return (this->type < other.type);
        }
        return (this->minute < other.minute);
    }
};

class EventManager
{
    heap<MIN, Event> events; //the Event with min minute and highest priority is at the top 

public:
    EventManager() = default;
    EventManager(const EventManager &other) = delete;
    EventManager &operator=(const EventManager &other) = delete;
    ~EventManager() = default;

    void add(const Client &client);
    void add(const WorkerEvent &order);
    void add(std::vector<Client> &clients);
    void add(std::vector<WorkerEvent> &workerEvents);

    //Returns sorted by priority events vector
    std::vector<Event> getEvents();
};
