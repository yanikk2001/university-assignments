#pragma once

#include "interface.h"
#include "./Data_Structures/LinkedQueue.h"
#include "delivery.h"
#include "waitingList.inc"
#include "eventManager.h"
#include <iostream>
#include <algorithm>
#include <vector>

struct MyStore : Store {
public:
	ActionHandler *actionHandler = nullptr;

	void setActionHandler(ActionHandler *handler) override;

	void init(int workerCount, int startBanana, int startSchweppes) override;

	void addClients(const Client *clients, int count) override;

	void advanceTo(int minute) override;
	
	virtual int getBanana() const;

	virtual int getSchweppes() const;

	~MyStore(){delete nonProcessedClients;}

private:
	int bananasSupply;
	int schweppesSupply;
	int availableWorkers;
	
	std::size_t lastClientIndex = 0;

	//Stores here clients when adding them
	Queue<Client>* nonProcessedClients = new LinkedQueue<Client>;

	//Consists of all client arrival, depart minute - 1 , depart times
	std::vector<int> keyMoments;

	//Manages all delivery orders
	Delivery deliveryManager;

	//Those clients that cannot be satisfied on arrival are stored here 
	WaitingList waitingList;

	//Collects all events (WorkerSend, WorkerBack, ClientDepart) and organised them according to priority
	EventManager eventManager;

	void storeKeyMoments(const Client& client);
	
	//Generates all events that happened in that minute
	void eventsBy(const int& minute);
	
	//If store has all needed supplies return true 
	bool canServeClient(const Client& client);
	
	/*
		Checks if a client can be served, makes new delivery orders and events
		@param minute the minute the clients arrived 
	*/
	void processNewClients(const int& minute);

	//Update store supply from all deliveries that have been delivered at minute
	void resupplyFromDeliveries(const int& minute);

	//Checks if currentSupply + expectedSupplyFromDeliveries >= clientNeeds
	int willHaveSuppliesByClientDeparture(const Client& client);

	/* 
		Adds new delivery to delivery object
		@param minute delivery order time
		@param client which client triggered the delivery
		@param neededResource it is presented by number (if number == 1: need schweppes), (number == 0: need bananas), (number == -1: need both), (number == 2: don`t need anything)
	*/
	void newDelivery(const int& minute, const Client& client, const int& neededResource);

	//Based on delivery resupply amount determines how many workers have returned
	void calculateAvailableWorkers(const int& delivery);

	//Subtracts from store supplies what clients take with them when departing
	void updateSupplies(Client* clients, int size);

	/*
		Uses actionHandler`s methods
		@param events organized by priority vector of all events that have happened
	*/
	void printEvents(const std::vector<Event>& events);

	//Checks for expired clients and if there are any updates store supplies
	void updateExpiredClients(const int& minute);

	//Checks for waiting clients and if there are any updates store supplies
	void updateWaitingClients(const int& minute);
};