#pragma once

#include "./Data_Structures/LinkedQueue.h"
#include "interface.h"

class WaitingList : public LinkedQueue<Client>
{
    int departTime(const Client& client)
    {
        return client.arriveMinute + client.maxWaitTime;
    }
public:
    std::vector<Client> removeExpiredClients(const int& minute)
    {
        std::vector<Client>ServedClients;
        while(!this->isEmpty() && departTime(this->first()) <= minute)
        {
            Client client = this->first();
            client.departTime = departTime(client);
            ServedClients.push_back(client);
            this->dequeue();
        }

        return ServedClients;
    }

    std::vector<Client> checkClients(const int& bananaSupply, const int& schweppesSupply, const int& minute)
    {
        std::vector<Client>ServedClients;
        while(!this->isEmpty() && (this->first().banana <= bananaSupply && this->first().schweppes <= schweppesSupply))
        {
            Client client = this->first();
            client.departTime = minute;
            ServedClients.push_back(client);
            this->dequeue();
        }

        return ServedClients;
    }
};