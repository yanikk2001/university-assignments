#pragma once

#include "./Data_Structures/LinkedQueue.h"
#include "ResourceTypes.h"

#include <vector>

struct resupply
{
	int bananas = 0;
	int schweppes = 0;
};

class Delivery
{
    Queue<deliveryOrder>* deliveries = new LinkedQueue<deliveryOrder>;
    std::vector<deliveryOrder>ComletedOrders;

    resupply calculateReSupply(Queue<deliveryOrder>* data, const int& minute, bool createActionHandlers = false);

public:
    Delivery() = default;
    ~Delivery(){delete deliveries;}
    Delivery(const Delivery& other) = delete; //Do not want more than 1 delivery object per store
    Delivery& operator=(const Delivery& other) = delete;

    /*
        Adds new delivery order to queue
        @param departMinute when the order was made
        @param resource needed resource 
    */
    void addNew(const int departMinute, ResourceType resource);

    /*
        Checks all completed deliveries in queue by minute and removes them
        @return resupply object containing the number of all bananas and schweppes delivered from orders
    */
    resupply checkCompleted(const int& minute);

    //Only checks if all completed deliveries by minute but does not remove them from queue
    resupply getSupplyAtMinute(const int& minute);

    deliveryOrder getTop() const;
    std::vector<deliveryOrder> getCompletedOrders() const;
};