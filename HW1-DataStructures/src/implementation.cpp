#include "implementation.h"

/// This is sample empty implementation you can place your solution here or delete this and include tests to your solution

void MyStore::setActionHandler(ActionHandler *handler)
{
	actionHandler = handler;
}

void MyStore::init(int workerCount, int startBanana, int startSchweppes)
{
	bananasSupply = startBanana;
	schweppesSupply = startSchweppes;
	availableWorkers = workerCount;
}

void MyStore::addClients(const Client *clients, int count)
{
	for (int i = 0; i < count; i++)
	{
		Client *client = const_cast<Client *>(&clients[i]);
		client->index = lastClientIndex;
		lastClientIndex++;
		nonProcessedClients->enqueue(clients[i]);
		storeKeyMoments(clients[i]);
	}
	std::sort(keyMoments.begin(), keyMoments.end());
}

void MyStore::advanceTo(int minute)
{
	if (keyMoments.empty())
		return;

	for (auto i = keyMoments.begin(); *i <= minute && i != keyMoments.end(); i++)
	{
		try
		{
			eventsBy(*i);
		}
		catch (const std::runtime_error &e)
		{
			std::cerr << e.what() << '\n';
			return;
		}
	}
}

int MyStore::getBanana() const
{
	return bananasSupply;
}

int MyStore::getSchweppes() const
{
	return schweppesSupply;
}

void MyStore::storeKeyMoments(const Client &client)
{
	keyMoments.push_back(client.arriveMinute);
	int clientExpiration = client.arriveMinute + client.maxWaitTime;
	if (clientExpiration > 0)
		keyMoments.push_back(clientExpiration - 1);
	keyMoments.push_back(clientExpiration);
}

void MyStore::eventsBy(const int &minute)
{
	// Save supply count before updating it
	int startBananaSupply = bananasSupply;
	int startSchweppesSupply = schweppesSupply;

	// Delivery check
	try
	{
		resupplyFromDeliveries(minute);
	}
	catch (const std::runtime_error &e)
	{
		std::cerr << e.what() << '\n';
		return;
	}

	updateExpiredClients(minute);

	updateWaitingClients(minute);

	processNewClients(minute);

	try
	{
		printEvents(eventManager.getEvents());
	}
	catch (const std::runtime_error &e)
	{
		throw e;
	}
}

bool MyStore::canServeClient(const Client &client)
{
	return (client.banana <= bananasSupply && client.schweppes <= schweppesSupply);
}

void MyStore::processNewClients(const int &minute)
{
	if (!nonProcessedClients)
		return;

	while (!nonProcessedClients->isEmpty() && nonProcessedClients->first().arriveMinute <= minute)
	{
		auto client = nonProcessedClients->first();
		if (canServeClient(client))
		{
			client.departTime = minute;
			// Add on depart event to eventHandler
			eventManager.add(client);
			updateSupplies(&client, 1);
		}
		else
		{
			int neededSupplies;
			try
			{
				neededSupplies = willHaveSuppliesByClientDeparture(client);
			}
			catch(const std::bad_cast& e)
			{
				std::cerr << e.what() << '\n';
				return;
			}
			
			if (neededSupplies != 2)
			{
				//	add new delivery to Delivery object
				newDelivery(minute, client, neededSupplies);
			}
			//	Add client to waiting list
			waitingList.enqueue(client);
		}
		nonProcessedClients->dequeue();
	}
}

void MyStore::calculateAvailableWorkers(const int &delivery)
{
	availableWorkers += delivery / C_RESTOCK_AMOUNT; // 1 worker per delivery. A delivery restocks 1 RESTOCK_AMOUNT.
}

void MyStore::updateSupplies(Client *clients, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (bananasSupply - clients[i].banana < 0) // If the supply won`t be enough to satisfy client`s needs
		{
			clients[i].banana = bananasSupply; // Client takes what he can from the remaining supply
			bananasSupply = 0;
		}
		else
			bananasSupply -= clients[i].banana; // If we have enough supply client leaves with wanted number stock

		if (schweppesSupply - clients[i].schweppes < 0)
		{
			clients[i].schweppes = schweppesSupply;
			schweppesSupply = 0;
		}
		else
			schweppesSupply -= clients[i].schweppes;
	}
}

void MyStore::printEvents(const std::vector<Event> &events)
{
	if (!actionHandler)
		throw std::runtime_error("No action handler set");

	for (Event event : events)
	{
		switch (event.type)
		{
		case WorkerSend:
			actionHandler->onWorkerSend(event.minute, event.resource);
			break;

		case WorkerBack:
			actionHandler->onWorkerBack(event.minute, event.resource);
			break;

		case ClientDepart:
			actionHandler->onClientDepart(event.index, event.minute, event.bananas, event.schweppes);
			break;

		default:
			break;
		}
	}
}

void MyStore::updateExpiredClients(const int &minute)
{
	// check waiting list
	auto expiredClients = waitingList.removeExpiredClients(minute);
	if (!expiredClients.empty())
	{
		updateSupplies(expiredClients.data(), expiredClients.size());
		eventManager.add(expiredClients);
	}
}

void MyStore::updateWaitingClients(const int &minute)
{
	std::vector<Client> servedClients;
	auto deliveries = deliveryManager.getCompletedOrders();
	for (auto delivery : deliveries)
	{
		eventManager.add(WorkerEvent(delivery));
		std::vector<Client> moreServedClients = waitingList.checkClients(bananasSupply, schweppesSupply, delivery.arrivalMinute);
		if (!moreServedClients.empty())
			servedClients.insert(servedClients.end(), moreServedClients.begin(), moreServedClients.end());
	}
	updateSupplies(servedClients.data(), servedClients.size());
	eventManager.add(servedClients);
}

void MyStore::resupplyFromDeliveries(const int &minute)
{
	resupply res;
	try
	{
		res = deliveryManager.checkCompleted(minute);
	}
	catch (const std::runtime_error &e)
	{
		throw e;
	}

	calculateAvailableWorkers(res.bananas);
	calculateAvailableWorkers(res.schweppes);
	bananasSupply += res.bananas;
	schweppesSupply += res.schweppes;
}

int MyStore::willHaveSuppliesByClientDeparture(const Client &client)
{
	int clientDepartTime = client.arriveMinute + client.maxWaitTime;
	resupply res;
	try
	{
		res = deliveryManager.getSupplyAtMinute(clientDepartTime);
	}
	catch(const std::bad_cast& e)
	{
		throw e;
	}
	
	// Will have enough schweppes
	bool enoughSchweppes = schweppesSupply + res.schweppes >= client.schweppes;
	// Will have enough bananas
	bool enoughBananas = bananasSupply + res.bananas >= client.banana;

	if (enoughBananas && enoughSchweppes)
		return 2; // Will have both bananas and schweppes
	else if (enoughBananas && !enoughSchweppes)
		return 1; // Only bananas
	else if (!enoughBananas && enoughSchweppes)
		return 0; // Only schwepps
	else
		return -1; // Will not have neither
}

void MyStore::newDelivery(const int &minute, const Client &client, const int &neededResource)
{
	WorkerEvent sendWorkerEvent(EventType::WorkerSend, minute, ResourceType::banana);

	if (availableWorkers <= 0)
	{
		return;
	}
	else if (availableWorkers == 1)
	{
		if (neededResource == 1) // need schweppes
		{
			deliveryManager.addNew(minute, ResourceType::schweppes);
			sendWorkerEvent.resourse = ResourceType::schweppes;
			eventManager.add(sendWorkerEvent);
		}
		if (neededResource == 0) // need bananas
		{
			deliveryManager.addNew(minute, ResourceType::banana);
			sendWorkerEvent.resourse = ResourceType::banana;
			eventManager.add(sendWorkerEvent);
		}
		else // Need both resources but only one 1 worker - order bananas
		{
			deliveryManager.addNew(minute, ResourceType::banana);
			sendWorkerEvent.resourse = ResourceType::banana;
			eventManager.add(sendWorkerEvent);
		}

		availableWorkers--;
	}
	else //Have more than 1 worker available
	{
		if (neededResource == -1) //Need both bananas and schweppes
		{
			deliveryManager.addNew(minute, ResourceType::banana);
			sendWorkerEvent.resourse = ResourceType::banana;
			eventManager.add(sendWorkerEvent);

			deliveryManager.addNew(minute, ResourceType::schweppes);
			sendWorkerEvent.resourse = ResourceType::schweppes;
			eventManager.add(sendWorkerEvent);

			availableWorkers -= 2;
		}
		else if (neededResource == 1)
		{
			deliveryManager.addNew(minute, ResourceType::schweppes);
			sendWorkerEvent.resourse = ResourceType::schweppes;
			eventManager.add(sendWorkerEvent);
			availableWorkers--;
		}
		else if (neededResource == 0)
		{
			deliveryManager.addNew(minute, ResourceType::banana);
			sendWorkerEvent.resourse = ResourceType::banana;
			eventManager.add(sendWorkerEvent);
			availableWorkers--;
		}
	}
}

Store *createStore()
{
	return new MyStore();
}