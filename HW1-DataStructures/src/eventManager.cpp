#include "eventManager.h"

void EventManager::add(const Client& client)
{
    Event event;
    event.type = ClientDepart;
    event.minute = client.departTime;
    event.index = client.index;
    event.bananas = client.banana;
    event.schweppes = client.schweppes;

    events.insert(event);
}

void EventManager::add(const WorkerEvent& order)
{
    Event event = {order.type, order.minute, order.resourse};
    events.insert(event);
}

void EventManager::add(std::vector<Client>& clients)
{
    for(auto client : clients)
    {
        add(client);
    }
}

void EventManager::add(std::vector<WorkerEvent>& workerEvents)
{
    for(auto event : workerEvents)
    {
        add(event);
    }
}

std::vector<Event> EventManager::getEvents()
{
    std::vector<Event> eventsVec;
    std::size_t size = events.size();
    for(std::size_t i = 0; i < size; i++)
    {
        eventsVec.push_back(events.topElement());
        events.extract();
    }
    return eventsVec;
}
