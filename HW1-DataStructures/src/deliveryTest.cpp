#define CATCH_CONFIG_MAIN

#include "catch.h"
#include "delivery.h"

TEST_CASE("Normal delivery operation")
{
    Delivery delivery;

    SECTION("Add new orders")
    {
        delivery.addNew(1, ResourceType::banana);
        REQUIRE(delivery.getTop().arrivalMinute == 61);
        REQUIRE(delivery.getTop().resourse == ResourceType::banana);

        delivery.addNew(2, ResourceType::schweppes);
        REQUIRE(delivery.getTop().arrivalMinute == 61);
        REQUIRE(delivery.getTop().resourse == ResourceType::banana);
    }
}

TEST_CASE("Getting completed orders at a minute")
{
    Delivery delivery;

    SECTION("All orders are completed")
    {
        delivery.addNew(1, ResourceType::banana);
        delivery.addNew(2, ResourceType::schweppes);
        delivery.addNew(2, ResourceType::schweppes);

        resupply res = delivery.checkCompleted(62);

        REQUIRE(res.bananas == 100);
        REQUIRE(res.schweppes == 200);
    }

    SECTION("Some orders are completed")
    {
        delivery.addNew(1, ResourceType::banana);
        delivery.addNew(2, ResourceType::schweppes);
        delivery.addNew(3, ResourceType::schweppes);

        resupply res = delivery.checkCompleted(61);

        REQUIRE(res.bananas == 100);
        REQUIRE(res.schweppes == 0);
        REQUIRE(delivery.getTop().arrivalMinute == 62);
        REQUIRE(delivery.getTop().resourse == ResourceType::schweppes);
    }

    SECTION("None orders are completed")
    {
        delivery.addNew(1, ResourceType::banana);
        delivery.addNew(2, ResourceType::schweppes);
        delivery.addNew(3, ResourceType::schweppes);

        resupply res = delivery.checkCompleted(10);

        REQUIRE(res.bananas == 0);
        REQUIRE(res.schweppes == 0);
        REQUIRE(delivery.getTop().arrivalMinute == 61);
        REQUIRE(delivery.getTop().resourse == ResourceType::banana);
    }

    SECTION("Completed and future orders do not intervene with each other")
    {
        delivery.addNew(1, ResourceType::banana);
        delivery.addNew(2, ResourceType::schweppes);
        delivery.addNew(3, ResourceType::schweppes);

        resupply res = delivery.getSupplyAtMinute(62);

        REQUIRE(res.bananas == 100);
        REQUIRE(res.schweppes == 100);

        REQUIRE(delivery.getTop().arrivalMinute == 61);
        REQUIRE(delivery.getTop().resourse == ResourceType::banana);
    }

    SECTION("No deliveries")
    {
        resupply res = delivery.getSupplyAtMinute(62);

        REQUIRE(res.bananas == 0);
        REQUIRE(res.schweppes == 0);

        res = delivery.checkCompleted(62);
        REQUIRE(res.bananas == 0);
        REQUIRE(res.schweppes == 0);
    }
}