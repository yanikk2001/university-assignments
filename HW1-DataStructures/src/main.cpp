#include "interface.h"
#include <vector>

#include <iostream>

#define MINUTES_IN_A_DAY 1440

struct ActionManager : public ActionHandler
{
    void onWorkerSend(int minute, ResourceType resource) 
    {
    std::cout << 'W' << ' ' << minute << ' ' << (int(resource) ? "schweppes" : "banana") << std::endl;
    }

    void onWorkerBack(int minute, ResourceType resource) 
    {
    std::cout << 'D' << ' ' << minute << ' ' << (int(resource) ? "schweppes" : "banana") << std::endl;
    }

    void onClientDepart(int index, int minute, int banana, int schweppes) 
    {
    std::cout << index << ' ' << minute << ' ' << banana << ' ' << schweppes << std::endl;
    }
};

typedef std::vector<Client> ClientList;

int main()
{
    Store* store = createStore();
    ActionHandler* actionManager = new ActionManager;
    store->setActionHandler(actionManager);

    std::size_t workerCount, clientCount;
    std::cin >> workerCount >> clientCount;

    store->init(workerCount,0,0);

    ClientList clients;
    Client client;
    for(std::size_t i = 0; i < clientCount; i++)
    {
        do
        {
            std::cin >> client.arriveMinute >> client.banana >> client.schweppes >> client.maxWaitTime;
        } while ((client.arriveMinute < 0 || client.arriveMinute > MINUTES_IN_A_DAY) || client.banana < 0 || client.schweppes < 0 || client.maxWaitTime < 0 
                    || client.maxWaitTime > MINUTES_IN_A_DAY);
        
        clients.push_back(client);
    }
    store->addClients(clients.data(), clients.size());
    store->advanceTo(MINUTES_IN_A_DAY);
}
