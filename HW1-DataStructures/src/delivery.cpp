#include "delivery.h"

void Delivery::addNew(const int departMinute, ResourceType resource) 
{
    if(!deliveries)
        return;
    
    deliveryOrder order = {departMinute + C_RESTOCK_TIME, resource};

    deliveries->enqueue(order);
}

resupply Delivery::checkCompleted(const int& minute) 
{
    if(!deliveries)
        throw std::runtime_error("failed to allocate delivery");

    return calculateReSupply(deliveries, minute, true);
}

resupply Delivery::getSupplyAtMinute(const int& minute) 
{
    LinkedQueue<deliveryOrder>* cast{};
    try
    {
        cast = dynamic_cast<LinkedQueue<deliveryOrder>*>(deliveries);
    }
    catch(const std::bad_cast& e)
    {
        throw e;
    }
    
    LinkedQueue<deliveryOrder> cpy(*cast);

    return calculateReSupply(&cpy, minute);
}

resupply Delivery::calculateReSupply(Queue<deliveryOrder>* data, const int& minute, bool createActionHandlers)
{
    ComletedOrders.clear();

    resupply order;
    while(!data->isEmpty() && data->first().arrivalMinute <= minute)
    {
        deliveryOrder delivery = data->first(); 
        if(createActionHandlers)
        {
            //Add action handler
            ComletedOrders.push_back(delivery);
        }
        switch (delivery.resourse)
        {
            case ResourceType::banana:
                order.bananas += C_RESTOCK_AMOUNT; 
                break;

            case ResourceType::schweppes:
                order.schweppes += C_RESTOCK_AMOUNT;
                break;
            
            default:
                break;
        }
        data->dequeue();
    }
    return order;
}

deliveryOrder Delivery::getTop() const
{
    return deliveries->first();
}

std::vector<deliveryOrder> Delivery::getCompletedOrders() const
{
    return ComletedOrders;
}
