#pragma once

#include "Queue.h"
#include <stdexcept>

template <typename DataType>
class LinkedQueue : public Queue<DataType>
{
    struct node
    {
        DataType data;
        node* next;
        node(const DataType& data, node* tonext = nullptr) : data(data), next(tonext){}
    };

    node* begin{};
    node* end{};

    void deleteContents();

public:
    LinkedQueue() : begin(nullptr), end(nullptr){}
    LinkedQueue(const LinkedQueue<DataType>& other);
    LinkedQueue<DataType>& operator=(const LinkedQueue<DataType>& other);
    ~LinkedQueue();

    virtual void enqueue(const DataType& data) override;
    virtual void dequeue() override;
    bool isEmpty() const override;
    virtual const DataType& first() const override;
    virtual DataType first() override;
};

#include "LinkedQueue.inc"