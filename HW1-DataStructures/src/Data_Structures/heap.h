//Heap with vector
#include <iostream>
#include <vector>

enum TYPE
{
    MIN,
    MAX
};

template <TYPE HEAPTYPE = MAX, typename T = int>
class heap
{
    std::vector<T> values{};

public: 
    heap() = default;

    heap(const heap& other)
    {
        values = other.values;
    }

    heap<HEAPTYPE, T>& operator=(const heap& other)
    {
        if(this != &other)
        {
            heap cpy(other);
            std::swap(values, cpy.values);
        }
        return *this;
    }

    void insert(const T& value)
    {
        values.push_back(value);
        auto current_element_id = values.size() - 1;
        auto parent_id = getParentId(current_element_id);

        while(parent_id >= 0 && operation(values[parent_id], values[current_element_id], HEAPTYPE))
        {
            std::swap(values[parent_id], values[current_element_id]);
            current_element_id = parent_id;
            parent_id = getParentId(current_element_id);
        }
    }

    void extract()
    {
        if(isEmpty())
            throw std::length_error("Extract empty heap");
        
        std::swap(values[0], values.back());
        values.pop_back();
        heapify(0);
    }

    const std::size_t size() const
    {
        return values.size();
    }

    const bool isEmpty() const 
    {
        return size() == 0;
    }

    T topElement() const
    {
        return values.front();
    }

    void print()
    {
        for(T value : values)
        {
            std::cout << value << " ";
        }
        std::cout << std::endl;
    }

private:
    int getParentId(int id)
    {
        return (id - 1) / 2;
    }

    std::size_t getLeftChild(const std::size_t& id)
    {
        return id * 2 + 1;
    }

    std::size_t getRightChild(const std::size_t& id)
    {
        return id * 2 + 2;
    }

    bool operation(T& val1, T& val2, TYPE type)
    {
        if(type == TYPE::MIN)
            return val1 > val2;
        
        return val1 < val2;
    }

    void heapify(size_t id)
    {
        auto left = getLeftChild(id);
        auto right = getRightChild(id);
        auto n = values.size();
        auto id_to_swap = id;

        if(left < n && operation(values[id_to_swap], values[left], HEAPTYPE))
            id_to_swap = left;
        if(right < n && operation(values[id_to_swap], values[right], HEAPTYPE))
            id_to_swap = right;

        if(id_to_swap == id)
            return;

        std::swap(values[id_to_swap], values[id]);
        heapify(id_to_swap);
    }
};



