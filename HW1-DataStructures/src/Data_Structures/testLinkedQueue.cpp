#define CATCH_CONFIG_MAIN

#include <string>

using namespace std;

#include "LinkedQueue.h"
#include "../../Catch/catch.h"

TEST_CASE("Create queue")
{
    LinkedQueue<string> queue;

    SECTION("Empty queue")
    {

        REQUIRE(queue.isEmpty() == true);

        REQUIRE_THROWS_AS(queue.first(), std::out_of_range);

        REQUIRE_THROWS_AS(queue.dequeue(), std::out_of_range);

    }

    SECTION("Add an element, remove an element")
    {

        queue.enqueue("hello world");

        REQUIRE(queue.isEmpty() == false);
    
        REQUIRE(strcmp(queue.first().data(), "hello world") == 0);

        queue.dequeue();

        REQUIRE(queue.isEmpty() == true);
    }
}

TEST_CASE("Copy queue")
{
    std::cout << "3" << std::endl;

    LinkedQueue<string> queue;
    queue.enqueue("hello");
    queue.enqueue("basic");
    queue.enqueue("white");
    queue.enqueue("world");

    SECTION("Using constructor")
    {

        LinkedQueue<string> cpy(queue);

        REQUIRE(cpy.isEmpty() == false);


        REQUIRE(strcmp(cpy.first().data(), "hello") == 0);

        cpy.dequeue();

        REQUIRE(strcmp(cpy.first().data(), "basic") == 0);
        cpy.dequeue();
        REQUIRE(strcmp(cpy.first().data(), "white") == 0);
        cpy.dequeue();
        REQUIRE(strcmp(cpy.first().data(), "world") == 0);
        cpy.dequeue();
        
        REQUIRE(cpy.isEmpty() == true);
    }

    SECTION("Using operator =")
    {

        LinkedQueue<string> cpy;
        cpy.enqueue("1");
        cpy.enqueue("2");
        cpy.enqueue("3");

        cpy = queue;

        REQUIRE(cpy.isEmpty() == false);


        REQUIRE(strcmp(cpy.first().data(), "hello") == 0);
        cpy.dequeue();

        REQUIRE(strcmp(cpy.first().data(), "basic") == 0);
        cpy.dequeue();
        REQUIRE(strcmp(cpy.first().data(), "white") == 0);
        cpy.dequeue();
        REQUIRE(strcmp(cpy.first().data(), "world") == 0);
        cpy.dequeue();
        
        REQUIRE(cpy.isEmpty() == true);
    }
}