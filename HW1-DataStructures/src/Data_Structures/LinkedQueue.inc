#include "LinkedQueue.h"

template <typename DataType>
inline LinkedQueue<DataType>::LinkedQueue(const LinkedQueue<DataType>& other)
{ 
    node* input_begin = other.begin;
    while(input_begin)
    {
        try
        {
            enqueue(input_begin->data);
        }
        catch(const std::bad_alloc& e)
        {
            deleteContents();
            throw e;
        }
        input_begin = input_begin->next;
    }
}

template<typename DataType>
inline LinkedQueue<DataType>& LinkedQueue<DataType>::operator=(const LinkedQueue<DataType>& other) 
{
    if(this != &other)
    {
        try
        {
            LinkedQueue<DataType> cpy(other);
            swap(begin, cpy.begin);
            swap(end, cpy.end);
        }
        catch(const std::bad_alloc& e)
        {
            throw e;
        }
    }
    return *this;
}

template<typename DataType>
void LinkedQueue<DataType>::deleteContents() 
{
    while(!isEmpty())
    {
        dequeue();
    }
}

template<typename DataType>
LinkedQueue<DataType>::~LinkedQueue() 
{
    deleteContents();
}

template<typename DataType>
inline void LinkedQueue<DataType>::enqueue(const DataType& data) 
{
    node* Node{};
    try
    {
        Node = new node(data);
    }
    catch(const std::bad_alloc& e)
    {
        throw e;
    }
    
    if(isEmpty())
        begin = Node;
    else
        end->next = Node;
    end = Node;
}

template<typename DataType>
inline void LinkedQueue<DataType>::dequeue() 
{
    if(isEmpty())
        throw std::out_of_range("Trying to dequeue an empty queue");
    node* curr = begin;
    begin = begin->next;
    delete curr;
}

template<typename DataType>
inline bool LinkedQueue<DataType>::isEmpty() const 
{
    return begin == nullptr;
}

template<typename DataType>
inline const DataType& LinkedQueue<DataType>::first() const 
{
    if(isEmpty())
    {
        throw std::out_of_range("Trying to interact with an empty queue");
    }
    return begin->data;
}

template<typename DataType>
DataType LinkedQueue<DataType>::first() 
{
    if(isEmpty())
    {
        throw std::out_of_range("Trying to interact with an empty queue");
    }
    return begin->data;
}
