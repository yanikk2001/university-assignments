#pragma once

template <typename DataType>
class Queue
{
    public:
    virtual void enqueue(const DataType& data) = 0;
    virtual void dequeue() = 0;
    virtual const DataType& first() const = 0;
    virtual DataType first() = 0;
    virtual bool isEmpty() const = 0;
    virtual ~Queue() {};
};