#pragma once

#include <mutex>
#include <shared_mutex>
#include <unordered_map>

enum ExecutionPolicy
{
    seq,
    par,
    both
};

// Stored key-value pairs of file descriptor and execution policy
// This class is thread-safe
class ExecutionPolicyStorage
{
private:
    std::unordered_map<int, ExecutionPolicy> policies; // hashmap to store saved policy for each file descriptor (client)
    std::mutex mutex;
    std::shared_mutex shared_mutex;

public:
    ExecutionPolicy getPolicy(int fileDescriptor)
    {
        // Multiple readers can read from the map at the same time
        std::shared_lock<std::shared_mutex> lock(shared_mutex);
        return policies.at(fileDescriptor);
    }

    void setPolicy(int fileDescriptor, ExecutionPolicy policy)
    {
        // Only one writer can write to the map at the same time
        std::scoped_lock<std::mutex> lock(mutex);
        policies[fileDescriptor] = policy;
    }

    void removePolicy(int fileDescriptor)
    {
        std::scoped_lock<std::mutex> lock(mutex);
        policies.erase(fileDescriptor);
    }
};