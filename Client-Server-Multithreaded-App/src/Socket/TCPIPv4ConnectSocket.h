#pragma once
#include "SocketBase.h"
#include <iostream>

#define MAX_BUFFER_SIZE 1024 // Maximum buffer size for receiving and sending data

/**
 * @class TCPIPv4ConnectSocket
 * @brief Represents a TCP/IP v4 connect socket.
 *
 * The TCPIPv4ConnectSocket class provides functionality for creating and managing a TCP/IP v4 connect socket.
 * It allows connecting to a remote server using an IPv4 address and port number.
 * The class provides methods for sending and receiving data over the socket.
 */
class TCPIPv4ConnectSocket : public SocketBase
{
public:
    TCPIPv4ConnectSocket(const char *addr, const char *port) : SocketBase()
    {
        init(addr, port);
    }

    TCPIPv4ConnectSocket() = delete;
    TCPIPv4ConnectSocket(const TCPIPv4ConnectSocket &) = delete;
    TCPIPv4ConnectSocket(const TCPIPv4ConnectSocket &&) = delete;

    void send(const std::string &msg) const
    {
        TCPIPv4ConnectSocket::send(msg, sock_fd);
    }

    void sendStreamData(std::istream &dataStream) const
    {
        TCPIPv4ConnectSocket::sendStreamData(sock_fd, dataStream);
    }

    void recv(std::ostream &os) const
    {
        TCPIPv4ConnectSocket::recv(sock_fd, os);
    }

    static void send(const std::string &msg, int _sock_fd)
    {
        if (_sock_fd == -1)
        {
            throw std::runtime_error("Socket is not initialized");
        }
        if (::send(_sock_fd, msg.c_str(), msg.size() + 1, 0) == -1)
        {
            throwSystemError("send failed");
        }
    }

    /**
     * Sends data from the given input stream over the specified socket.
     *
     * @param _sock_fd The file descriptor of the socket to send data over.
     * @param dataStream The input stream containing the data to be sent.
     *
     * @throws std::runtime_error if the socket is not initialized.
     * @throws std::system_error if there is an error while sending the data.
     */
    static void sendStreamData(int _sock_fd, std::istream &dataStream)
    {
        if (_sock_fd == -1)
        {
            throw std::runtime_error("Socket is not initialized");
        }

        char buffer[MAX_BUFFER_SIZE];

        while (!dataStream.eof())
        {
            dataStream.read(buffer, sizeof(buffer));
            size_t bytesToWrite = dataStream.gcount(); // num of bytes read by buffer

            size_t totalWritten = 0;
            while (totalWritten < bytesToWrite)
            { // If send for some reason doesn't send all the data, we need to keep sending until all the data is sent
                ssize_t bytesWritten = ::send(_sock_fd, buffer + totalWritten, bytesToWrite - totalWritten, 0);
                if (bytesWritten == -1)
                {
                    throwSystemError("Failed to send stream data");
                }
                totalWritten += bytesWritten;
            }
        }
    }

    /**
     * @brief Receives data from a connected socket and writes it to the specified output stream.
     *
     * @param _sock_fd The file descriptor of the socket to receive data from.
     * @param os The output stream to write the received data to.
     *
     * @throws std::runtime_error if the socket is not initialized or if the socket is hung up.
     * @throws std::system_error if the recv function fails.
     */
    static void recv(int _sock_fd, std::ostream &os)
    {
        if (_sock_fd == -1)
        {
            throw std::runtime_error("Socket is not initialized");
        }

        char buffer[MAX_BUFFER_SIZE] = {0};
        while (true)
        {
            memset(buffer, 0, sizeof(buffer)); // Clear the buffer each time
            const int bytesReceived = ::recv(_sock_fd, buffer, sizeof buffer, 0);
            if (bytesReceived <= 0)
            {
                if (bytesReceived == 0)
                {
                    throw std::runtime_error("Socket hung up");
                    shutdown(_sock_fd, 2); // 2 = SHUT_RDWR, Disables further send and receive operations.
                }
                else if (bytesReceived == -1)
                {
                    throwSystemError("recv failed");
                    shutdown(_sock_fd, 2);
                }
            }
            else
            {
                os.write(buffer, bytesReceived);

                if (bytesReceived < sizeof(buffer)) // last bytes received
                    break;
            }
        }
    }

private:
    void init(const char *addr, const char *port)
    {
        struct addrinfo hints, *servinfo, *p;

        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_INET;       // set to AF_INET to force IPv4
        hints.ai_socktype = SOCK_STREAM; // TCP

        // resolve a hostname (such as a website URL or a remote server address)
        if ((getaddrinfo(addr, port, &hints, &servinfo)) != 0)
        {
            throwSystemError("Cannot get address info");
        }

        if (p == nullptr)
        {
            throwSystemError("Cannot connect to the server");
        }

        for (p = servinfo; p != nullptr; p = p->ai_next) // loop through all the results and connect to the first we can
        {
            if ((sock_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1)
            {
                continue;
            }

            if (connect(sock_fd, p->ai_addr, p->ai_addrlen) == -1)
            {
                closeSocket(sock_fd);
                continue;
            }

            break; // if we get here, we must have connected successfully
        }

        freeaddrinfo(servinfo); // all done with this structure, free linked list allocated by getaddrinfo
    }
};