#pragma once

#if defined(_WIN32)
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "ws2_32.lib")
#include <WinSock2.h>
#include <stdlib.h>
#include <Windows.h>
#include <in6addr.h>
#include <WS2tcpip.h>
#include <ws2def.h>
#include <sys/types.h>
#include <WS2tcpip.h>

#elif defined(__linux__)
// Linux-specific includes
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#endif

#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <functional>

#include "../helpers.h"
#include "TCPIPv4ListenerSocket.h"

/**
 * @class ReadSelectWrapper
 * @brief A wrapper class for the select() system call used for monitoring multiple file descriptors for read readiness.
 *
 * The ReadSelectWrapper class provides a convenient interface for using the select() system call to monitor multiple file descriptors
 * for read readiness. It maintains a set of file descriptors and provides methods to add and remove file descriptors, perform the select()
 * operation, and handle incoming requests on the ready file descriptors.
 *
 * The class is designed to be used in a client-server multithreaded application, where the server needs to handle multiple client
 * connections concurrently. It can be used to efficiently monitor and handle incoming requests from multiple clients using a single thread.
 */
class ReadSelectWrapper
{
private:
    fd_set master;   // Master file descriptor list
    fd_set read_fds; // Temp file descriptor list for select()
    int fdmax;       // Maximum file descriptor number

    std::vector<std::shared_ptr<ClientSocket>> clients;

    const TCPIPv4ListenerSocket *listener{};

public:
    ReadSelectWrapper(const TCPIPv4ListenerSocket *_listener) : listener(_listener)
    {
        if (listener == nullptr)
        {
            throw std::runtime_error("Listener is not initialized");
        }
        FD_ZERO(&master);
        FD_ZERO(&read_fds);

        FD_SET(listener->getSocket(), &master);
        fdmax = listener->getSocket();
    }

    /**
     * @brief Performs a select operation on the file descriptors.
     *
     * This function uses the select system call to monitor the file descriptors for any activity.
     * If no file descriptors are ready, the function will block until at least one file descriptor is ready.
     * @throws std::system_error if an error occurs during the select operation.
     */
    void select()
    {
        // Upon return, each of the file descriptor sets is modified in place to indicate which file  descriptors
        // are currently "ready".  Thus, if using select() within a loop, the sets must be reinitialized before each call
        // Thats why we copy
        read_fds = master; // Copy it
        if (::select(fdmax + 1, &read_fds, NULL, NULL, NULL) == -1)
        {
            throwSystemError("Error in select()");
        }
    }

    /**
    Checks if a socket is ready for reading.
    *
    * @param sock The socket to check.
    * @return True if the socket is ready for reading, false otherwise.
    */
    bool canRead(int sock)
    {
        return FD_ISSET(sock, &read_fds);
    }

    /**
     * Handles incoming requests from client sockets.
     * This function iterates through the file descriptors and checks if a client socket is ready to read.
     *
     * @param acceptNewClient A function that is called when a new client socket is accepted. It takes a shared pointer to the accepted client socket as an argument.
     * @param handleClientRequest A function that is called when an existing client socket wants to send data. It takes a shared pointer to the client socket as an argument.
     */
    void handleIncomingRequests(std::function<void(std::shared_ptr<ClientSocket>)> acceptNewClient,
                                std::function<void(std::shared_ptr<ClientSocket>)> handleClientRequest)
    {
        // Iterate untill max file descriptor and checks if a client is ready to read
        for (int i = 0; i <= fdmax; i++)
        {
            if (canRead(i))
            {
                if (i == listener->getSocket())
                { // handle new connections
                    std::shared_ptr<ClientSocket> client = listener->accept();
                    addClientSocket(client);
                    acceptNewClient(client);
                }
                else
                { // Client wants to send data
                    handleClientRequest(getClientSocket(i));
                }
            }
        }
    }

    void addClientSocket(std::shared_ptr<ClientSocket> client)
    {
        clients.push_back(client);
        addSocket(client->sock_fd);
    }

    void removeClientSocket(std::shared_ptr<ClientSocket> client)
    {
        clients.erase(std::remove(clients.begin(), clients.end(), client), clients.end());
        removeSocket(client->sock_fd);
    }

    std::shared_ptr<ClientSocket> getClientSocket(int i)
    {
        return *std::find_if(clients.begin(), clients.end(), [i](std::shared_ptr<ClientSocket> client)
                             { return client->sock_fd == i; });
    }

    ~ReadSelectWrapper()
    {
        fdmax = 0;
        listener = nullptr;
        FD_ZERO(&master);
        FD_ZERO(&read_fds);
    }

private:
    void addSocket(int sock)
    {
        FD_SET(sock, &master); // Add to master set
        if (sock > fdmax)
        { // Keep track of the max
            fdmax = sock;
        }
    }

    void removeSocket(int sock)
    {
        FD_CLR(sock, &master);
        if (sock == fdmax)
        {
            while (FD_ISSET(fdmax, &master) == false)
                fdmax -= 1;
        }
    }
};