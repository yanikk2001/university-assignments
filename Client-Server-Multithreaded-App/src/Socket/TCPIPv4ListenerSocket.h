#pragma once
#include "SocketBase.h"
#include "../helpers.h"
#include "ClientSocket.h"
#include <memory>

/**
 * @class TCPIPv4ListenerSocket
 * @brief Represents a TCP/IP version 4 listener socket.
 *
 * Provides functionality
 * for creating and managing a TCP/IP version 4 listener socket (For a server). It allows binding to a specific
 * port and listening for incoming connections.
 */
class TCPIPv4ListenerSocket : public SocketBase
{
public:
    TCPIPv4ListenerSocket(int port) : SocketBase()
    {
        init(port);
    }

    TCPIPv4ListenerSocket() = delete;
    TCPIPv4ListenerSocket(const TCPIPv4ListenerSocket &) = delete;
    TCPIPv4ListenerSocket(const TCPIPv4ListenerSocket &&) = delete;

    std::shared_ptr<ClientSocket> accept() const
    {
        std::shared_ptr<ClientSocket> clientSocket = std::make_shared<ClientSocket>();
        int socket = ::accept(sock_fd, (struct sockaddr *)&clientSocket->addr, &clientSocket->addrlen);

        clientSocket->sock_fd = socket;
        if (clientSocket->sock_fd == -1)
        {
            throwSystemError("Could not accept connection");
        }
        return clientSocket;
    }

private:
    void init(int port)
    {
        // Create ipv4(AF_INET) tcp(SOCK_STREAM) socket, 0 is the protocol (default for TCP)
        sock_fd = socket(AF_INET, SOCK_STREAM, 0);
        if (sock_fd == -1)
        {
            throwSystemError("Could not create socket");
        }

        sockaddr_in addr; // for more info see man 7 ip
        addr.sin_family = AF_INET;
        // htons ensures that integer values are stored in memory in network byte order, which is big-endian which can be understood by network
        // protocols like TCP/IP
        addr.sin_port = htons(port);

        #if defined(_WIN32)
        addr.sin_addr.S_un.S_addr = INADDR_ANY; // means any address for binding (localhost, or any other IP address of the machine)
        #elif defined(__linux__)
        addr.sin_addr.s_addr = INADDR_ANY;
        #endif
        
        if (bind(sock_fd, (struct sockaddr *)&addr, sizeof(addr)) == -1)
        {
            close();
            throwSystemError("Could not bind socket to port");
        }

        // listen for connections on a socket
        //  SOMAXCONN is the maximum number of pending connections that can be queued up before connections are refused
        if (listen(sock_fd, SOMAXCONN) == -1)
        {
            close();
            throwSystemError("Could not listen on socket");
        }
    }
};
