#pragma once

#include <string>
#include "../helpers.h"

#if defined(_WIN32)
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#pragma comment(lib, "ws2_32.lib")
#include <WinSock2.h>
#include <stdlib.h>
#include <Windows.h>
#include <in6addr.h>
#include <WS2tcpip.h>
#include <ws2def.h>
#include <sys/types.h>
#include <WS2tcpip.h>

#elif defined(__linux__)
// Linux-specific includes
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif

/**
 * @class SocketBase
 * @brief Base class for socket operations.
 *
 * The SocketBase class provides a base implementation for socket operations.
 * It includes platform-specific socket file descriptor and common socket operations. Works on Windows and Linux.
 */
class SocketBase
{
protected:
#if defined(_WIN32)
    SOCKET sock_fd;
#elif defined(__linux__)
    int sock_fd;
#endif

public:
    SocketBase()
    {
        sock_fd = -1;

#if defined(_WIN32)
        // Initialze winsock
        WSADATA wsData;
        WORD ver = MAKEWORD(2, 2);
        int wsOk = WSAStartup(ver, &wsData);
        if (wsOk != 0)
        {
            throwSystemError("Could not initialize Winsock");
        }
#endif
    }
    virtual ~SocketBase() { close(); }

#if defined(_WIN32)
    inline SOCKET getSocket() const
    {
        return sock_fd;
    }
#elif defined(__linux__)
    inline int getSocket() const
    {
        return sock_fd;
    }
#endif

    virtual void close()
    {
#if defined(_WIN32)
        if (sock_fd != INVALID_SOCKET)
        {
            shutdown(sock_fd, 2);                     // 2 = SD_BOTH, Disables further send and receive operations.
            if (closesocket(sock_fd) == SOCKET_ERROR) // frees allocated resources
            {
                throw std::system_error(WSAGetLastError(), std::system_category(), "Could not close socket");
            }

            // Cleanup winsock
            WSACleanup();

            sock_fd = INVALID_SOCKET;
        }
#elif defined(__linux__)
        if (sock_fd != -1)
        {
            shutdown(sock_fd, 2); // 2 = SD_BOTH, Disables further send and receive operations.
            // Using :: to call the global namespace close function
            if (::close(sock_fd) == -1)
            {
                throw std::system_error(errno, std::system_category(), "Could not close socket");
            }
            sock_fd = -1;
        }
#endif
    }

protected:
    // Normal close without added logic
    virtual void closeSocket(int _sock_fd)
    {
#if defined(_WIN32)
        if (closesocket(_sock_fd) == SOCKET_ERROR) // frees allocated resources
        {
            throw std::system_error(WSAGetLastError(), std::system_category(), "Could not close socket");
        }
#elif defined(__linux__)
        if (::close(_sock_fd) == -1)
        {
            throw std::system_error(errno, std::system_category(), "Could not close socket");
        }
#endif
    }
};