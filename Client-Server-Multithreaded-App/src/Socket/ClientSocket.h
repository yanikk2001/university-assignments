#pragma once

#include <string>
#include <exception>
#include <cstring>

#include "TCPIPv4ConnectSocket.h"

/**
 * @class ClientSocket
 * @brief Represents a client socket for communication with a server.
 * 
 * The ClientSocket class provides methods for sending and receiving data
 * to a client a server using TCP/IP protocol.
 */
class ClientSocket
{
public:
    int sock_fd = 1;
    sockaddr_storage addr; // client address
    socklen_t addrlen = sizeof(addr);

    void send(const std::string &msg) const
    {
        TCPIPv4ConnectSocket::send(msg, sock_fd);
    }

    void recv(std::ostream &os) const
    {
        TCPIPv4ConnectSocket::recv(sock_fd, os);
    }

    void sendStreamData(std::istream& dataStream) const {
        TCPIPv4ConnectSocket::sendStreamData(sock_fd, dataStream);
    }

    std::string getIp()
    {
        char remoteIP[INET_ADDRSTRLEN];
        if (inet_ntop(addr.ss_family,
                      get_in_addr((struct sockaddr *)&addr),
                      remoteIP, INET_ADDRSTRLEN) == NULL)
        {
            throwSystemError("inet_ntop failed");
        }
        return std::string(remoteIP);
    }

private:
    void *get_in_addr(struct sockaddr *sa)
    {
        if (sa->sa_family == AF_INET)
        {
            return &(((struct sockaddr_in *)sa)->sin_addr);
        }

        return &(((struct sockaddr_in6 *)sa)->sin6_addr);
    }
};
