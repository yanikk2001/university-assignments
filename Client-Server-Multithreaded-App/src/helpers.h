#pragma once
#include <string>
#include <system_error> // Include this for std::system_error
#include <cstring>      // Include for std::strerror

#define SERVER_MSG "MSG"
#define SERVER_FILE "FILE"

#if defined(_WIN32)
#include <WinSock2.h>
#endif

inline void throwSystemError(const std::string &msg)
{
#if defined(_WIN32)
    int error = WSAGetLastError();
    std::cerr << "Error code: " << error << std::endl;
    throw std::system_error(error, std::system_category(), msg);
#elif defined(__linux__)
    // designed to throw exceptions for system call errors. This exception type can utilize errno
    std::cerr << "Error code: " << errno << std::endl;
    throw std::system_error(errno, std::system_category(), msg);
#endif
}