#pragma once

#include <thread>
#include <algorithm>
#include <future>
#include <execution>

// Min num of elements required to create a new thread
// We multiply by a power of 2 to reduce the load on each thread and Powers of 2 simplifies binary arithmetic operations and caching
const int MIN_EL_PER_THREAD = 1'000'000 / std::thread::hardware_concurrency() * 2 ^ 3;

/**
 * Sorts the elements in the range [first, last) using parallel quicksort algorithm.
 * The algorithm divides the range into smaller partitions and sorts them concurrently using multiple threads.
 * 
 * @param first The iterator to the first element in the range.
 * @param last The iterator to the element past the last element in the range.
 * @param minPerThread The minimum number of elements per thread. Defaults to MIN_EL_PER_THREAD. 
 * If the number of elements in the range is less than or equal to this value, the range is sorted sequentially.
 * @return The number of threads used for sorting.
 */
template <typename Iterator>
inline unsigned constexpr parallelQuicksort(Iterator first, Iterator last, const int minPerThread = MIN_EL_PER_THREAD)
{
    if (first == last)
        return 0;

    if (std::distance(first, last) <= minPerThread)
    {
        std::sort(first, last);
        return 0;
    }

    // Choose pivot
    auto pivot = *std::next(first, std::distance(first, last) / 2);
    // When the pivot value has duplicates in the array, this approach places all duplicates of the pivot together in between middle1 and middle2
    // Improves performance as it reduces redundant comparisons
    auto middle1 = std::partition(std::execution::par, first, last, [pivot](const auto &em)
                                  { return em < pivot; });
    auto middle2 = std::partition(std::execution::par, middle1, last, [pivot](const auto &em)
                                  { return em <= pivot; }); // returns iterator to pivot el

    // Create new threads for the two partitions
    auto future = std::async(std::launch::async, [&]()
                             { return parallelQuicksort<Iterator>(first, middle1); });
    auto future1 = std::async(std::launch::async, [&]()
                              { return parallelQuicksort<Iterator>(middle2, last); });

    // Wait for the threads to finish
    return future.get() + future1.get() + 1;
}
