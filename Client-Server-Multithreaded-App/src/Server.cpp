#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <chrono>
#include <random>
#include <functional>
#include <sstream>
#include "ParallelQuicksort.h"
#include "Socket/TCPIPv4ListenerSocket.h"
#include "Socket/ClientSocket.h"
#include "Socket/ReadSelectWrapper.h"
#include "ExecutionPolicyStorage.h"

#define PORT 4950

// Stores each client's preferred sorting execution policy
ExecutionPolicyStorage policyStorage;

unsigned sort(std::vector<int> &v, ExecutionPolicy policy)
{
    switch (policy)
    {
    case seq:
        std::sort(v.begin(), v.end());
        return 0;
        break;

    case par:
        return parallelQuicksort<std::vector<int>::iterator>(v.begin(), v.end());
        break;
    default:
        std::sort(v.begin(), v.end());
        return 0;
        break;
    }
}

/**
 * Evaluates the execution time of a given function and prints the result.
 * 
 * @param func The function to be evaluated.
 * @param name The name of the function (optional).
 * @param out The output stream to print the result (default is std::cout).
 */
void evalAndPrintExecutionTime(auto func, const std::string &name = "", std::ostream &out = std::cout)
{
    const auto t1 = std::chrono::high_resolution_clock::now();
    auto res = func();
    const auto t2 = std::chrono::high_resolution_clock::now();
    const std::chrono::duration<double, std::milli> ms = t2 - t1;
    out << std::fixed << "\n -------------------------- \n"
        << name << " took " << ms.count() << " ms"
        << '\n'
        << "Additional threads used: " << res
        << "\n -------------------------- \n";
};

void executeSorting(std::vector<int> &v, ExecutionPolicy policy, std::ostream &os)
{
    if (policy == both)
    {
        evalAndPrintExecutionTime([&]()
                                  { return sort(v, seq); }, "Sequential sort", os);
        std::copy(std::begin(v), std::end(v), std::ostream_iterator<int>(os, " "));

        evalAndPrintExecutionTime([&]()
                                  { return sort(v, par); }, "Parallel sort", os);
        std::copy(std::begin(v), std::end(v), std::ostream_iterator<int>(os, " "));
        return;
    }
    std::string policyStr = policy == seq ? "Sequential" : "Parallel";
    std::string outputStr = "Sorted with " + policyStr + " execution policy \n";
    evalAndPrintExecutionTime([&]()
                              { return sort(v, policy); }, outputStr, os);
    std::copy(std::begin(v), std::end(v), std::ostream_iterator<int>(os, " "));
}

/**
 * Handles the client command and performs the corresponding actions.
 *
 * @param clientfd The file descriptor of the client socket.
 * @param command The command received from the client.
 * @param input The input stream to read additional data from the client.
 * @param os The output stream to send the response to the client.
 */
void handleCommand(int clientfd, const std::string &command, std::istream &input, std::ostream &os)
{
    if (command == "help")
    {
        os << SERVER_MSG << " "
           << "Available commands:\n\n"
           << "help - display this message\n\n"
           << "gen {SIZE} - generates a random vector of the specified size and sorts it\n\n"
           << "method {EXECUTION POLICY | default is par} - sets the execution policy for the sorting algorithm, available options: seq, par "
           << "or both (sorts sequentially and in parallel so you can compare execution times)\n\n";
        return;
    }
    else if (command == "gen")
    {
        size_t size;
        input >> size;
        std::vector<int> v(size);

        // Fills the vector with random numbers
        std::generate(std::execution::par, v.begin(), v.end(), [size]()
                      {
                        std::mt19937 generator; // Mersenne Twister: Good quality random number generator
                        generator.seed(std::random_device()()); // Seed with real random data

                        std::uniform_int_distribution<int> distribution(0, size); 

                        return distribution(generator); });

        os << "Before sorting: \n";
        std::copy(std::begin(v), std::end(v), std::ostream_iterator<int>(os, " "));

        ExecutionPolicy policy = policyStorage.getPolicy(clientfd);
        executeSorting(v, policy, os);
    }
    else if (command == "method")
    {
        std::string policy;
        input >> policy;
        if (policy == "seq")
        {
            policyStorage.setPolicy(clientfd, ExecutionPolicy::seq);
            os << SERVER_MSG << " Execution policy set to sequential\n";
        }
        else if (policy == "par")
        {
            policyStorage.setPolicy(clientfd, ExecutionPolicy::par);
            os << SERVER_MSG << " Execution policy set to parallel\n";
        }
        else if (policy == "both")
        {
            policyStorage.setPolicy(clientfd, ExecutionPolicy::both);
            os << SERVER_MSG << " Execution policy set to both\n";
        }
        else
        {
            os << SERVER_MSG << " Invalid policy\n";
        }
    }
    else
    {
        os << SERVER_MSG << " Invalid command\n";
    }
}

int main()
{
    TCPIPv4ListenerSocket listenerSocket(PORT);
    ReadSelectWrapper selectWrapper(&listenerSocket);

    std::cout << "Server started\n";

    while (true)
    {
        selectWrapper.select();

        selectWrapper.handleIncomingRequests(
            [&](std::shared_ptr<ClientSocket> client) // Accept new client connection
            {
                std::cout << "----------------------\n";
                std::cout << "New client connected\n";
                std::cout << "Client address: " << client->getIp() << '\n';
                std::cout << "----------------------\n";

                policyStorage.setPolicy(client->sock_fd, ExecutionPolicy::par);
                std::string msg = SERVER_MSG;
                msg += std::string(" Welcome to the sorting server! Type 'help' to see available commands\n");
                client->send(msg);
            },
            [&](std::shared_ptr<ClientSocket> client) // Handle client request
            {
                const std::string clientIp = client->getIp();
                std::stringstream clientInput;
                try
                {
                    client->recv(clientInput);
                }
                catch (const std::exception &e)
                {
                    std::cerr << clientIp << " " << e.what() << '\n';
                    policyStorage.removePolicy(client->sock_fd);
                    selectWrapper.removeClientSocket(client);
                    return;
                }

                std::string command;
                clientInput >> command;
                std::stringstream out;
                std::cout << clientIp << " : " << command << '\n';
                handleCommand(client->sock_fd, command, clientInput, out);
                client->sendStreamData(out);
            });
    }

    return 0;
}
