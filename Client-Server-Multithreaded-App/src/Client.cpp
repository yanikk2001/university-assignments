#include <iostream>
#include <sstream>
#include <fstream>
#include "Socket/TCPIPv4ConnectSocket.h"
#define SERVERPORT "4950" // the port users will be connecting to
#define MAXBUFLEN 10000

size_t reportFileSize(const std::string &filename)
{
    std::ifstream file;
    file.open(filename, std::ios::in | std::ios::binary);

    file.seekg(0, std::ios::end);
    return file.tellg();
}

int main(int argc, char *argv[])
{
    std::string filename("ezoutput.txt");
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s hostname\n", argv[0]);
        exit(1);
    }

    if (argv[2])
    {
        filename = argv[2];
    }

    TCPIPv4ConnectSocket connectSocket(argv[1], SERVERPORT);

    printf("Enter commands to send to the server, type 'end' to quit:\n");
    char buf[MAXBUFLEN];
    std::stringstream output;
    std::stringstream input;

    std::ofstream outfile;

    while (true)
    {
        try
        {
            input.clear(); // Reset state flags, important for reusing the stream
            input.str(""); // Clear contents
            outfile.open(filename, std::ios::out | std::ios::trunc);

            std::cout << "Receiving results...\n";
            connectSocket.recv(input); //Receive data from the server -------------
            std::string typeOfData;
            input >> typeOfData;
            if (typeOfData == SERVER_MSG)
            {
                std::cout << input.str() << '\n';
            }
            else
            {
                if (!outfile.is_open() || !outfile.good())
                {
                    std::cerr << "Failed to open file " << filename << '\n';
                    continue;
                }
                std::cout << "Writing results to " << filename << "...\n";
                outfile << input.str();

                std::cout << "Received " << reportFileSize(filename) << " bytes\n";
            }

            std::cout << '\n';
            outfile.close();
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
            continue;
        }

        std::cin.getline(buf, MAXBUFLEN);
        if (strcmp(buf, "end") == 0)
            break;

        try
        {
            output.clear(); // Reset state flags, important for reusing the stream
            output.str(""); // Clear contents
            output << buf;  // Insert new data
            connectSocket.sendStreamData(output); //Send data to the server -------------
        }
        catch (const std::exception &e)
        {
            std::cerr << e.what() << '\n';
            continue;
        }
    }

    return 0;
}