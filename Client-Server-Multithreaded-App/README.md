<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">Client-Server Multithreaded Sorting App</h3>

  <p align="center">
    A Client and a Server app build from the ground up using C++ and native network apis.
    Works both on <strong>Windows</strong> and <strong>Linux</strong>
    <br />
    <br />
    <a href="#examples">View Demo</a>
   
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#building-and-running">Building and running</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#examples">Examples</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
The project implements a client-server architecture where client instances can connect via a custom-made socket library to a server.
Once connected each client can request an array of random size to be generated and sorted via different methods. The result is sent back and saved as a file on the client's machine.

* `Server` - Accepts client's connections. It uses the **select/selector** paradigm to concurrently handle multiple clients on the same thread
* `Client` - Application that allows connection and interaction with a server instance 
* `Socket library` - Custom-made library of classes that utilize native/kernel socket libraries such as **winsock2** and **sys/socket**. These classes wrap these complex APIs and provide Interchangeability between operating systems. This means the project compiles and works both on **Linux** and **Windows**
* `Parallel Quicksort` - Function that can sort a given array of numbers on multiple threads boosting its productivity
* `ExecutionPolicyStorage` - Fully thread-safe storage for client's preferences. It uses mutexes to avoid race conditions

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started

How to install and run it?

### Prerequisites

- CMake installed

### Building and Running 
Create a build folder and initialize CMake
```sh
mkdir build
cd ./build
cmake ../
```
To build using the default settings use:
```sh
cmake --build .
```

### Running
Once compiled the executable files will be located in the `build` folder

```
cd ./build

// Starts the server, it automatically starts listening for requests
// on port 4950
./Server 

// Starts the client, you should provide an IP/URL of the server
// and optionally a file path where you want files to be saved
// by default it is exoutput.txt
./Client 127.0.0.1
```

<!-- USAGE EXAMPLES -->
## Usage

Once both server and clients are running you can send different requests from a client to the server.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

The following examples show different scenarios from the points of view of the client and server

## Examples

* When a client connects the server accepts the connection and prints some info

SERVER
```
----------------------
New client connected
Client address: 127.0.0.1
----------------------
```

* Help command

CLIENT
```
Enter commands to send to the server, type 'end' to quit:
Receiving results...
MSG Welcome to the sorting server! Type 'help' to see available commands


help
Receiving results...
MSG Available commands:

help - display this message

gen {SIZE} - generates a random vector of the specified size and sorts it

method {EXECUTION POLICY | default is par} - sets the execution policy for the sorting algorithm, available options: seq, par, or both (sorts sequentially and in parallel so you can compare execution times)
```

* gen command

CLIENT
```
gen 1000000
Receiving results...
Writing results to ezoutput.txt...
Received 13780570 bytes
```

* method command

CLIENT
```
method par
Receiving results...
MSG Execution policy set to parallel
```

* The client can execute different commands on the server. The server logs each client what command requested

```
----------------------
New client connected
Client address: 127.0.0.1
----------------------
127.0.0.1 : help
127.0.0.1 : gen
127.0.0.1 : method
```

<!-- LICENSE -->
## License

Distributed under the MIT License. 

<p align="right">(<a href="#readme-top">back to top</a>)</p>