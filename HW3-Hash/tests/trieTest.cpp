#define CATCH_CONFIG_MAIN
#include "../../catch.h"

#include "../DataStructures/trie.h"

TEST_CASE("Copy operations")
{

    Trie tr;
    tr.add("hello");
    tr.add("world");
    tr.add("wide");
    tr.add("web");

    REQUIRE(tr.countOf("hello") == 1);
    REQUIRE(tr.countOf("world") == 1);
    REQUIRE(tr.countOf("wide") == 1);
    REQUIRE(tr.countOf("web") == 1);

    REQUIRE(tr.countOfUniqueWords() == 4);
    REQUIRE(tr.countOfAllWords() == 4);

    std::multiset<std::string> expected{"hello", "world", "wide", "web"};
    REQUIRE(tr.content() == expected);

    SECTION("Copy constructor")
    {

        Trie tr1(tr);
        REQUIRE(tr1.countOf("hello") == 1);
        REQUIRE(tr1.countOf("world") == 1);
        REQUIRE(tr1.countOf("wide") == 1);
        REQUIRE(tr1.countOf("web") == 1);

        REQUIRE(tr1.countOfUniqueWords() == 4);
        REQUIRE(tr1.countOfAllWords() == 4);

        std::multiset<std::string> expected1{"hello", "world", "wide", "web"};
        REQUIRE(tr1.content() == expected1);
    }

    SECTION("Copy operator")
    {

        Trie tr1;

        tr1 = tr;

        REQUIRE(tr1.countOf("hello") == 1);
        REQUIRE(tr1.countOf("world") == 1);
        REQUIRE(tr1.countOf("wide") == 1);
        REQUIRE(tr1.countOf("web") == 1);

        REQUIRE(tr1.countOfUniqueWords() == 4);
        REQUIRE(tr1.countOfAllWords() == 4);

        std::multiset<std::string> expected1{"hello", "world", "wide", "web"};
        REQUIRE(tr1.content() == expected1);
    }
}

TEST_CASE("Empty trie")
{

    Trie tr;

    REQUIRE(tr.countOfUniqueWords() == 0);
    REQUIRE(tr.countOfAllWords() == 0);
    REQUIRE(tr.countOf("abfebubf") == 0);
    REQUIRE(tr.contains("hfepkfep") == false);
    REQUIRE(tr.content().size() == 0);
    REQUIRE(tr.contains("") == false);

    SECTION("Add empty string")
    {
        tr.add("");
        REQUIRE(tr.countOfUniqueWords() == 0);
        REQUIRE(tr.countOfAllWords() == 0);
        REQUIRE(tr.contains("") == false);
        REQUIRE(tr.content().size() == 0);
    }
}

TEST_CASE("Adding elements")
{

    Trie tr;

    SECTION("One element")
    {
        tr.add("hello");

        REQUIRE(tr.contains("hello") == true);
        REQUIRE(tr.countOf("hello") == 1);
        REQUIRE(tr.countOfUniqueWords() == 1);
        REQUIRE(tr.countOfAllWords() == 1);

        std::multiset<std::string> expected{"hello"};
        REQUIRE(tr.content() == expected);
    }

    SECTION("SHANO elements")
    {
        tr.add("567");
        tr.add("5432");

        std::multiset<std::string> expected{"567", "5432"};
        REQUIRE(tr.content() == expected);
    }

    SECTION("Multiple elements")
    {
        tr.add("kekw", 4);
        tr.add("567hvs?$sh+.`");
        tr.add("5493", 3);
        tr.add("//////");
        tr.add("double");
        tr.add("one");
        tr.add("double");

        REQUIRE(tr.contains("kekw") == true);

        REQUIRE(tr.countOf("kekw") == 4);
        REQUIRE(tr.countOf("567hvs?$sh+.`") == 1);
        REQUIRE(tr.countOf("5493") == 3);
        REQUIRE(tr.countOf("//////") == 1);
        REQUIRE(tr.countOf("double") == 2);
        REQUIRE(tr.countOf("one") == 1);

        REQUIRE(tr.countOfUniqueWords() == 6);
        REQUIRE(tr.countOfAllWords() == 12);

        std::multiset<std::string> expected{"kekw", "kekw", "kekw", "kekw", "567hvs?$sh+.`", "5493", "5493", "5493", "//////",
                                            "double", "double", "one"};

        REQUIRE(tr.content() == expected);
    }

    SECTION("Shano elements2")
    {
        tr.add("car", 2);
        tr.add("heck");
        tr.add("cars");
        tr.add("carsell", 2);

        REQUIRE(tr.countOf("car") == 2);
        REQUIRE(tr.countOf("heck") == 1);
        REQUIRE(tr.countOf("cars") == 1);
        REQUIRE(tr.countOf("carsell") == 2);

        REQUIRE(tr.countOfUniqueWords() == 4);
        REQUIRE(tr.countOfAllWords() == 6);

        std::multiset<std::string> expected{"car", "car", "heck", "cars", "carsell", "carsell"};
        REQUIRE(tr.content() == expected);
        REQUIRE(tr.content() == expected);
    }

    SECTION("Shano elements3")
    {
        tr.add("123");
        tr.add("134");
        tr.add("135");

        std::multiset<std::string> expected{"123", "134", "135"};
        REQUIRE(tr.content() == expected);
    }
}

TEST_CASE("Removing elements")
{
    Trie tr;
    tr.add("hello");
    tr.add("world");
    tr.add("wide");
    tr.add("web");

    REQUIRE(tr.countOf("hello") == 1);
    REQUIRE(tr.countOf("world") == 1);
    REQUIRE(tr.countOf("wide") == 1);
    REQUIRE(tr.countOf("web") == 1);

    REQUIRE(tr.countOfUniqueWords() == 4);
    REQUIRE(tr.countOfAllWords() == 4);

    std::multiset<std::string> expected{"hello", "world", "wide", "web"};
    REQUIRE(tr.content() == expected);

    SECTION("Remove empty element")
    {
        tr.remove("");

        REQUIRE(tr.countOf("hello") == 1);
        REQUIRE(tr.countOf("world") == 1);
        REQUIRE(tr.countOf("wide") == 1);
        REQUIRE(tr.countOf("web") == 1);

        REQUIRE(tr.countOfUniqueWords() == 4);
        REQUIRE(tr.countOfAllWords() == 4);

        std::multiset<std::string> expected1{"hello", "world", "wide", "web"};
        REQUIRE(tr.content() == expected1);
    }

    SECTION("Removing non-existent element")
    {
        tr.remove("kekw");

        REQUIRE(tr.countOf("hello") == 1);
        REQUIRE(tr.countOf("world") == 1);
        REQUIRE(tr.countOf("wide") == 1);
        REQUIRE(tr.countOf("web") == 1);

        REQUIRE(tr.countOfUniqueWords() == 4);
        REQUIRE(tr.countOfAllWords() == 4);

        std::multiset<std::string> expected1{"hello", "world", "wide", "web"};
        REQUIRE(tr.content() == expected1);
    }

    SECTION("No dublicate elements")
    {
        tr.remove("hello");
        REQUIRE(tr.contains("hello") == false);

        std::multiset<std::string> expected1{"world", "wide", "web"};
        REQUIRE(tr.content() == expected1);

        tr.remove("wide");
        REQUIRE(tr.contains("wide") == false);

        std::multiset<std::string> expected2{"world", "web"};
        REQUIRE(tr.content() == expected2);

        REQUIRE(tr.countOfUniqueWords() == 2);
        REQUIRE(tr.countOfAllWords() == 2);
    }

    SECTION("Duplicate elements")
    {
        tr.add("hello");
        REQUIRE(tr.countOf("hello") == 2);
        tr.add("wide", 2);
        REQUIRE(tr.countOf("wide") == 3);

        std::multiset<std::string> expected0{"hello", "hello", "web", "wide", "wide", "wide", "world"};
        REQUIRE(tr.content() == expected0);

        tr.remove("hello");
        REQUIRE(tr.contains("hello") == true);

        std::multiset<std::string> expected1{"hello", "web", "wide", "wide", "wide", "world"};
        REQUIRE(tr.content() == expected1);

        tr.remove("wide");
        REQUIRE(tr.contains("wide") == true);

        std::multiset<std::string> expected2{"hello", "wide", "wide", "world", "web"};
        REQUIRE(tr.content() == expected2);

        REQUIRE(tr.countOfUniqueWords() == 4);
        REQUIRE(tr.countOfAllWords() == 5);

        tr.remove("hello");
        REQUIRE(tr.contains("hello") == false);

        REQUIRE(tr.countOfUniqueWords() == 3);
        REQUIRE(tr.countOfAllWords() == 4);

        REQUIRE(tr.countOf("hello") == 0);
        REQUIRE(tr.countOf("world") == 1);
        REQUIRE(tr.countOf("wide") == 2);
        REQUIRE(tr.countOf("web") == 1);
    }

    SECTION("Times & obliterate options")
    {
        tr.add("hello", 2);
        tr.add("web", 10);

        REQUIRE(tr.countOf("hello") == 3);
        REQUIRE(tr.countOf("web") == 11);

        tr.remove("hello", 2);

        REQUIRE(tr.countOf("hello") == 1);

        tr.remove("web", 1, true);

        REQUIRE(tr.contains("web") == false);

        std::multiset<std::string> expected2{"hello", "world", "wide"};
        REQUIRE(tr.content() == expected2);

        REQUIRE(tr.countOfUniqueWords() == 3);
    }

    SECTION("Clear method")
    {
        tr.clear();
        REQUIRE(tr.countOfUniqueWords() == 0);
        REQUIRE(tr.countOfAllWords() == 0);

        REQUIRE(tr.content().size() == 0);

        REQUIRE(tr.countOf("hello") == 0);
        REQUIRE(tr.countOf("world") == 0);
        REQUIRE(tr.countOf("wide") == 0);
        REQUIRE(tr.countOf("web") == 0);
    }
}
