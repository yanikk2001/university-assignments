#include <iostream>
#include <fstream>
#include "./interface.h"

void printResult(const char* name, const WordsMultiset& file, const WordsMultiset& common, const char* otherName)
{
    std::size_t countAllWords = file.countOfAllWords() + common.countOfAllWords();
    unsigned percent = float(common.countOfAllWords()) / float(countAllWords) *100;
    std::cout << name << " contains " << countAllWords << " words " 
    << "of which " << common.countOfAllWords() << " (" << common.countOfUniqueWords() << " unique)"
    << " are also in " << otherName << " (" << percent << "%)" <<'\n';
}

int main(int argc, char *argv[])
{
    if(argc > 3 || argc < 3) 
    {
        std::cerr << "Invalid number of arguments, enter only file paths" << '\n';
        return 1;
    }

    std::ifstream file1(argv[1]);
    std::ifstream file2(argv[2]);
    if(!file1.good() || !file1.is_open() || !file2.good() || !file2.is_open())
    {
        std::cout << "Failed to open the file" << "\n";
        return 2;
    }
    Comparator cmp;
    ComparisonReport report = cmp.compare(file1, file2);

    printResult(argv[1], report.uniqueWords[0], report.commonWords, argv[2]);
    printResult(argv[2], report.uniqueWords[1], report.commonWords, argv[1]);
}