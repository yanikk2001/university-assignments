#include <iostream>
#include <set>
#include <string>
#include "../DataStructures/trie.h"

///
/// Represents a multiset of words
///
/// If you need to see how it is intended to be used,
/// check out the corresponding unit tests
///
/// Implement all methods of this class
/// 
class WordsMultiset {
public:
	/// Adds times occurences of word to the container
	///
	/// For example, add("abc") adds the word "abc" once,
	/// while add("abc", 4) adds 4 occurrances.
	void add(const std::string& word, size_t times = 1) {
		try
		{
			data.add(word, times);
		}
		catch(const std::bad_alloc& e)
		{
			throw std::runtime_error("Failed to add word due to bad allocation, please show this to your nearest programmer for more support :p");
		}
	}

	/// Checks whether word is contained in the container
	bool contains(const std::string& word) const {return data.contains(word);}

	/// Number of occurrances of word 
	size_t countOf(const std::string& word) const {return data.countOf(word);}

	/// Number of unique words in the container
	size_t countOfUniqueWords() const {return data.countOfUniqueWords();}

	/// Returns a multiset of all words in the container
	std::multiset<std::string> words() const {return data.content();}

	void remove(const std::string &word, size_t times = 1, bool obliterate = false) {data.remove(word, times, obliterate);}

	void clear() {data.clear();}

	size_t countOfAllWords() const {return data.countOfAllWords();}
	
	// You can add additional members if you need to
private:
	Trie data;
};

///
/// Results of the comparison of two streams of words
/// DO NOT modify this class
/// If you need to see how it is intended to be used,
/// check out the corresponding unit tests
///
class ComparisonReport {
public:
	/// A multiset of all words that exist in both streams
	WordsMultiset commonWords;

	/// Multisets of words unique to the two streams
	/// The first element of the array contains the words that are unique
	/// to the first stream (a) and the second one -- to the second stream (b)
	WordsMultiset uniqueWords[2];
};

/// 
/// Can be used to compare two streams of words
///
class Comparator {
public:
	ComparisonReport compare(std::istream& a, std::istream& b)
	{
		ComparisonReport report;
		if(loadA(a, report))
			loadB(b, report);

		return report;
	}

private:
	bool loadA(std::istream& in, ComparisonReport& report)
	{
		while (in.good())
		{
			std::string word;
			in >> word;
			try
			{
				report.uniqueWords[0].add(word);
			}
			catch(const std::runtime_error& e)
			{
				std::cerr << e.what() << '\n';
				report.uniqueWords[0].clear();
				return false;
			}
		}
		return true;
	}
	bool loadB(std::istream& in, ComparisonReport& report)
	{
		WordsMultiset& wordsA = report.uniqueWords[0];
		WordsMultiset& wordsB = report.uniqueWords[1];
		WordsMultiset& common = report.commonWords;

		while (in.good())
		{
			std::string word;
			in >> word;

			try
			{
				if(wordsA.contains(word))
				{
					wordsA.remove(word);
					common.add(word);
				}
				else
				{
					wordsB.add(word);
				}
			}
			catch(const std::runtime_error& e)
			{
				std::cerr << e.what() << '\n';
				wordsA.clear();
				wordsB.clear();
				common.clear();
				return false;
			}
		}
		return true;
	}
	// You can add additional members if you need to
};