#include "./trie.h"

void Trie::add(const std::string &data, size_t times)
{
    std::size_t size = data.length();
    if(size <= 0) return;
    
    if (!root)
    {
        try
        {
            root = new node;
        }
        catch (const std::bad_alloc &e)
        {
            throw e;
        }
    }

    node *currNode = root;
    for (std::size_t i = 0; i < size; i++)
    {
        unsigned short hashed = data[i];
        try
        {
            currNode->pointers.at(hashed);
        }
        catch (const std::out_of_range &e)
        {
            currNode->pointers.resize(hashed + 1);
        }

        if (!currNode->pointers[hashed])
        {
            try
            {
                currNode->pointers[hashed] = new node;
            }
            catch (const std::bad_alloc &e)
            {
                clear();
                throw e;
            }
        }

        currNode = currNode->pointers[hashed];
    }

    if (currNode->value == 0)
        countUniqueWords++;
    currNode->value += times;
    countAllWords += times;
}

void Trie::remove(const std::string &data, std::size_t times, bool obliterate)
{
    if(data.size() <= 0) return;
    remove(root, data, times, obliterate);
}

bool Trie::isEmpty(node *root)
{
    if (!root)
        return true;

    std::size_t size = root->pointers.size();
    for (std::size_t i = 0; i < size; i++)
        if (root->pointers[i])
            return false;
    return true;
}

Trie::node *Trie::remove(node *root, const std::string &key, std::size_t &times, bool &obliterate, std::size_t depth)
{
    // If tree is empty
    if (!root)
        return nullptr;

    // We are at the last character
    if (depth == key.size())
    {
        // Remove x instance
        if(times > root->value) 
            times = root->value;
        root->value -= times;
        countAllWords -= times;
        if (root->value <= 0 || obliterate) // If all inst removed or obliterate flag is selected - delete
        {
            if (isEmpty(root)) // If there are no other elements below this one
            {
                delete (root);
                root = nullptr;
            }
            countUniqueWords--;
        }
        return root;
    }

    // If not last character, continue traversing
    int index = key[depth];
    root->pointers[index] = remove(root->pointers[index], key, times, obliterate, depth + 1);

    // If root does not have any child (its only child got
    // deleted), and it is not end of another element.
    if (isEmpty(root) && root->value == 0)
    {
        delete (root);
        root = nullptr;
    }

    return root;
}

bool Trie::contains(const std::string &data) const
{
    return countOf(data);
}

std::size_t Trie::countOf(const std::string &data) const
{
    if (!root)
        return 0;

    node *currNode = root;
    std::size_t size = data.length();
    for (std::size_t i = 0; i < size; i++)
    {
        unsigned short hashed = data[i];
        node *n{};
        try
        {
            n = currNode->pointers.at(hashed);
        }
        catch (const std::out_of_range &e)
        {
            return 0;
        }

        if (!n)
            return 0;

        currNode = n;
    }

    return currNode->value;
}

std::size_t Trie::countOfUniqueWords() const
{
    return countUniqueWords;
}

std::size_t Trie::countOfAllWords() const
{
    return countAllWords;
}

std::multiset<std::string> Trie::content() const
{
    std::multiset<std::string> multiset;
    std::string str;
    std::size_t level = 0;
    content(root, str, level, multiset);
    return multiset;
}

void Trie::content(node *root, std::string str, std::size_t level, std::multiset<std::string> &multiset) const
{
    if (!root)
        return;

    // If value is not 0 it means end of word
    if (root->value != 0)
    {
        for (unsigned i = 0; i < root->value; i++)
        {
            multiset.insert(str);
        }
    }

    std::size_t size = root->pointers.size();
    for (std::size_t i = 0; i < size; i++)
    {
        // if not nullptr child is found
        // add parent key to str and
        // call the content function recursively
        // for child node
        node *n = root->pointers[i];

        if (n != nullptr)
        {
            str.erase(level);              // Erases all chars afer level
            str.insert(level, 1, char(i)); // insert doesn`t replace old chars so we erase first

            content(n, str, level + 1, multiset);
        }
    }
}

Trie::Trie(const Trie &other)
{
    copy(&root, other.root);
    countUniqueWords = other.countUniqueWords;
    countAllWords = other.countAllWords;
}

Trie &Trie::operator=(const Trie &other)
{
    if (this != &other)
    {
        clear();
        copy(&root, other.root);
        countUniqueWords = other.countUniqueWords;
        countAllWords = other.countAllWords;
    }
    return *this;
}

void Trie::copy(node **i_root, const node *other)
{
    if (!other)
        return;

    try
    {
        *i_root = new node;
        (*i_root)->value = other->value;
        std::size_t size = other->pointers.size();
        for (std::size_t i = 0; i < size; i++)
        {
            if (other->pointers.at(i) != nullptr)
            {
                try
                {
                    (*i_root)->pointers.at(i);
                }
                catch (const std::out_of_range &e)
                {
                    (*i_root)->pointers.resize(i + 1);
                }

                copy(&(*i_root)->pointers[i], other->pointers[i]);
            }
        }
    }
    catch (const std::bad_alloc &e)
    {
        clear();
        throw e;
    }
}

Trie::~Trie()
{
    free_all(&root);
}

void Trie::free_all(node **i_root)
{
    if (*i_root == nullptr)
        return;

    std::size_t size = (*i_root)->pointers.size();
    for (std::size_t i = 0; i < size; i++)
        free_all(&(*i_root)->pointers[i]);

    delete *i_root;
    *i_root = nullptr;
}
