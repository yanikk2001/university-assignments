#pragma once

#include <vector>
#include <string>
#include <functional>
#include <stdexcept>
#include <set> //required by interface.h

/* 
    Complexity of adding ,retrieving and searching for elements is N where N is the lenght of the element
*/

class Trie
{
    std::size_t countUniqueWords = 0;
    std::size_t countAllWords = 0;

    struct node
    {
        std::size_t value = 0;
        std::vector<node *> pointers{128, nullptr}; //Could be set on preferences if lower number is chosen more free memory but slower
    } * root{};

public:
    Trie() = default;
    Trie(const Trie &other);
    Trie &operator=(const Trie &other);
    ~Trie();

    void add(const std::string &data, size_t times = 1);
    void remove(const std::string &data, std::size_t times = 1, bool obliterate = false);
    bool contains(const std::string &data) const;
    std::size_t countOf(const std::string &data) const;
    std::size_t countOfUniqueWords() const;
    std::size_t countOfAllWords() const;
    std::multiset<std::string> content() const;
    void clear()
    {
        free_all(&root);
        countUniqueWords = 0;
        countAllWords = 0;
    }

private:
    void content(node *root, std::string str, std::size_t level, std::multiset<std::string> &multiset) const;
    void free_all(node **i_root);
    bool isEmpty(node *root);
    node *remove(node *root, const std::string &key, std::size_t &times, bool &obliterate, std::size_t depth = 0);
    void copy(node **root, const node *other);
};