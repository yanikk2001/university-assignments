#define CATCH_CONFIG_MAIN

#include "../../catch.h"
#include "../DataStructures/tree.h"
#include <string>
#include <vector>

using namespace std;

bool lexicographicalCompare(std::string a, std::string b)
{
    return a < b;
}

TEST_CASE("Copy operations")
{
    Tree<string> tr("root");
    REQUIRE(tr.add("blabla", "kekw") == false);
    REQUIRE(tr.add("root", "hello") == true);
    tr.add("hello", "general");
    tr.add("general", "kenobi");
    REQUIRE(tr.getSize() == 4);
    REQUIRE(tr.getHeight() == 4);

    SECTION("Copy constructor")
    {
        Tree<string> cpy(tr);

        auto vec = cpy.bfsPrint();
        REQUIRE(vec[0] == "root");
        REQUIRE(vec[1] == "hello");
        REQUIRE(vec[2] == "general");
        REQUIRE(vec[3] == "kenobi");

        REQUIRE(cpy.getSize() == 4);
        REQUIRE(cpy.getHeight()== 4);
    }

    SECTION("Copy operator")
    {
        Tree<string> cpy("lolo");
        cpy.add("lolo", "yolo");
        cpy.add("lolo", "lel");

        cpy = tr;
        auto vec = cpy.bfsPrint();
        REQUIRE(vec[0] == "root");
        REQUIRE(vec[1] == "hello");
        REQUIRE(vec[2] == "general");
        REQUIRE(vec[3] == "kenobi");

        REQUIRE(cpy.getSize() == 4);
        REQUIRE(cpy.getHeight()== 4);
    }
}

TEST_CASE("Basic operations")
{
    SECTION("Create an empty tree")
    {
        Tree<string> tr;
        // std::cout << '1' << std::endl;
        REQUIRE(tr.getSize() == 0);
        // std::cout << '2' << std::endl;
        
        REQUIRE(tr.getHeight() == 0);
        // std::cout << '3' << std::endl;

        std::vector<string> vec = tr.bfsPrint();
        // std::cout << '4' << std::endl;

        REQUIRE(vec.size() == 0);
    }

    SECTION("Add elements")
    {
        // std::cout << '5' << std::endl;

        Tree<string> tr("root");
        // std::cout << '6' << std::endl;

        tr.add("root","hello");
        // std::cout << '7' << std::endl;

        tr.add("root", "world");
        // std::cout << "7.1" << std::endl;

        tr.add("hello", "there jeneral kenobi");
        // std::cout << '8' << std::endl;


        std::vector<string> vec = tr.bfsPrint();
        // std::cout << '9' << std::endl;

        REQUIRE(vec[0] == "root");
        // std::cout << "10" << std::endl;

        REQUIRE(vec[1] == "hello");
        REQUIRE(vec[2] == "world");
        REQUIRE(vec[3] == "there jeneral kenobi");
        
        REQUIRE(tr.getSize() == 4);
        REQUIRE(tr.getHeight() == 3);
    }

    SECTION("Remove element")
    {
        Tree<string> tr;
        REQUIRE(tr.remove("lala") == false);
        
        REQUIRE(tr.add("root") == true);
        REQUIRE(tr.getSize() == 1);
        REQUIRE(tr.remove("root") == true);
        REQUIRE(tr.getSize() == 0);

        tr.add("newRoot");
        tr.add("newRoot", "hello");

        REQUIRE(tr.getSize() == 2);
        tr.remove("hello");
        REQUIRE(tr.getSize() ==1);
    }

    SECTION("Contains method")
    {
        Tree<string> tr("root");
        tr.add("root", "hello");
        tr.add("hello", "there");
        tr.add("root", "kenobi");

        REQUIRE(tr.contains("root") == true);
        REQUIRE(tr.contains("kenobi") == true);
        REQUIRE(tr.contains("hello") == true);
        REQUIRE(tr.contains("lalal") == false);
    }
}

TEST_CASE("Add using custom order")
{
    Tree<string> tr("root");
    tr.add("root", "Misho", &lexicographicalCompare);
    tr.add("root", "Slavi", &lexicographicalCompare);
    tr.add("root", "Gosho", &lexicographicalCompare);
    
    tr.add("Gosho", "Puncho", &lexicographicalCompare);
    tr.add("Gosho", "Pencho", &lexicographicalCompare);

    auto vec = tr.bfsPrint();

    REQUIRE(vec[0] == "root");
    REQUIRE(vec[1] == "Gosho");
    REQUIRE(vec[2] == "Misho");
    REQUIRE(vec[3] == "Slavi");
    REQUIRE(vec[4] == "Pencho");
    REQUIRE(vec[5] == "Puncho");

    REQUIRE(tr.getSize() == 6);
    REQUIRE(tr.getHeight() == 3);

    SECTION("Add to middle")
    {
        tr.add("root", "Helen", &lexicographicalCompare);
        
        auto vec1 = tr.bfsPrint();

        REQUIRE(vec1[0] == "root");
        REQUIRE(vec1[1] == "Gosho");
        REQUIRE(vec1[2] == "Helen");
        REQUIRE(vec1[3] == "Misho");
        REQUIRE(vec1[4] == "Slavi");
        REQUIRE(vec1[5] == "Pencho");
        REQUIRE(vec1[6] == "Puncho");

        REQUIRE(tr.getSize() == 7);
        REQUIRE(tr.getHeight() == 3);
    }

    SECTION("Add to end")
    {
        tr.add("Gosho", "Zeta", &lexicographicalCompare);
        
        auto vec1 = tr.bfsPrint();

        REQUIRE(vec1[0] == "root");
        REQUIRE(vec1[1] == "Gosho");
        REQUIRE(vec1[2] == "Misho");
        REQUIRE(vec1[3] == "Slavi");
        REQUIRE(vec1[4] == "Pencho");
        REQUIRE(vec1[5] == "Puncho");
        REQUIRE(vec1[6] == "Zeta");

        REQUIRE(tr.getSize() == 7);
        REQUIRE(tr.getHeight() == 3);
    }

    SECTION("Find parent")
    {
        REQUIRE(tr.get("Gosho")->parent->data == "root");
        REQUIRE(tr.get("Misho")->parent->data == "root");
        REQUIRE(tr.get("Puncho")->parent->data == "Gosho");
        REQUIRE(tr.get("Pencho")->parent->data == "Gosho");
    }
}