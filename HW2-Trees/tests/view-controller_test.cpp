#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "../../catch.h"
#include <cstring>

#include "..\source\view.h"
#include "..\source\controller.h"

TEST_CASE("Input process for Viewer")
{
    View& v = View::getInstance();

    SECTION("Convert input to uppercase")
    {
        v.getInput(std::string("hello"));
        REQUIRE(strcmp(v.getMessageSbj().c_str(), "HELLO") == 0);
    }

    SECTION("Messages are transmited correctly between classes")
    {
        Controller cont(v);
        cont.Attach(&v);

        v.getInput(std::string("hello"));
        REQUIRE(strcmp(cont.getMessageObs().c_str(), "HELLO") == 0);
        REQUIRE(strcmp(v.getMessageObs().c_str(), "invalid command!") == 0);
    }
}