#include "ExecutionModule.h"

void ExecutionModule::loadFile(const char* treeName, const char* fileName = nullptr) 
{
    try
    {
        findIndexTree(treeName);
    }
    catch(const std::out_of_range& e) //If it throws that means there is no such element so we create one
    {
        std::fstream file;
        Hierarchy* tree;
        try
        {
            tree = new Hierarchy(); //create new tree
            tree->setName(treeName);
            if(fileName) //If the user provided filename load file
            {
                openFile(file, fileName, "in");
                tree->load(file);
            }
            else //load from console
                tree->load(std::cin);
        }
        catch(const std::bad_alloc& e)
        {
            file.close();
            delete tree;
            throw e;
        }
        catch(const std::invalid_argument& e)
        {
            file.close();
            delete tree;
            throw e;
        }
        stored_trees.push_back(tree);
        file.close();
        return;
    }
    throw std::invalid_argument(std::string(treeName) + std::string(" is already loaded"));
}

void ExecutionModule::saveFile(const char* treeName, const char* fileName) 
{
    std::size_t index;
    try
    {
        index = findIndexTree(treeName);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }

    std::fstream file;
    try
    {
        openFile(file, fileName, "out");
    }
    catch(const std::invalid_argument& e)
    {
        file.close();
        throw e;
    }
    stored_trees[index]->save(file);
    file.close();
}

bool ExecutionModule::findEmployee(const std::string& treeName, const std::string& employeeName) const
{
    std::size_t index;
    try
    {
        index = findIndexTree(treeName);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }

    return stored_trees[index]->find(employeeName);
}

int ExecutionModule::numSubordinates(const std::string& treeName, const std::string& employeeName) const 
{
    std::size_t index;
    try
    {
        index = findIndexTree(treeName);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }

    return stored_trees[index]->num_subordinates(employeeName);
}

std::string ExecutionModule::getManager(const std::string& treeName, const std::string& employeeName) const
{
    std::size_t index;
    try
    {
        index = findIndexTree(treeName);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }

    try
    {
        return stored_trees[index]->manager(employeeName);
    }
    catch(const std::invalid_argument& e)
    {
        throw e;
    }
}

int ExecutionModule::getNumEmployees(const std::string& treeName) const
{
    std::size_t index;
    try
    {
        index = findIndexTree(treeName);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }

    return stored_trees[index]->num_employees();
}

int ExecutionModule::getNumOverloaded(const std::string& treeName) const
{
    std::size_t index;
    try
    {
        index = findIndexTree(treeName);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }

    return stored_trees[index]->num_overloaded();
}

bool ExecutionModule::fireEmployee(const std::string& treeName, const std::string& employeeName) 
{
    std::size_t index;
    try
    {
        index = findIndexTree(treeName);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }
    
    return stored_trees[index]->fire(employeeName);
}

bool ExecutionModule::hireEmployee(const std::string& treeName, const std::string& employeeName, const std::string& bossName) 
{
    std::size_t index;
    try
    {
        index = findIndexTree(treeName);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }

    return stored_trees[index]->hire(employeeName, bossName);
}

unsigned long ExecutionModule::getSalary(const std::string& treeName, const std::string& employeeName) const
{
    std::size_t index;
    try
    {
        index = findIndexTree(treeName);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }

    return stored_trees[index]->getSalary(employeeName);
}

void ExecutionModule::incorporate(const std::string& treeName) const
{
    std::size_t index;
    try
    {
        index = findIndexTree(treeName);
    }
    catch(const std::out_of_range& e)
    {
        throw e;
    }

    stored_trees[index]->incorporate();
}

std::size_t ExecutionModule::findIndexTree(const std::string& treeName) const
{
    std::size_t size = stored_trees.size();
    for(std::size_t i = 0; i < size; i++)
    {
        if(stored_trees[i]->getName() == treeName)
            return i;
    }
    throw std::out_of_range("No object with this name exists");
}

void ExecutionModule::openFile(std::fstream& file, const char* fileName, const std::string& flag) 
{
    if(flag == "out")
        file.open(fileName, std::ios::out);
    else
        file.open(fileName, std::ios::in);
    
    if (file.fail() || !file.is_open())
        throw std::invalid_argument("Can`t open file");
}

ExecutionModule::~ExecutionModule() 
{
    for(Hierarchy* hiearchy : stored_trees)
    {
        delete hiearchy;
    }
}

