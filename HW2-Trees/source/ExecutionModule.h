#pragma once
#include "../fileFormats/hierarchy.h"
#include <vector>
#include <string>
#include <fstream>

class ExecutionModule
{
    std::vector<Hierarchy*> stored_trees;
public:
    ~ExecutionModule();
    void loadFile(const char* treeName, const char* fileName);
    void saveFile(const char* treeName, const char* fileName);
    bool findEmployee(const std::string& treeName, const std::string& employeeName) const;
    int numSubordinates(const std::string& treeName, const std::string& employeeName) const;
    std::string getManager(const std::string& treeName, const std::string& employeeName) const;
    int getNumEmployees(const std::string& treeName) const;
    int getNumOverloaded(const std::string& treeName) const;
    bool fireEmployee(const std::string& treeName, const std::string& employeeName);
    bool hireEmployee(const std::string& treeName, const std::string& employeeName, const std::string& bossName);
    unsigned long getSalary(const std::string& treeName, const std::string& employeeName) const;
    void incorporate(const std::string& treeName) const;

private:
    std::size_t findIndexTree(const std::string& treeName) const;
    void openFile(std::fstream& file, const char* fileName, const std::string& flag);
};