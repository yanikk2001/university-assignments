#pragma once

#include "../observer-framework/observer.h"
#include "../observer-framework/subject.h"
#include "ExecutionModule.h"
#include <vector>
#include <functional>

enum CommandType
{
    HELP,
    LOAD,
    SAVE,
    FIND,
    NUM_SUBORDINATES,
    MANAGER,
    NUM_EMPLOYEES,
    OVERLOADED,
    JOIN,
    FIRE,
    HIRE,
    SALARY,
    INCORPORATE,
    MODERNIZE,
    EXIT,
    INVALID
};

class Controller : public Observer, public Subject
{
    ExecutionModule execModule;

    //Used when converting from string to enum CommandType, the order matters! Index here represents value in the enum
    std::vector<std::string> availableCommands = {
    "HELP",
    "LOAD",
    "SAVE",
    "FIND",
    "NUM_SUBORDINATES",
    "MANAGER",
    "NUM_EMPLOYEES",
    "OVERLOADED",
    "JOIN",
    "FIRE",
    "HIRE",
    "SALARY",
    "INCORPORATE",
    "MODERNIZE",
    "EXIT"
};

public:
    Controller(Subject &subject) : Observer(subject) {}
    Controller() : Observer(), Subject() {}

    virtual void Update(const std::string &message_from_subject_input) override;

private:
    std::vector<std::string> proccessInput(const std::string& input); //Separates command into strings
    const void toUpper(std::string& command);
    const CommandType stringToCommand(const std::string &command); //Converts to enum CommandType
    void setAndNotify(std::string message); //Notifies view

    /*
        Catches common exceptions that may occur during run time
        @param func - function to be executed
        @param arguments - to be checked for validity and passed to input function
        @param numArgs - minimum number of arguments
    */
    bool safeExecute(std::function<void(void)> func, std::vector<std::string>& arguments, unsigned numArgs);
};
