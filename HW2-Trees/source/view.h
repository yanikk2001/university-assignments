#pragma once
#include "..\observer-framework\observer.h"
#include "..\observer-framework\subject.h"

class View : public Observer, public Subject //Singleton class
{
//Input section
public:
    /*
        Sets the field: message, Notifies all observers of the new command
    */
    void getInput(std::string inputCommand);

//Display section
private:
    View(Subject &subject);
    View();

public:
    static View& getInstance();

    /*
        Attaches observer to subject
        @param subject sbj to be attached to
        @return ref to class View  
    */
    static View& getInstance(Subject& Subject);

    /*
        Adds display functionality, prints all updates recieved
    */
    virtual void Update(const std::string &message_from_subject_input) override;

    View(const View& other) = delete;
    View& operator=(const View& other) = delete;
    View(const View&& other) = delete;
    View& operator=(View&& other) = delete;
};