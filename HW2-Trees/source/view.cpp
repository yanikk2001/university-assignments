#include <iostream>
#include "view.h"

void View::getInput(std::string inputCommand) 
{
    this->setMessageSbj(inputCommand);
    this->Notify(); 
}

View::View(Subject &subject) : Observer(subject){}

View::View() : Observer(), Subject(){}

View& View::getInstance() 
{
    static View v;
    return v;
}

View& View::getInstance(Subject& Subject) 
{
    static View v(Subject);
    return v;
}

void View::Update(const std::string &message_from_subject_input) 
{
    Observer::Update(message_from_subject_input);
    std::cout << message_from_subject_input << std::endl;
}




