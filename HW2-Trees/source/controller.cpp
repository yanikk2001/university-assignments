#include "./controller.h"

std::string helpstr = 
"load object_name file_name - loads hierarchy data from a file with the specified name and creates a tree associated with object_name.\n"
"save object_name file_name - saves the hierarchy information of the specified object to a file with the specified name.\n"
"find object_name employee_name - checks if there is an employee with the specified name in the specified object;\n"
"num_subordinates object_name employee_name - displays the number of direct subordinates of the employee in the specified object;\n"
"manager name_of_object name_of_employee - displays the name of the manager of the given employee in the specified object;\n"
"num_employees object_name - displays the number of employees in the specified object;\n"
"overloaded object_name - displays the number of employees in the specified object for which the number of subordinates (direct or not) is greater than 20;\n"
"join object_name_1_object_name_2 object_name_result - merges the two submitted objects into a new object named object_name_result;\n"
"fire object_name employee_name - removes the employee from the corresponding object;\n"
"hire name_of_object name_of_employee name_of_manager - appoints the employee in the respective site as subordinate to the submitted manager;\n"
"salary name_of_object name_of_employee - displays the salary of the employee;\n"
"incorporate object_name - incorporates the company; the operation is applied to the object object_name;\n"
"modernize object_name - modernizes the company; the operation is applied to the object object_name\n;"
"exit - terminates the program. For all new or changed objects after loading, ask the user if he wants to save them in a file.\n";

void Controller::Update(const std::string &message_from_subject_input) 
{
    Observer::Update(message_from_subject_input);
    
    std::vector<std::string> arguments = proccessInput(message_from_subject_input);
    CommandType command;
    toUpper(arguments[0]);
    command = stringToCommand(arguments[0]);

    switch (command)
    {
    case HELP:
    {
        setAndNotify(helpstr);
        break;
    }

    case LOAD:
    {
        auto func = [&]()
        {
            if(arguments.size() >= 3)
                execModule.loadFile(arguments[1].c_str(), arguments[2].c_str());
            else
               execModule.loadFile(arguments[1].c_str(), nullptr);
        };

        if(safeExecute(func, arguments, 2))
            setAndNotify("File loaded");
        break;
    }

    case SAVE:
    {
        auto func = [&]()
        {
            execModule.saveFile(arguments[1].c_str(), arguments[2].c_str());
        };
        if(safeExecute(func, arguments, 3))
            setAndNotify("File saved");

        break;
    }

    case FIND:
    {
        auto func = [&]()
        {
            if(execModule.findEmployee(arguments[1], arguments[2]))
                setAndNotify(std::string(arguments[2]) + std::string(" is employed in ") + std::string(arguments[1]));
            else
                setAndNotify(std::string("There is no such employee in ") + std::string(arguments[1]));
        };

        safeExecute(func, arguments, 3);
        break;
    }

    case NUM_SUBORDINATES:
    {
        auto func = [&]()
        {
            int num = execModule.numSubordinates(arguments[1], arguments[2]);
            if(num <= 0)
                setAndNotify(std::string(arguments[2]) + std::string(" has no employees"));
            else
                setAndNotify(std::string(arguments[2]) + std::string(" has ") + std::to_string(num) + std::string(" employees"));
        };

        safeExecute(func, arguments, 3);
        break;
    }

    case MANAGER:
    {
        auto func = [&]()
        {
            setAndNotify(execModule.getManager(arguments[1], arguments[2]));
        };

        safeExecute(func, arguments, 3);
        break;
    }

    case NUM_EMPLOYEES:
    {
        auto func = [&]()
        {
            setAndNotify(std::string(arguments[1] + " has " + std::to_string(execModule.getNumEmployees(arguments[1])) + " employees"));
        };

        safeExecute(func, arguments, 2);
        break;
    }

    case OVERLOADED:
    {
        auto func = [&]()
        {
            int numOfOverloaded = execModule.getNumOverloaded(arguments[1]);
            std::string answ1 = "Overloaded employees: " + std::to_string(numOfOverloaded);
            std::string res = (numOfOverloaded == 0) ? 
                std::string("No overloaded employees in " + arguments[1]) : answ1;
            setAndNotify(res);
        };

        safeExecute(func, arguments, 2);
        break;
    }

    case FIRE:
    {
        auto func = [&]()
        {
            bool res = execModule.fireEmployee(arguments[1], arguments[2]);
            std::string answ = res ? 
                std::string(arguments[2] + " was fired from " + arguments[1]) : std::string(arguments[2] + " does not work in " + arguments[1]);
            setAndNotify(answ);
        };

        safeExecute(func, arguments, 3);
        break;
    }

    case HIRE:
    {
        auto func = [&]()
        {
            bool res = execModule.hireEmployee(arguments[1], arguments[2], arguments[3]);
            std::string answ = res ? 
                std::string(arguments[2] + " was hired at " + arguments[1]) : std::string(arguments[2] + " could not be hired at " + arguments[1]);
            setAndNotify(answ);
        };

        safeExecute(func, arguments, 4);
        break;
    }

    case SALARY:
    {
        auto func = [&]()
        {
            unsigned long salary = execModule.getSalary(arguments[1], arguments[2]);
            setAndNotify(arguments[2] + " has " + std::to_string(salary) + " salary");
        };

        safeExecute(func, arguments, 3);
        break;
    }

    case INCORPORATE:
    {
        auto func = [&]()
        {
            execModule.incorporate(arguments[1]);
            setAndNotify("Incorporated");
        };

        safeExecute(func, arguments, 2);
        break;
    }
    
    case EXIT:
    {
        setAndNotify("Exiting...");
        break;
    }

    case INVALID:
        setAndNotify("Invalid command");
        break;
    
    default:
        setAndNotify("The end of the world");
        break;
    }

}

void Controller::setAndNotify(std::string message) 
{
    this->setMessageSbj(message);
    this->Notify();
}

bool Controller::safeExecute(std::function<void(void)> func, std::vector<std::string>& arguments, unsigned numArgs) 
{
    if(arguments.size() < numArgs)
    {
        setAndNotify(std::string("Invalid number of arguments, should be at least ") + std::to_string(numArgs)); 
        return false;
    }

    try
    {
        func();
    }
    catch(const std::out_of_range& e)
    {
        setAndNotify(e.what());
        return false;
    }
    catch(const std::invalid_argument& e)
    {
        setAndNotify(e.what());
        return false;
    }
    catch(const std::bad_alloc& e)
    {
        setAndNotify("Failed to allocate memory");
        return false;
    }
    return true;
}

std::vector<std::string> Controller::proccessInput(const std::string& input) 
{
    std::vector<std::string> subStr;

    std::size_t inputLen = input.length();
    std::size_t prevoiusPos = 0;
    std::size_t count = 0;
    for(std::size_t i = 0; i <= inputLen; i++)
    {
        //Handles arguments with quotes("")
        if(input[i] == '"')
        {
            std::size_t countQuote = 0;
            std::size_t startPos = i + 1;
            do
            {
                countQuote++;
                i++;
            } while (input[i] != '"');

            subStr.push_back(input.substr(startPos, countQuote - 1));
            return subStr;
        }

        //Handles normal input seprarated by spaces
        count++;
        if(input[i] == ' ' || input[i] == '\0')
        {
            subStr.push_back(input.substr(prevoiusPos, count - 1));

            prevoiusPos = i + 1;
            count = 0; 
        }
    }

    return subStr;
}

const void Controller::toUpper(std::string& command) 
{
    std::size_t size = command.size();
    for(std::size_t i = 0; i < size; i++)
    {
        command[i] = toupper(command[i]); 
    }
}

const CommandType Controller::stringToCommand(const std::string &command)
{
    std::size_t size = availableCommands.size();
    for(std::size_t i = 0; i < size; i++)
    {
        if(command == availableCommands[i])
            return static_cast<CommandType>(i);
    }
    return CommandType::INVALID;
}