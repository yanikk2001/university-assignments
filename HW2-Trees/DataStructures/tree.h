#pragma once
#include <stdexcept>
#include <vector>
#include <queue>

template <class DataType>
class Tree
{
    struct node
    {
        DataType data;
        node* child;
        node* sibling;
        node* parent;

        node(const DataType& data, node* parent = nullptr, node* child = nullptr, node* sibling = nullptr) :
            data(data), parent(parent), child(child), sibling(sibling){}

        std::vector<const node*> getSiblings() const
        {
            std::vector<const node*> siblings;
            if(!parent) return siblings;
            
            node* Child = parent->child;
            while(Child)
            {
                if(Child != this)
                {
                    siblings.push_back(Child);
                }
                Child = Child->sibling;
            }

            return siblings;
        }
    };

    node* root;
    std::size_t size;

public:
    Tree() : root(nullptr), size(0) {}
    Tree(const DataType& root);
    Tree(const Tree& other);
    Tree& operator=(const Tree& other);
    ~Tree() {clear(root);}

    bool contains(const DataType& key) const {return (find(root, key) == nullptr) ? false : true;}
    bool add(const DataType& parent, const DataType& key);
    bool add(const DataType& key);
    bool add(const DataType& parent, const DataType& key, bool (*func)(DataType, DataType));
    bool addSubTree(const DataType& parentName, const Tree<DataType>::node* key, bool (*func)(DataType, DataType));

    bool remove(const DataType& key) { return remove(root, key); }
    void clear() {clear(root); size = 0;}

    size_t getSize() const { return size; }
    size_t getHeight() const { return height(root); }
    size_t height(const node* root) const;

    const Tree<DataType>::node* get(DataType key) const;

    std::vector<DataType> bfsPrint() const {return bfsPrint(root);}
    std::vector<DataType> bfsPrint(const DataType& node) const;
    std::vector<const node*> bfsPrintNodes() const {return bfsPrintNodes(root);}


private:
    void clear(node* root);

    node* copy(node* root);

    Tree<DataType>::node* find(node* i_root, const DataType& key) const;

    bool remove(node*& root, const DataType& key);

    Tree<DataType>::node* lastElement(node* input_node) const;

    void changeParent(node* n, node* n_parent);

    //Does not free memory
    void softRemove(node* key);

    std::vector<DataType> bfsPrint(const node* root) const;

    std::vector<const node*> bfsPrintNodes(const node* root) const //for some reason cannot write implementation in .inc file
    {
        std::vector<const node*> vec;
        if (!root) return vec;
        std::queue<const node*> front;
        front.push(root);
        front.push(nullptr);
        while(true)
        {
            const node* current = front.front();
            front.pop();
            if (!current) {
                if (front.empty()) return vec;
                front.push(nullptr);
            }
            else {
                vec.push_back(current);
                for (const node* it = current->child; it; it = it->sibling) {
                    front.push(it);
                }
            }
        }
        return vec;
    }
};
  
#include "./tree.inc"