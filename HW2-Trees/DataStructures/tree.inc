template<class DataType>
Tree<DataType>::Tree(const DataType& root) 
{
    try
    {
        this->root = new node(root);
        this->size = 1;
    }
    catch(const std::bad_alloc& e)
    {
        throw e;
    }
}

template<class DataType>
Tree<DataType>::Tree(const Tree& other) 
{
    try
    {
        root = copy(other.root);
        size = other.size;
    }
    catch(const std::bad_alloc& e)
    {
        throw e;
    }
}

template<class DataType>
Tree<DataType>& Tree<DataType>::operator=(const Tree& other) 
{
    if(this != &other)
    {
        clear(root);
        root = copy(other.root);
        size = other.size;
    }
    return *this;
}

template<class DataType>
bool Tree<DataType>::add(const DataType& parentName, const DataType& key) 
{
    Tree<DataType>::node* parent = find(root, parentName);
    if(!parent) return false;
    node* it = lastElement(parent->child);
    try
    {
        if(!it)
            parent->child = new node(key, parent);
        else
            it->sibling = new node(key, parent);    
    }
    catch(const std::bad_alloc& e)
    {
        return false;
    }
    size++;
    return true;
}

template<class DataType>
bool Tree<DataType>::add(const DataType& key) 
{
    if(!root)
    {
        try
        {
            root = new node(key);
        }
        catch(const std::bad_alloc& e)
        {
            return false;
        }
        size++;
        return true;
    }
    return false;
}

template<class DataType>
bool Tree<DataType>::add(const DataType& parentName, const DataType& key, bool (*func)(DataType, DataType)) 
{
    try
    {
        Tree<DataType>::node* parent = find(root, parentName);
        if(!parent) return false;
        auto it = parent->child;
        if(!it) //Parent element has no children
        {
            parent->child = new node(key, parent);
            size++;
            return true;
        }
        
        //Has children nodes
        if(func(key, it->data)) //new node should be infront of all other child nodes
        {
            Tree<DataType>::node* n_node = new node(key, parent, nullptr, it);
            parent->child = n_node;
        }
        else
        {
            while(it->sibling) //Search through all siblings
            {
                if(func(key, it->sibling->data))
                {
                    node* n_node = new node(key, parent, nullptr, it->sibling); //Insert new node somewhere between
                    it->sibling = n_node;
                    size++;
                    return true;
                }
                it = it->sibling;
            }
            it->sibling = new node(key, parent); //add new node to the end
        }
    }
    catch(const std::bad_alloc& e)
    {
        return false;
    }

    size++;
    return true;
}

template<class DataType>
void Tree<DataType>::softRemove(node* key) 
{
    if(!key) return;

    node* oldParent = key->parent;
    if(!oldParent) return;

    node* childParent = oldParent->child;
    if(childParent == key)
    {
        childParent = key->sibling;
        oldParent->child = childParent;
        return;
    }

    node* cpy;
    while(childParent)
    {
        if(childParent == key)
        {
            cpy->sibling = childParent->sibling;
            childParent = nullptr;
            return;
        }
        cpy = childParent;
        childParent = childParent->sibling;
    }
}

template<class DataType>
bool Tree<DataType>::addSubTree(const DataType& parentName, const Tree<DataType>::node* key, bool (*func)(DataType, DataType)) 
{
    Tree<DataType>::node* parent = find(root, parentName);
    if(!parent || !key) return false;
    auto it = parent->child;

    node* n_key = const_cast<node*>(key);
    if(find(n_key->child, parentName)) //Don`t want the boss to be employee of key
        return false;

    softRemove(n_key);
    n_key->sibling = nullptr;
    n_key->parent = parent;

    if(!it) //Parent element has no children
    {
        parent->child = n_key; //Insert at the start
        return true;
    }
    
    //Has children nodes
    if(func(n_key->data, it->data)) //new node should be infront of all other child nodes
    {
        n_key->sibling = it;
        parent->child = n_key;
    }
    else
    {
        while(it->sibling) //Search through all siblings
        {
            if(func(n_key->data, it->sibling->data))
            {
                n_key->sibling = it->sibling; //Insert new node somewhere between
                it->sibling = n_key;
                return true;
            }
            it = it->sibling;
        }
        it->sibling = n_key; //add new node to the end
    }

    return true;
}

template<class DataType>
void Tree<DataType>::clear(node* root) 
{
    if(root)
    {
        clear(root->child);
        clear(root->sibling);
        delete root;
    }
}

template<typename DataType>
typename Tree<DataType>::node* Tree<DataType>::copy(node* root) 
{
    node* n_node = nullptr;
    if(root)
    {
        try
        {
            n_node = new node(root->data, nullptr, copy(root->child), copy(root->sibling));
        }
        catch(const std::bad_alloc& e)
        {
            throw e;
        }
    }
    return n_node;
}

template<class DataType>
const typename Tree<DataType>::node* Tree<DataType>::get(DataType key) const
{
    return find(root, key);
}

template<class DataType>
typename Tree<DataType>::node* Tree<DataType>::find(node* i_root, const DataType& key) const
{
    if(!i_root || i_root->data == key) return i_root;
    
    auto i = find(i_root->sibling, key);
    if(i) return i;
    auto j = find(i_root->child, key);
    if(j) return j;
}

template<class DataType>
void Tree<DataType>::changeParent(node* n, node* n_parent) 
{
    node* cpy = n;
    while(cpy)
    {
        cpy->parent = n_parent;
        cpy = cpy->sibling;
    }
}

template<class DataType>
bool Tree<DataType>::remove(node*& root, const DataType& key) 
{
    if (!root) return false;

    if (root->data == key) {
        node* toDelete = root;
        if (!root->child) {
            root = root->sibling;
        }
        else if (root->sibling && root->child){
            node* it = lastElement(root->child);
            it->sibling = root->sibling->child;
            root->sibling->child = root->child;
            root = root->sibling;
            changeParent(root, toDelete->parent);
        }
        else {
            root = root->child;
            changeParent(root, toDelete->parent);
        }
        --size;
        delete toDelete;
        return true;
    }
    else return remove(root->sibling, key) || remove(root->child, key);
}

template<class DataType>
size_t Tree<DataType>::height(const node* root) const
{
    if (!root) return 0;
    return std::max(1+height(root->child), height(root->sibling));
}

template<class DataType>
std::vector<DataType> Tree<DataType>::bfsPrint(const node* root) const
{
    std::vector<DataType> vec;
    if (!root) return vec;
    std::queue<const node*> front;
    front.push(root);
    front.push(nullptr);
    while(true)
    {
        const node* current = front.front();
        front.pop();
        if (!current) {
            if (front.empty()) return vec;
            front.push(nullptr);
        }
        else {
            vec.push_back(current->data);
            for (const node* it = current->child; it; it = it->sibling) {
                front.push(it);
            }
        }
    }
    return vec;
}

template<class DataType>
std::vector<DataType> Tree<DataType>::bfsPrint(const DataType& node) const
{
    const Tree<DataType>::node* n = get(node);
    if(!n)
        throw std::invalid_argument("No such entity exists in this tree");
    
    return bfsPrint(n);
}

template<class DataType>
typename Tree<DataType>::node* Tree<DataType>::lastElement(node* input_node) const
{
    if(!input_node) return nullptr;
    node* last = input_node;
    while(last->sibling)
        last = last->sibling;

    return last;
}


