#include "subject.h"
#include "observer.h"

void Subject::Attach(IObserver *observer) 
{
    this->list_observers.push_back(observer);
}

void Subject::Detach(IObserver *observer) 
{
    this->list_observers.remove(observer);
}

void Subject::Notify() 
{
    std::list<IObserver *>::iterator iterator = list_observers.begin();
    while (iterator != list_observers.end())
    {
      (*iterator)->Update(message);
      ++iterator;
    }
}

void Subject::setMessageSbj(std::string& message_input) 
{
    this->message = message_input;
}

const std::string& Subject::getMessageSbj() const
{
    return this->message;
}
