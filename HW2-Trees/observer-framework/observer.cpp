#include "observer.h"
#include "subject.h"


Observer::Observer(Subject &subject_input) : subject(&subject_input)
{
    this->subject->Attach(this);
}

Observer::Observer()
{
    this->subject = nullptr;
}

void Observer::Update(const std::string &message_from_subject_input) 
{
    this->message_from_subject = message_from_subject_input;
}

void Observer::RemoveMeFromTheList() 
{
    if(!subject)
        this->subject->Detach(this);
}

const std::string& Observer::getMessageObs() const
{
    return this->message_from_subject;
}
