#pragma once
#include <string>
#include <list>

// #include "observer.h"
class IObserver;

//Interface class
class ISubject 
{
 public:
  virtual ~ISubject(){};

  /*
    Adds obser to sbj`s list
    @param observer to be added
  */
  virtual void Attach(IObserver *observer) = 0;

  /*
    Removes obser from sbj`s list
    @param observer to be removed
  */
  virtual void Detach(IObserver *observer) = 0;

  //Calls Update() on all observers from list
  virtual void Notify() = 0;
};

class Subject : public ISubject
{
    std::list<IObserver *> list_observers;
    std::string message;

public:
    Subject() = default;
    virtual void Attach(IObserver *observer) override;
    virtual void Detach(IObserver *observer) override;
    virtual void Notify() override;
    void setMessageSbj(std::string& message_input);
    const std::string& getMessageSbj() const;
};