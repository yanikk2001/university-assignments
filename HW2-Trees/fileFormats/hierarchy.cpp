#include "./hierarchy.h"

int findFirstOccurance(const std::string& str, std::size_t startPos = 0, char c = ' ')
{
    int size = str.length();
    for(int i = startPos; i < size; i++)
    {
        if(str[i] == c || str[i] == '\n')
            return i;
    }
    return -1;
}
int findFirstAbsence(const std::string& str, std::size_t startPos = 0, char c = ' ')
{
    int size = str.length();
    for(int i = startPos; i < size; i++)
    {
        if(str[i] != c)
            return i;
    }
}

void Hierarchy::load(std::istream& in) 
{
    std::string row;
    while(true)
    {
        std::getline(in, row);

        if(in.eof()) break;

        if(row.size() == 0) //Skips blank rows
            continue;

        row.erase(std::remove_if(row.begin(), row.end(), ::isspace), row.end()); //remove white spaces

        if(!isValid(row))
        {
            throw std::invalid_argument("Invalid file format");
        }

        std::string manager;
        std::string employee;

        // determineManagerEmployee(manager, employee, row);


        std::size_t posOfDash = row.find_first_of('-');

        manager = row.substr(0, posOfDash);
        employee = row.substr(posOfDash+1, row.length());

        if(tree.contains(employee))
        {
            tree.clear();
            throw std::invalid_argument(std::string(employee) + std::string(" already exists in this hierarchy"));
        }

        if(!tree.add(manager, employee, &lexicographicalCompare))
        {
            throw std::invalid_argument("Manager not found");
        }
    }
}

void Hierarchy::save(std::ostream& out) const
{
   std::vector<std::string> vec = tree.bfsPrint();
    std::size_t size = vec.size();
    for(std::size_t i = 0; i < size; i++)
    {
        auto node = tree.get(vec[i]);
        if(node->parent)
        {
            out << node->parent->data << '-' << node->data << std::endl;
        }
    }
}

bool Hierarchy::find(const std::string& name) const
{
    return tree.contains(name);
}

bool Hierarchy::isValid(std::string& row) 
{
    std::size_t CountDash = 0;
    std::size_t CountSpace = 0;
    for(char c : row)
    {
        if(c == '-')
            CountDash++;
        else if(c == ' ')
            CountSpace++;
    }
    return (CountDash == 1 && CountSpace == 0);
}

bool Hierarchy::lexicographicalCompare(std::string a, std::string b) 
{
    return a < b;
}

void Hierarchy::determineManagerEmployee(std::string& manager, std::string& employee, const std::string& row) 
{
    int pos = findFirstAbsence(row); //First letter of manager`s name
    int pos1 = findFirstOccurance(row, pos, '-');
    int pos2 = findFirstOccurance(row, pos, ' ');
    int pos21 = (pos2 == -1) ? pos1 : std::min(pos1, pos2); //last letter of manager`s name
    int managerLength = std::abs(pos21 - pos);
    
    int pos3 = std::max(pos1, pos2);
    int pos4 = findFirstAbsence(row, pos3+1, ' '); //First letter of employee`s name
    int pos5 = findFirstOccurance(row, pos4, ' '); //last letter of employee`s name
    std::cout << pos5 << "\n";
    int empoloyeeLength = std::abs(pos5 - pos4);

    manager = row.substr(pos, managerLength);
    employee = row.substr(pos4, empoloyeeLength);

    std::cout << row << "\n";
    std::cout << pos <<' ' << managerLength << ' '<< pos4 << ' ' << empoloyeeLength << "\n";

    std::cout << manager << " " << employee << "\n";
}

std::string Hierarchy::highestPaid(std::vector<std::string>& children) 
{
    std::size_t size = children.size();
    std::string highestSalary = children[1];
    for(std::size_t i = 2; i < size; i++)
    {
        unsigned long salary1 = getSalary(children[i]);
        unsigned long salary2 = getSalary(highestSalary);
        if(salary1 > salary2)
            highestSalary = children[i];
        else if(salary1 == salary2)
        {
            if(lexicographicalCompare(children[i], highestSalary))
                highestSalary = children[i];
        }
    }

    return highestSalary;
}

Hierarchy::Hierarchy(const string& data) 
{
    if(data.size() == 0)
        return;

    if(!tree.add(DEFAULT_ROOT)) //Set Uspeshnia as root
        throw std::bad_alloc();
    
    size_t newLineCount = std::count(data.begin(), data.end(), '\n');
    size_t dashCount = std::count(data.begin(), data.end(), '-');

    if(newLineCount != dashCount || dashCount == 0)
        throw std::invalid_argument("Invalid format");
    
    std::istringstream is(data);
    try
    {
        load(is);
    }
    catch(const std::invalid_argument& e)
    {
        throw e;
    }
}

string Hierarchy::print() const
{
    std::ostringstream sStream;
    save(sStream);
    return sStream.str();
}

int Hierarchy::longest_chain() const
{
    return tree.getHeight();
}

int Hierarchy::num_employees() const
{
    return tree.getSize();
}

int Hierarchy::num_overloaded(int level) const
{
    int count = 0; // We know for sure that the root is > level
    auto nodes = tree.bfsPrint();

    for(auto node : nodes)
    {
        auto children = tree.bfsPrint(node);
        if(int(children.size()-1) > level)
            count++;
    }

    return count;
}

string Hierarchy::manager(const string& name) const
{
    auto employee = tree.get(name);
    if(!employee)
        throw std::invalid_argument(name + std::string(" does not exist in the hierarchy"));
    
    auto manager = employee->parent;
    return manager ? manager->data : "";
}

int Hierarchy::num_subordinates(const string& name) const
{
    auto parent = tree.get(name);
    if(!parent)
        return -1;

    auto child = parent->child;
    int num = 0;
    while(child)
    {
        num++;
        child = child->sibling;
    }
    return num;
}

unsigned long Hierarchy::getSalary(const string& who) const
{
    if(!tree.contains(who))
        return -1;
    std::vector<std::string> vec = tree.bfsPrint(who);
    std::size_t allEmployees = vec.size() - 1;
    std::size_t directEmployees = num_subordinates(who);
    std::size_t indirectEmployees = allEmployees - directEmployees;

    return 500* directEmployees + 50* indirectEmployees;
}

bool Hierarchy::fire(const string& who) 
{
    if(who == DEFAULT_ROOT)
        return false;
    return tree.remove(who);
}

bool Hierarchy::hire(const string& who, const string& boss) 
{
    if(who == DEFAULT_ROOT)
        throw std::invalid_argument("Can`t hire " + DEFAULT_ROOT);
    
    auto employee = tree.get(who);
    if(employee)
    {
        if(employee->parent->data == boss)
            return false;

        return tree.addSubTree(boss, employee, &lexicographicalCompare);
    } 
    else return tree.add(boss, who, &lexicographicalCompare);
}

void Hierarchy::incorporate() 
{
    auto employees = tree.bfsPrint();
    for(auto i = employees.end()-1; i != employees.begin(); i--)
    {
        std::cout << *i << "\n";
        auto employee = tree.get(*i);
        auto siblings = employee->getSiblings();
        if(siblings.size() > 0)
        {
            std::vector<std::string> cpySiblings;
            for(auto sibling : siblings)
            {
                cpySiblings.push_back(sibling->data);
            }
            cpySiblings.push_back(employee->data);
            std::string highestPaidEmp = highestPaid(cpySiblings);
            for(auto sibling : siblings)
            {
                tree.addSubTree(highestPaidEmp, sibling, &lexicographicalCompare);
            }
        }
    }
}

void Hierarchy::setName(const std::string i_name) 
{
    name = i_name;
}

Hierarchy::Hierarchy() 
{
    if(!tree.add(DEFAULT_ROOT)) //Set Uspeshnia as root
        throw std::bad_alloc();
}
