#pragma once

#include "../DataStructures/tree.h"
#include <iostream>
#include <sstream>
#include <algorithm> //remove if
#include <cctype> // is whiteSpace
using std::string;

const std::string DEFAULT_ROOT = "Uspeshnia";

class Hierarchy
{
public:
    Hierarchy();
    Hierarchy(const string& data);
    Hierarchy(Hierarchy&& r) noexcept : name{r.name}, tree{r.tree} {}
    Hierarchy(const Hierarchy& r) : name{r.name}, tree{r.tree} {}
    ~Hierarchy() noexcept {name.clear();}
    void operator=(const Hierarchy&) = delete;

    string print()const;

    int longest_chain() const;
    bool find(const string& name) const;
    int num_employees() const;
    int num_overloaded(int level = 20) const;

    string manager(const string& name) const;
    int num_subordinates(const string& name) const;
    unsigned long getSalary(const string& who) const;

    bool fire(const string& who);
    bool hire(const string& who, const string& boss);

    void incorporate();
    // void modernize();

    // Hierarchy join(const Hierarchy& right) const;

    void setName(const std::string i_name);
    const std::string& getName() const {return name;}
    void load(std::istream& in);
    void save(std::ostream& out) const;

private:
    string name;
    Tree<std::string> tree;

    bool isValid(std::string& row);
    static bool lexicographicalCompare(std::string a, std::string b);
    void determineManagerEmployee(std::string& manager, std::string& employee, const std::string& row);
    std::string highestPaid(std::vector<std::string>& children);
};