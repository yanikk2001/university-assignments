#include "catch2/catch_all.hpp"
#include "RleString.h"

TEST_CASE("Test getNodeCount")
{
    SECTION("Empty object")
    {
        RleString r;
        REQUIRE(r.getNodeCount() == 0);

        RleString k("");
        REQUIRE(k.getNodeCount() == 0);
    }

    SECTION("Not so empty object when created")
    {
        RleString r("abc");
        REQUIRE(r.getNodeCount() == 3);

        RleString r1("aaaabc");
        REQUIRE(r1.getNodeCount() == 3);

        RleString r2("aaabbccc");
        REQUIRE(r2.getNodeCount() == 3);
    }

    SECTION("NodeCount is correct when using copy ctor")
    {
        RleString r("abc");
        RleString c(r);

        REQUIRE(r.getNodeCount() == 3);
        REQUIRE(c.getNodeCount() == r.getNodeCount());
    }

    SECTION("NodeCount is correct when using copy assignment")
    {
        RleString r("abc");
        RleString c("");

        c = r;

        REQUIRE(c.getNodeCount() == r.getNodeCount());
    }
}