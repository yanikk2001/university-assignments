#include "RleString.h"

RleString::RleString(const std::string& str) 
{
    if(str != "")
    {
        begin = allocMemory(str[0]);
        Node* curr = begin;
        for(auto iter = ++str.begin(); iter != str.end(); iter++) 
        {
            if(*iter == curr->symbol)
            {
                curr->count++;
            }
            else
            {
                curr->next = allocMemory(*iter);
                curr = curr->next;
            }
        }
        end = curr;
    }
}

void RleString::freeMemory() 
{
    Node * iter = begin;
    Node * detach = begin;

    while(iter) 
    {
        detach = iter;
        iter = iter->next;
        allocator.free(detach);
    }

    end = begin = nullptr;
    nodeCount = 0;
}

RleString::Node* RleString::allocMemory(const char &symbol, const size_t &count, Node *next) 
{
    try
    {
        Node* ptr = allocator.allocate(symbol, count, next);
        nodeCount++;
        return ptr;
    }
    catch(const std::exception& e)
    {
        freeMemory();
        throw e;
    }
    
}

RleString::Node* RleString::at(const size_t& index) const
{
    Node* iter = begin;
    size_t count = begin->count;
    for (size_t i = 0; i < index; i++)
    {
        count--;
        if(!count)
        {
            iter = iter->next;
        }
    }
    return iter;
}

RleString::RleString(const RleString& other) 
{
    if(this != &other) 
    {
        Node dummy('a');
        Node * iter = &dummy;
        Node * curr = other.begin;
        while (curr) 
        {
            iter->next = allocMemory(curr->symbol, curr->count, curr->next);
            iter = iter->next;
            curr = curr->next;
        }
        begin = dummy.next;
        end = begin ? iter : nullptr;
        nodeCount = other.nodeCount;
    }
}

RleString& RleString::operator=(const RleString& other) 
{
    if(this != &other)
    {
        RleString copy(other);
        std::swap(begin, copy.begin);
        std::swap(end, copy.end);
        std::swap(nodeCount, copy.nodeCount);
        std::swap(allocator, copy.allocator);
    }
    return *this;
}

std::string RleString::toString() const
{
    if(!begin)
        return "";

    const Node* curr = begin;
    std::string str;
    size_t symbolCount = curr->count;
    while(curr)
    {
        if(symbolCount == 0)
        {
            curr = curr->next;
            if(curr)
                symbolCount = curr->count;
        }
        else
        {
            symbolCount--;
            str.push_back(curr->symbol);
        }
    }
    return str;
}

bool RleString::operator==(const RleString& other) const
{
    if(nodeCount != other.nodeCount)
        return false;

    const Node* curr = begin;
    const Node* otherCurr = other.begin;

    // Checks if they are both either nullptrs or valid ptrs
    if(bool(curr) != bool(otherCurr))
        return false;

    while(curr)
    {
        // Checks if they have matching symbols and symbol counts
        if(!(*curr == *otherCurr))
            return false;
        
        // Does a check in case one of the rle strings is shorter
        if(bool(curr->next) != bool(otherCurr->next))
            return false;

        curr = curr->next;
        otherCurr = otherCurr->next;
    }
    return true;
}

bool RleString::operator==(const std::string& str) const
{
    std::string str1 = toString();
    return str1 == str;
}

size_t RleString::size() const noexcept
{
    const Node* curr = begin;
    size_t size = 0;
    while(curr)
    {
        size += curr->count;
        curr = curr->next;
    }
    return size;
}

void RleString::insertAt(size_t index, char value) 
{
    if (index > size()) 
    {
        throw std::out_of_range("Index out of bounds\n");
    }

    if (index == 0) 
    {
        if(begin && begin->symbol == value)
        {
            begin->count++;
            return;
        }
        if(begin && begin->next && begin->next->symbol == value)
        {
            begin->next->count++;
            return;
        }
        
        begin = allocMemory(value, 1, begin);
        if (!(nodeCount - 1)) //Checks if before that the rlestring was empty
            end = begin;
    }
    else if (index == nodeCount) 
    {
        if(end && end->symbol == value)
        {
            end->count++;
            return;
        }
        Node* prevNode = at(index-1);
        if(prevNode->symbol == value)
        {
            prevNode->count++;
            return;
        }

        end->next = allocMemory(value);
        end = end->next;
    }
    else 
    {
        Node* prevNode = at(index-1);
        Node* currNode = prevNode->next;
        Node* nextNode = currNode->next;
        if(prevNode->symbol == value)
        {
            prevNode->count++;
            return;
        }
        if(currNode && currNode->symbol == value)
        {
            currNode->count++;
            return;
        }
        if(currNode && nextNode && nextNode->symbol == value)
        {
            nextNode->count++;
            return;
        }

        Node * iter = prevNode;
        iter->next = allocMemory(value, 1, iter->next);
    }
}

void RleString::removeAt(size_t index) 
{
    if (nodeCount == 0 || index >= size()) {
        throw std::out_of_range("Index out of bounds\n");
    }

    Node * detach = begin;

    if (index == 0) {
        begin->count--;
        if(!begin->count)
        {
            begin = begin->next;
            if (begin == nullptr) {
                end = nullptr;
            }

            --nodeCount;
            allocator.free(detach);
        }
    }
    else {
        Node * iter = at(index-1);
        detach = iter->next;
        detach->count--;
        if(!detach->count)
        {
            iter->next = detach->next;

            if (detach == end) {
                end = iter;
            }

            --nodeCount;
            allocator.free(detach);
        }
    }
}

void RleString::reverse() 
{
    if (size() < 2 || nodeCount < 2) {
        return;
    }

    Node * curr = begin;
    Node * perv = nullptr;
    Node * next = nullptr;
    end = begin;

    while (curr != nullptr) {
        next = curr->next;
        curr->next = perv;
        perv = curr;
        curr = next;
    }

    begin = perv;
}

bool RleString::contains(const RleString& rle) const
{
    const Node* thisCurr = begin;
    const Node* otherCurr = rle.begin;
    if(thisCurr == otherCurr || otherCurr == nullptr) return true;
    size_t matchingNodes = 0;

    while(thisCurr) 
    {
        if(otherCurr->symbol == thisCurr->symbol)
        {
            matchingNodes++;
            otherCurr = otherCurr->next;
        }
        else 
        {
            matchingNodes = 0;
            otherCurr = rle.begin;
        }

        if(matchingNodes == rle.getNodeCount()) return true;

        thisCurr = thisCurr->next;
    }
    return false;
}
