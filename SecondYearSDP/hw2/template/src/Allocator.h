#pragma once

template <typename T>
class Allocator
{
public:
    virtual T *allocate(const char &symbol, const size_t &count = 1, T *next = nullptr) = 0;
    virtual void free(T *arr) = 0;
    virtual std::size_t getSizeOfType() { return sizeof(T); }
};

template <typename T>
class DefAllocator : public Allocator<T>
{
public:
    virtual T *allocate(const char &symbol, const size_t &count = 1, T *next = nullptr) override
    {
        try
        {
            return new T(symbol, count, next);
        }
        catch (const std::bad_alloc &e)
        {
            throw e;
        }
    }

    virtual void free(T *el) override
    {
        delete el;
    }
};