#include "catch2/catch_all.hpp"
#include "solution.h"

TEST_CASE("isCorrect() recognizes correct expressions")
{
	SECTION("Empty expression") {
		REQUIRE(isCorrect(""));
	}
	SECTION("No brackets") {
		REQUIRE(isCorrect("abc"));
	}
	SECTION("One pair of brackets") {
		REQUIRE(isCorrect("(abc)"));
	}
	SECTION("Multiple brackets") {
		REQUIRE(isCorrect("(((abc)))"));
	}
	SECTION("Mixed brackets") {
		REQUIRE(isCorrect("([abc])"));
	}
	SECTION("Complex expression") {
		REQUIRE(isCorrect("((abc) { abc }(((def)))def)"));
	}
	SECTION("Single pair of brackets") {
		REQUIRE(isCorrect("()"));
	}
	SECTION("Brackets-only") {
		REQUIRE(isCorrect("((()))"));
	}
	SECTION("Brackets next to each other") {
		REQUIRE(isCorrect("{}[](){}[]()"));
	}
	SECTION("More tests") {
		REQUIRE(isCorrect("{[()]}"));
	}
	SECTION("More tests") {
		REQUIRE(isCorrect("{(hhsh{djjn}mdk)kdkd}"));
	}
}

TEST_CASE("isCorrect() recognizes when an expression is incorrect")
{
	SECTION("No closing bracket") {
		REQUIRE_FALSE(isCorrect("(abc"));
	}
	SECTION("No closing bracket and multiple brackets") {
		REQUIRE_FALSE(isCorrect("(((abc))"));
	}
	SECTION("No opening bracket") {
		REQUIRE_FALSE(isCorrect("abc)"));
	}
	SECTION("No opening bracket and mutliple brackets") {
		REQUIRE_FALSE(isCorrect("(abc))"));
	}
	SECTION("Single opening bracket") {
		REQUIRE_FALSE(isCorrect("("));
	}
	SECTION("Single closing bracket") {
		REQUIRE_FALSE(isCorrect(")"));
	}
	SECTION("Bracket types do not match") {
		REQUIRE_FALSE(isCorrect("{abc]"));
	}
	SECTION("Overlapping bracket sub-expressions") {
		REQUIRE_FALSE(isCorrect("(abcdef[)]"));
	}
}

TEST_CASE("getIndexOfSymbolInArr")
{
	const char arr[] = {'(', '{', '[', '<'};
	SECTION("Returns the correct index") {
		REQUIRE(getIndexOfSymbolInArr(arr, 4, '(') == 0);
		REQUIRE(getIndexOfSymbolInArr(arr, 4, '{') == 1);
		REQUIRE(getIndexOfSymbolInArr(arr, 4, '[') == 2);
		REQUIRE(getIndexOfSymbolInArr(arr, 4, '<') == 3);
	}
	SECTION("Return -1 if the symbol is not part of the arr")
	{
		REQUIRE(getIndexOfSymbolInArr(arr, 4, 'a') == -1);
		REQUIRE(getIndexOfSymbolInArr(arr, 4, 'f') == -1);
		REQUIRE(getIndexOfSymbolInArr(arr, 4, '+') == -1);
	}
}
