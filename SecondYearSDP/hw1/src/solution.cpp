#include "solution.h"
#include "stack"
#include <iostream>

const char OPENING_BRACKETS[] = {'(', '{', '[', '<'};
const char CLOSING_BRACKETS[] = {')', '}', ']', '>'};

int getIndexOfSymbolInArr(const char arr[], const unsigned &arrSize, const char &symbol)
{
  for (unsigned i = 0; i < arrSize; i++)
  {
    if (symbol == arr[i])
      return i;
  }
  return -1;
}

bool isCorrect(const char *expression)
{
  std::stack<char> bracketTracker;

  for (const char *i = expression; *i != '\0'; i++)
  {
    if (getIndexOfSymbolInArr(OPENING_BRACKETS, 4, *i) != -1)
    {
      bracketTracker.push(*i);
    }
    else
    {
      int index = getIndexOfSymbolInArr(CLOSING_BRACKETS, 4, *i);
      if (index != -1)
      {
        if (bracketTracker.size() > 0 && bracketTracker.top() == OPENING_BRACKETS[index])
        {
          bracketTracker.pop();
        }
        else
          return false;
      }
    }
  }

  return bracketTracker.size() == 0;
}