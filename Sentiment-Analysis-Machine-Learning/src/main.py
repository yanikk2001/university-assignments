import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report

from MultinominalNaiveBayes import MultinomialNaiveBayes
from Tokenizer import Tokenizer

RANDOM_SEED = 42

np.random.seed(RANDOM_SEED)

train = pd.read_csv("./data/labeledTrainData.tsv", delimiter="\t")
test = pd.read_csv("./data/testData.tsv", delimiter="\t")

reviews = train['review'].values
sentiments = train['sentiment'].values
  
X_train, X_test, y_train, y_test = train_test_split(reviews, sentiments, test_size=0.2, random_state=RANDOM_SEED)

SentimentAnalysisModel = MultinomialNaiveBayes(
    classes=np.unique(sentiments), 
    tokenizer=Tokenizer()
)

SentimentAnalysisModel.fit(X_train, y_train) # Train the model

y_hat = SentimentAnalysisModel.classify(X_test)

print("Accuracy score:", accuracy_score(y_test, y_hat))

print("Classification report: \n ", classification_report(y_test, y_hat))

# Manual test
positive_review1 = """
One of the other reviewers has mentioned that after watching just 1 Oz episode you'll be hooked. They are right, as this is
exactly what happened with me.<br /><br />The first thing that struck me about Oz was its brutality and unflinching scenes of violence,
which set in right from the word GO. Trust me, this is not a show for the faint hearted or timid. This show pulls no punches with regards to drugs,
sex or violence. Its is hardcore, in the classic use of the word.<br /><br />It is called OZ as that is the nickname given to the Oswald Maximum
Security State Penitentary. It focuses mainly on Emerald City, an experimental section of the prison where all the cells have glass fronts
and face inwards, so privacy is not high on the agenda. Em City is home to many..Aryans, Muslims, gangstas, Latinos, Christians, Italians, Irish
and more....so scuffles, death stares, dodgy dealings and shady agreements are never far away.<br /><br />I would say the main appeal of the
show is due to the fact that it goes where other shows wouldn't dare. Forget pretty pictures painted for mainstream audiences, forget charm,
forget romance...OZ doesn't mess around. The first episode I ever saw struck me as so nasty it was surreal, I couldn't say I was ready for it,
but as I watched more, I developed a taste for Oz, and got accustomed to the high levels of graphic violence. Not just violence, but injustice
(crooked guards who'll be sold out for a nickel, inmates who'll kill on order and get away with it, well mannered, middle class inmates being
turned into prison bitches due to their lack of street skills or prison experience) Watching Oz, you may become comfortable with
what is uncomfortable viewing....thats if you can get in touch with your darker side."""
print("Positive review 1:", SentimentAnalysisModel.classifyText(positive_review1))

positive_review2 = "I loved this movie. It was great. I will watch it again."
print("Positive review 2:", SentimentAnalysisModel.classifyText(positive_review2))

negative_review1 = "I hated this movie. It was terrible. I will not watch it again."
print("Negative review 1:", SentimentAnalysisModel.classifyText(negative_review1))

negative_review2 = "Boring... :("
print("Negative review 2:", SentimentAnalysisModel.classifyText(negative_review2))

negative_review3 = """
Scotty (Grant Cramer, who would go on to star in the great B-movie "Killer Klowns from outer space") agrees to help three middle-aged guys learn
how to 'dialog' the ladies in this bad '80's comedy. Not bad as in '80's lingo, which meant good. Bad as in bad. With no likable characters,
including, but not limited to, a kid who's the freakiest looking guy since "Friday the 13th part 2"' a girl who leads men on and then goes into
hissy fits when they want to touch her, and the token fat slob, because after all what would an '80's sex comedy be without a fat slob?? Well this
one has two. This movie is pretty much the bottom of the barrel of '80's sex comedies. And then came the sequel thus deepening said proverbial
barrel.<br /><br />My Grade:D- <br /><br />Eye Candy: too numerous to count, you even see the freaky looking kid imagined with boobs at on point,
think "Bachlor Party" but not as funny, and VERY disturbing.<br /><br />Where I saw it: Comcast Moviepass"""
print("Negative review 3:", SentimentAnalysisModel.classifyText(negative_review3))


