import numpy as np
import math
from collections import Counter, defaultdict

"""
A general implementation of the Multinomial Naive Bayes classifier which doesnt assume the number of classes or the tokenizer used.
It can be used for any classification problem, not just sentiment analysis, like spam detection, topic classification, etc.
"""
class MultinomialNaiveBayes:
  
    def __init__(self, classes, tokenizer):
      self.classes = classes
      self.tokenizer = tokenizer
      
    def group_by_class(self, X, y):
      data = dict()
      for c in self.classes:
        data[c] = X[np.where(y == c)]
      return data
           
    def fit(self, X, y):
      """
      Fit the Multinomial Naive Bayes classifier to the training data.

      Parameters:
      - X: list of strings
        The input data, where each string represents a document or text snippet. In this case, a review.
      - y: list of labels
        The corresponding labels for each document or text snippet in X. In this case, the sentiment of the review (positive or negative).

      Returns:
      - self: MultinominalNaiveBayes object
        The fitted classifier object.

      Note: This method assumes that the `tokenizer` attribute has been set to a tokenizer object that can
      tokenize the input text.

      """
      self.log_class_priors = {} # log(P(c)) - log of the probability of each class
      self.word_counts = {} # A map - number of times each word appears in a class
      self.items_num_class = {} # number of items in each class
      self.vocab = set() # vocabulary full of all unique seen words, it is shared between all classes

      n = len(X)
      
      grouped_data = self.group_by_class(X, y)
      
      for c, data in grouped_data.items(): # iterate over each class and its corresponding data (reviews)
        self.items_num_class[c] = len(data)
        self.log_class_priors[c] = math.log(self.items_num_class[c] / n) # log(P(c)), using log to avoid underflow, log is a monotonic function, so it will not affect the results
        self.word_counts[c] = defaultdict(lambda: 0)
        
        for text in data: # iterate over each text snipped (review) in the data
          counts = Counter(self.tokenizer.tokenize(text))
          for word, count in counts.items():
            if word not in self.vocab:
              self.vocab.add(word)

            self.word_counts[c][word] += count
            
      return self
      
    def laplace_smoothing(self, word, text_class, k=1):
      """
      Applies Laplace smoothing. Used to avoid zero probabilities for words not present in one or more classes in the training data,
      log(0) is undefined.
      Avoids zero probabilities by assuming that each word has been seen k extra times in each class.
      Avoids overfitting, or building a model that doesn’t generalize well to previously unseen data. Like when a word has been seen in one class but not in another.

      Parameters:
        word (str): The word to calculate the probability for.
        text_class (str): The text class to calculate the probability for.
        k (int, optional): The smoothing strength parameter. Defaults to 1. Strength k assumes having seen k extra of each
        outcome

      Returns:
        float: The log probability of seeing the word in a given text class.
      """
      num = self.word_counts[text_class][word] + k
      denom = self.items_num_class[text_class] + k * len(self.vocab)
      return math.log(num / denom)
    
    def classifyText(self, text):
      """
      Classifies the given single text into one of the predefined classes.

      Parameters:
      - text (str): The text to be classified.

      Returns:
      - str: The predicted class for the given text.
      """
      class_scores = {c: self.log_class_priors[c] for c in self.classes}

      words = set(self.tokenizer.tokenize(text))
      for word in words:
        if word not in self.vocab: continue # ignore words that are not in the vocabulary

        for c in self.classes:
          log_w_given_c = self.laplace_smoothing(word, c) # log(P(w|c))
          class_scores[c] += log_w_given_c # we add istead of multiply because log(ab) = log(a) + log(b)
      return max(class_scores, key=class_scores.get) # return the class name with the highest score

    def classify(self, X):
      """
      Classifies a list of texts using the Multinomial Naive Bayes classifier.

      Parameters:
      - X (list): A list of texts to be classified.

      Returns:
      - result (list): A list of classification results for each input text.
      """
      result = []
      for text in X:
        result.append(self.classifyText(text))

      return result
