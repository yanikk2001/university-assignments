import re
from bs4 import BeautifulSoup
import nltk
from nltk.corpus import stopwords
nltk.download('stopwords')

class Tokenizer:
    def clean(self, text):
            """
            Cleans the given text by removing HTML tags and non-alphabetic characters,
            and removing extra whitespaces.

            Args:
                text (str): The text to be cleaned.

            Returns:
                str: The cleaned text.
            """
            no_html = BeautifulSoup(text, features="html.parser").get_text() # remove HTML tags
            clean = re.sub("[^a-z\s]+", " ", no_html, flags=re.IGNORECASE) # negating matching, remove everything except a-z, A-Z and whitespaces, also case insensitive
            return re.sub("(\s+)", " ", clean) # remove extra whitespaces


    def tokenize(self, text):
            """
            Tokenizes the given text by removing punctuation, converting to lowercase, and removing stopwords.

            Args:
                text (str): The text to be tokenized.

            Returns:
                list: A list of tokens after tokenization.
            """
            clean = self.clean(text).lower()
            stopwords_en = stopwords.words("english") # get the stopwords for English language like that, is, and, etc. Speeding up our model
            return [w for w in re.split("\W+", clean) if not w in stopwords_en] # split the text into words and remove stopwords
