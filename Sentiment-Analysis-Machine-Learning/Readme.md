# Machine Learning Sentiment Analysis Model 

## TLDR
- classification model
- uses Naive Bayes theorem
- implemented to predict whether a movie review is positive or negative
- The `MultinominalNaiveBayes.py` can be used for other classification tasks such as spam detection, topic classification, etc

## Getting started
install dependencies (need python ^3) with
```
pip install -r requirements.txt
```

## License MIT
