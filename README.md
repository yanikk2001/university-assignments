# University assignments I have done over the years

### Cloning the repository 📄
As this repo contains submodules so it is recommended to clone it using:
```sh
git clone --recurse-submodules git@gitlab.com:yanikk2001/university-assignments.git
```

# License 🧾

**MIT**
