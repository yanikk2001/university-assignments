#define CATCH_CONFIG_MAIN
#include "../../catch.h"

#include <iostream>

#include "../Source/dyn-array.h"

using namespace dl;

TEST_CASE("DebugAllocator test")
{
    DebugAllocator<int> alloc;

    SECTION("Single allocation")
    {
        int *p = alloc.allocate(10);
        REQUIRE(alloc.allocations_count() == 1);
        REQUIRE_NOTHROW(alloc.free(p));
        REQUIRE(alloc.allocations_count() == 0);
    }

    SECTION("Multiple allocations")
    {
        int *p = alloc.allocate(10);
        int *ptr = alloc.allocate(20);
        REQUIRE(alloc.allocations_count() == 2);
        alloc.free(p);
        REQUIRE(alloc.allocations_count() == 1);
        alloc.free(ptr);
        REQUIRE(alloc.allocations_count() == 0);
    }

    SECTION("Trying to free foreign pointer")
    {
        int *ptr = new int[10];
        REQUIRE_THROWS(alloc.free(ptr));
    }
}

TEST_CASE("Constructor, destructor and operators")
{
    SECTION("Constructor with default values")
    {
        dynamicArr<int, DebugAllocator<int>> arr;
        REQUIRE(arr.allocGood());
        REQUIRE(arr.size() == 0);
        REQUIRE(arr.capacity() == 20);
    }

    SECTION("Constructor with set value")
    {
        dynamicArr<float, DebugAllocator<float>> arr{5};
        REQUIRE(arr.allocGood());
        REQUIRE(arr.size() == 0);
        REQUIRE(arr.capacity() == 5);
    }

    SECTION("Constructor with invalid arguments")
    {
        bool caught_except = false;
        try
        {
            dynamicArr<float, DebugAllocator<float>> arr{0};
        }
        catch (const std::invalid_argument &e)
        {
            caught_except = true;
        }

        // For some reason cannot test directly if constructor throws an exception with REQUIRE_THROWS
        // If the below statement is executes it means the constr didn`t throw an exception but it should have
        if (!caught_except)
        {
            REQUIRE(1 == 0);
        }
    }

    SECTION("Copy constructor")
    {
        dynamicArr<int, DebugAllocator<int>> arr{5};
        arr.push_back(1);
        arr.push_back(2);
        arr.push_back(3);
        arr.push_back(4);

        dynamicArr<int, DebugAllocator<int>> arrCpy{arr};
        REQUIRE(arrCpy.allocGood());

        REQUIRE(arr.size() == 4);
        REQUIRE(arr.capacity() == 5);
        REQUIRE(arrCpy.size() == 4);
        REQUIRE(arr.capacity() == 5);

        REQUIRE((arr == arrCpy));
    }

    SECTION("Copy assignment operator - has enough capacity")
    {
        dynamicArr<int, DebugAllocator<int>> arr{5};
        arr.push_back(1);
        arr.push_back(2);
        arr.push_back(3);
        arr.push_back(4);

        dynamicArr<int, DebugAllocator<int>> arrCpy{10};
        arrCpy.push_back(10);
        arrCpy.push_back(20);
        arrCpy.push_back(30);
        arrCpy.push_back(40);
        arrCpy.push_back(40);
        arrCpy.push_back(40);
        arrCpy.push_back(40);

        arrCpy = arr;
        REQUIRE(arrCpy.size() == 4);
        REQUIRE(arrCpy.capacity() == 5);
        REQUIRE((arrCpy == arr));
    }

    SECTION("Copy assignment operator - does not have enough capacity")
    {
        dynamicArr<int, DebugAllocator<int>> arr{5};
        arr.push_back(1);
        arr.push_back(2);
        arr.push_back(3);
        arr.push_back(4);

        dynamicArr<int, DebugAllocator<int>> arrCpy{2};
        arrCpy.push_back(10);
        arrCpy.push_back(20);

        arrCpy = arr;
        REQUIRE(arrCpy.size() == 4);
        REQUIRE(arrCpy.capacity() == 5);
        REQUIRE((arrCpy == arr));
    }

    SECTION("Clear method")
    {
        dynamicArr<int, DebugAllocator<int>> arr{5};
        arr.push_back(1);
        arr.push_back(2);
        arr.push_back(3);
        arr.push_back(4);

        arr.clear();
        REQUIRE(arr.capacity() == 0);
        REQUIRE(arr.size() == 0);
        REQUIRE(arr.allocGood(0));
    }
}

TEST_CASE("Push_back")
{
    dynamicArr<int, DebugAllocator<int>> arr{3};
    REQUIRE(arr.capacity() == 3);
    REQUIRE(arr.size() == 0);

    SECTION("Has enough capacity")
    {
        arr.push_back(1);
        REQUIRE(arr.size() == 1);
        REQUIRE(arr.capacity() == 3);
        REQUIRE(arr.allocGood());

        arr.push_back(2);
        REQUIRE(arr.size() == 2);
        REQUIRE(arr.capacity() == 3);
        REQUIRE(arr.allocGood());

        arr.push_back(3);
        REQUIRE(arr.size() == 3);
        REQUIRE(arr.capacity() == 3);
        REQUIRE(arr.allocGood());
    }

    SECTION("Does not have enough capacity")
    {
        arr.push_back(1);
        arr.push_back(2);
        arr.push_back(3);
        REQUIRE(arr.capacity() == 3);
        REQUIRE(arr.size() == 3);
        REQUIRE(arr.allocGood());

        dynamicArr<int, DebugAllocator<int>> cpy{arr};

        arr.push_back(4);
        REQUIRE(arr.capacity() == 6);
        REQUIRE(arr.size() == 4);
        REQUIRE(arr.allocGood());

        REQUIRE(arr[0] == 1);
        REQUIRE(arr[1] == 2);
        REQUIRE(arr[2] == 3);
        REQUIRE(arr[3] == 4);
    }
}

TEST_CASE("RESERVE")
{
    dynamicArr<double, DebugAllocator<double>> arr{10};
    arr.push_back(1);
    arr.push_back(2);
    arr.push_back(3);
    arr.push_back(4);

    SECTION("Invalid input")
    {
        REQUIRE_THROWS(arr.reserve(arr.max_size() + 1));
    }

    SECTION("Input lower capacity - should do nothing")
    {
        arr.reserve(5);
        REQUIRE(arr.capacity() == 10);
    }

    SECTION("Input higher capacity - should reallocate")
    {
        dynamicArr<double, DebugAllocator<double>> cpy{20};
        cpy.push_back(1);
        cpy.push_back(2);
        cpy.push_back(3);
        cpy.push_back(4);

        arr.reserve(20);
        REQUIRE(arr.capacity() == 20);
        REQUIRE((arr == cpy));
    }
}

TEST_CASE("Pop_back and empty")
{
    dynamicArr<int> arr;
    SECTION("Empty")
    {
        REQUIRE(arr.empty());
        arr.push_back(1);
        REQUIRE_FALSE(arr.empty());
        arr.pop_back();
        REQUIRE(arr.empty());
        REQUIRE(arr.size() == 0);
        REQUIRE(arr.capacity() == 20);
    }
}

TEST_CASE("At and front, back methods")
{
    dynamicArr<int> arr;
    arr.push_back(1);
    arr.push_back(2);
    arr.push_back(3);
    arr.push_back(4);
    arr.push_back(5);

    const dynamicArr<int> constArr{arr};

    SECTION("Invalid input")
    {
        REQUIRE_THROWS_AS(arr.at(100), std::out_of_range);
        REQUIRE_THROWS_AS(constArr.at(100), std::out_of_range);
    }

    SECTION("Normal use")
    {
        REQUIRE(arr.at(0) == 1);
        REQUIRE(arr.at(1) == 2);
        REQUIRE(arr.at(2) == 3);
        REQUIRE(arr.at(3) == 4);
        REQUIRE(arr.at(4) == 5);

        REQUIRE(constArr.at(0) == 1);
        REQUIRE(constArr.at(1) == 2);
        REQUIRE(constArr.at(2) == 3);
        REQUIRE(constArr.at(3) == 4);
        REQUIRE(constArr.at(4) == 5);
    }

    SECTION("Front, back")
    {
        REQUIRE(arr.front() == arr.at(0));
        REQUIRE(constArr.front() == constArr.at(0));
        REQUIRE(arr.back() == arr.at(arr.size() - 1));
        REQUIRE(constArr.back() == constArr.at(constArr.size() - 1));
    }
}

TEST_CASE("Resize")
{
    dynamicArr<int, DebugAllocator<int>> arr{10};
    arr.push_back(1);
    arr.push_back(2);
    arr.push_back(3);
    arr.push_back(4);
    arr.push_back(5);

    SECTION("Input smaller size")
    {
        arr.resize(2);
        REQUIRE(arr.size() == 2);
        REQUIRE(arr.capacity() == 10);
        REQUIRE(arr[0] == 1);
        REQUIRE(arr[1] == 2);
        REQUIRE(arr.allocGood());
    }

    SECTION("Input bigger size but is still smaller than capacity")
    {
        arr.resize(8);
        REQUIRE(arr.size() == 8);
        REQUIRE(arr.capacity() == 10);
        REQUIRE(arr[0] == 1);
        REQUIRE(arr[1] == 2);
        REQUIRE(arr[2] == 3);
        REQUIRE(arr[3] == 4);
        REQUIRE(arr[4] == 5);
        REQUIRE(arr[5] == 0);
        REQUIRE(arr[6] == 0);
        REQUIRE(arr[7] == 0);
        REQUIRE(arr.allocGood());
    }

    SECTION("Input size > capacity")
    {
        arr.resize(15);
        REQUIRE(arr.size() == 15);
        REQUIRE(arr.capacity() == 20);
        REQUIRE(arr[0] == 1);
        REQUIRE(arr[1] == 2);
        REQUIRE(arr[2] == 3);
        REQUIRE(arr[3] == 4);
        REQUIRE(arr[4] == 5);
        for (std::size_t i = 5; i < arr.size(); i++)
        {
            REQUIRE(arr[i] == 0);
        }
        REQUIRE(arr.allocGood());
    }
}

TEST_CASE("Shrink to Fit")
{
    dynamicArr<char, DebugAllocator<char>> arr{20};

    SECTION("Empty container")
    {
        arr.shrink_to_fit();
        REQUIRE(arr.capacity() == 0);
        REQUIRE(arr.size() == 0);
        REQUIRE(arr.allocGood(0));
    }

    SECTION("Not empty")
    {
        arr.push_back('a');
        arr.push_back('b');
        arr.push_back('c');
        arr.push_back('d');
        arr.push_back('e');
        REQUIRE(arr.size() == 5);
        REQUIRE(arr.capacity() == 20);

        arr.shrink_to_fit();
        REQUIRE(arr.capacity() == 5);
        REQUIRE(arr.size() == 5);
        REQUIRE(arr.allocGood());
    }
}