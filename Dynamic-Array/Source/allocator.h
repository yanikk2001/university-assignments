/**
 * @file allocator.h
 * @author Deyan Krumov (deyan.krumov@lights.digital)
 * @brief Contains Allocator and debug Allocator used in DynamicArr as well as interface class so a custom Allocator class can be implemented
 * @version 1.0.0
 * @date 2022-04-04
 * 
 * @copyright MIT
 */

#pragma once
#include <unordered_set>

template <typename T>
class Allocator
{
public:
    virtual T* allocate(const std::size_t &size) = 0;
    virtual void free(T* arr) = 0;
    virtual std::size_t getSizeOfType() {return sizeof(T);}
};

template <typename T>
class DefAllocator : public Allocator<T>
{
public:
    virtual T *allocate(const size_t &size) override
    {
        try
        {
            return new T[size];
        }
        catch (const std::bad_alloc &e)
        {
            throw e;
        }
    }

    virtual void free(T *arr) override
    {
        delete[] arr;
    }
};

template <typename T>
class DebugAllocator : public Allocator<T>
{
    std::unordered_set<T *> allocations{};

public:
    virtual T* allocate(const std::size_t &size) override
    {
        try
        {
            T *arr = new T[size];
            allocations.insert(arr);
            return arr;
        }
        catch (const std::bad_alloc &e)
        {
            throw e;
        }
    }

    virtual void free(T *arr) override
    {
        if (allocations.count(arr) != 1)
            throw std::invalid_argument("arr is not allocated by this instance");

        allocations.erase(arr);
        delete[] arr;
    }

    virtual std::size_t getSizeOfType() override
    {
        return sizeof(T);
    }

    size_t allocations_count() const
    {
        return allocations.size();
    }

    DebugAllocator<T>& operator=(const DebugAllocator<T>& other)
    {
        if(this != &other)
        {
            allocations = other.allocations;
        }
        return *this;
    }
};