/**
 * @file dyn-array.h
 * @author Deyan Krumov (deyan.krumov@lights.digital)
 * @brief A simple class representing basic implementation of dynamic array
 * @version 1.0.0
 * @date 2022-04-01
 * 
 * @copyright MIT
 */

#pragma once 

#ifndef DYNAMIC_ARRAY_INCLUDED
#define DYNAMIC_ARRAY_INCLUDED

#include <stdexcept>
#include <assert.h>
#include "allocator.h"

#define MAX_SIZE 2305843009213693951

namespace dl
{
    template <typename dataType, typename allocator = DefAllocator<dataType>>
    class dynamicArr
    {
        dataType* _data{};
        std::size_t _size, _capacity;

        allocator _alloc;

    public:
        /**
         * @brief Construct a new dynamic Arr object
         * @throws std::invalid_argument - if initialCapacity is set to <= 0
         * @throws std::bad_alloc - if it fails to allocate memory
         * @param initialCapacity (default val = 20)
         */
        dynamicArr(const std::size_t& initialCapacity = 20);

        /**
         * @brief Copy constructor
         * @throw - if it catches exception during copying
         * @param other object to be copied from
         */
        dynamicArr(const dynamicArr& other);

        /// Uses the copy and swap idiom
        dynamicArr& operator=(const dynamicArr& other);

        /// Calls the clear method
        ~dynamicArr();
        
        /**
         * @brief compares two dynamicArrs
         * @param other object to be compared to
         * @return true if their _data and _size propeties are the same
         * @note _capacity property is not compared
         */
        bool operator==(const dynamicArr& other);

        /**
         * @brief Appends the given element value to the end of the container
         * @details If there is enough capacity only appends otherwise it reallocates the whole container in linear complexity
         * @exception std::length_error if the needed new capacity is over the max limit
         * @exception If an exception is thrown (which can be due to Allocator::allocate() or element copy/move constructor/assignment)
         * @param el element to be appended
         * @note strong exception guarantee
         */
        void push_back(const dataType& el);

        /**
         * @brief Removes the last element of the container
         * @exception std::runtime_error if container is empty
         */
        void pop_back();

        ///@brief exception safe methods
        dataType& at(const std::size_t& index);
        const dataType& at(const std::size_t& index) const;

        /// Following the stl example this operator does not protect you from 'out of bounds' errors and segmentation faults
        dataType& operator[](const std::size_t& index) {return _data[index];}
        const dataType& operator[](const std::size_t& index) const {return _data[index];}

        dataType& front() {return _data[0];}
        const dataType& front() const {return _data[0];}
        dataType& back() {return _data[_size -1];}
        const dataType& back() const {return _data[_size -1];}

        size_t size() const {return _size;}
        size_t capacity() const {return _capacity;}
        size_t max_size() const {return MAX_SIZE;}

        bool empty() const {return _size == 0;}

        /**
         * @brief frees memory and sets all properties to initial values
         */
        void clear();

        /**
         * @brief Increase the capacity of the container (the total number of elements that it can hold)
         * @details If newCapacity is greater than the current capacity(), new storage is allocated, otherwise the function does nothing.
         * @exception std::length_error if newCapacity > max_size().
         * @exception thrown by Allocator::allocate() (typically std::bad_alloc)
         * @param newCapacity 
         * @note strong exception guarantee
         */
        void reserve(const std::size_t& newCapacity);

        /**
         * @brief Resizes the container to contain newSize number of elements
         * @details If the current size is greater than newSize, the container is reduced to its first count elements.
                    If the current size is less than newSize but newSize is <= to capacity it puts default values to all 
                    slots with indexes from size to newSize
                    If newSize is > capacity it reallocates the container and does the same as the previous if statement
         * @exception thrown by Allocator::allocate() (typically std::bad_alloc)
         * @param newSize 
         */
        void resize (const std::size_t& newSize);

        /**
         * @brief Requests the removal of unused capacity
         * @details reallocates the container with capacity == size
         * @exception thrown by Allocator::allocate() (typically std::bad_alloc)
         */
        void shrink_to_fit();

        /**
         * @brief reports whether there are memory leaks
         * 
         * @param val 1 means there is only one allocated ptr which will be deleted after obj destr
         * @return true if allocated ptrs are equal to input param val
         * @return false 
         */
        bool allocGood(std::size_t val = 1) {return _alloc.allocations_count() == val;}

    protected:
        /**
         * @brief Copies all elements from source to destination
         * 
         * @param source 
         * @param destination 
         * @param size 
         */
        void safeCopy(const dataType* source, dataType* destination, const size_t& size);

        /**
         * @brief assigns a certain value to multiple elements
         * 
         * @param receiver the container of elements to be changed
         * @param val value to be set to all elements
         * @param start inidex of first value to be changed from reciever
         * @param finish index of last value, it is not changed
         */
        void insertValue(dataType* receiver, const dataType& val, const size_t& start, const size_t& finish);

        /**
         * @brief reallocates container
         * @exception thrown by Allocator::allocate() (typically std::bad_alloc)
         * @param newCapacity 
         */
        void changeCapacity(const size_t& newCapacity);
    };

    #include "dyn-array.inl"
} // namespace dl

#endif