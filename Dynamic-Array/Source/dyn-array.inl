template <typename dataType, typename allocator>
inline dynamicArr<dataType, allocator>::dynamicArr(const std::size_t &initialCapacity) : _capacity(initialCapacity), _size(0)
{
    if (sizeof(dataType) != _alloc.getSizeOfType())
        throw std::invalid_argument("Data type and data type of allocator don`t match");
    if (initialCapacity <= 0 || initialCapacity > MAX_SIZE)
        throw std::invalid_argument("Invalid initial capacity");

    _data = _alloc.allocate(_capacity);
}

template <typename dataType, typename allocator>
inline void dynamicArr<dataType, allocator>::safeCopy(const dataType *source, dataType *destination, const size_t &size)
{
    try
    {
        for (std::size_t i = 0; i < size; i++)
        {
            destination[i] = source[i];
        }
    }
    catch (...)
    {
        this->_alloc.free(destination);
        throw;
    }
}

template <typename dataType, typename allocator>
inline dynamicArr<dataType, allocator>::dynamicArr(const dynamicArr<dataType, allocator> &other) : _capacity(other._capacity), _size(other._size)
{
    _data = _alloc.allocate(_capacity);

    safeCopy(other._data, _data, other._size);
}

template <typename dataType, typename allocator>
inline dynamicArr<dataType, allocator> &dynamicArr<dataType, allocator>::operator=(const dynamicArr &other)
{
    if (this != &other)
    {
        dynamicArr<dataType, allocator> cpy{other};
        std::swap(_data, cpy._data);
        std::swap(_size, cpy._size);
        std::swap(_capacity, cpy._capacity);
        allocator tmp = _alloc;
        _alloc = cpy._alloc;
        cpy._alloc = tmp;
    }
    return *this;
}

template <typename dataType, typename allocator>
inline dynamicArr<dataType, allocator>::~dynamicArr()
{
    clear();
}

template <typename dataType, typename allocator>
inline bool dynamicArr<dataType, allocator>::operator==(const dynamicArr<dataType, allocator> &other)
{
    if (_size != other._size)
        return false;

    for (std::size_t i = 0; i < _size; i++)
    {
        if (_data[i] != other._data[i])
            return false;
    }
    return true;
}

template <typename dataType, typename allocator>
inline void dynamicArr<dataType, allocator>::clear()
{
    if (_data)
    {
        _alloc.free(_data);
        _data = nullptr;
    }
    _size = _capacity = 0;
}

template <typename dataType, typename allocator>
inline void dynamicArr<dataType, allocator>::push_back(const dataType &el)
{
    if (_size == _capacity)
    {
        try
        {
            reserve(2 * _capacity);
        }
        catch (...)
        {
            throw;
        }
    }

    _data[_size] = el;
    _size++;
}

template <typename dataType, typename allocator>
inline void dynamicArr<dataType, allocator>::reserve(const std::size_t &newCapacity)
{
    if (newCapacity < 0 || newCapacity > MAX_SIZE)
        throw std::length_error("Invalid new capacity value");

    if (newCapacity <= _capacity)
        return;

    changeCapacity(newCapacity);
}

template <typename dataType, typename allocator>
inline void dynamicArr<dataType, allocator>::changeCapacity(const size_t &newCapacity)
{
    dataType *newData = _alloc.allocate(newCapacity);
    _capacity = newCapacity;
    safeCopy(_data, newData, _size);

    _alloc.free(_data);
    _data = newData;
}

template <typename dataType, typename allocator>
inline void dynamicArr<dataType, allocator>::pop_back()
{
    if (empty())
        throw std::runtime_error("Container is empty");
    _size--;
}

template <typename dataType, typename allocator>
inline dataType &dynamicArr<dataType, allocator>::at(const std::size_t &index)
{
    if (index < 0 || index >= _size)
        throw std::out_of_range("Invalid index");
    return _data[index];
}

template <typename dataType, typename allocator>
inline const dataType &dynamicArr<dataType, allocator>::at(const std::size_t &index) const
{
    try
    {
        return const_cast<dynamicArr<dataType, allocator> *>(this)->at(index);
    }
    catch (...)
    {
        throw;
    }
}

template <typename dataType, typename allocator>
inline void dynamicArr<dataType, allocator>::insertValue(dataType *receiver, const dataType &val, const size_t &start, const size_t &finish)
{
    for (size_t i = start; i < finish; i++)
    {
        receiver[i] = val;
    }
}

template <typename dataType, typename allocator>
inline void dynamicArr<dataType, allocator>::resize(const std::size_t &newSize)
{
    if (newSize == _size)
        return;
    else if (newSize > _size && newSize <= _capacity)
    {
        insertValue(_data, dataType{}, _size, newSize);
    }
    else
    {
        size_t newCapacity = _capacity;
        while (newCapacity < newSize)
            newCapacity *= 2;
        reserve(newCapacity);
        insertValue(_data, dataType{}, _size, newSize);
    }
    _size = newSize;
}

template <typename dataType, typename allocator>
inline void dynamicArr<dataType, allocator>::shrink_to_fit()
{
    if (empty())
        clear();
    else
        changeCapacity(_size);
}
