# Simple Dynamic Array 🎉
## _Simple, Fast, Tested_ 👌

### Setting up CMAKE
(From root)
#### Configures the project and generates a native build system inside build dir
```sh
cd ./Tests
mkdir build
cd ./build
cmake ../
```
#### To compile and run tests
(from build dir created in the previous step) execute:
```sh
cmake --build . --target test
./test
```
> Note: To compile tests and generate documentation at the same time use `cmake --build .` 

### Documentation: Can be generated using the following command: 
(doxygen v1.9.3 or lower)
```sh
cd Tests
cmake --build . --target doc_doxygen
```
> Note: Documentation files will be generated inside `DocumentationFiles/`  
## License

MIT

**Free Software, Hell Yeah!**